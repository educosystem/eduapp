<?xml version="1.0" encoding="utf-8"?><!--
  ~ /*
  ~  * This file is part of EduApp.
  ~  *
  ~  *     EduApp is free software: you can redistribute it and/or modify
  ~  *     it under the terms of the GNU General Public License as published by
  ~  *     the Free Software Foundation, either version 3 of the License, or
  ~  *     (at your option) any later version.
  ~  *
  ~  *     EduApp is distributed in the hope that it will be useful,
  ~  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~  *     GNU General Public License for more details.
  ~  *
  ~  *     You should have received a copy of the GNU General Public License
  ~  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
  ~  *
  ~  *     Copyright Cosme José Nieto Pérez, 2020
  ~  */
  -->


<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="element"
            type="com.educosystem.eduapp.data.entity.Element" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:id="@+id/listItemElementItemContainer"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_margin="8dp"
        android:elevation="2dp">

        <TextView
            android:id="@+id/listItemElementTitle"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="16dp"
            android:layout_marginTop="16dp"
            android:layout_marginEnd="16dp"
            android:layout_marginBottom="16dp"
            android:ellipsize="end"
            android:singleLine="true"
            android:textAppearance="@style/TextAppearance.AppCompat.Large"
            android:textColor="@drawable/selectable_item_color"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toStartOf="@+id/listItemElementBarrierGrade"
            app:layout_constraintStart_toEndOf="@+id/listItemElementBarrier"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintVertical_bias="0.0"
            app:textAndVisibility="@{element.name}" />

        <TextView
            android:id="@+id/listItemElementDescription"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="16dp"
            android:layout_marginTop="16dp"
            android:layout_marginEnd="16dp"
            android:layout_marginBottom="16dp"
            android:ellipsize="end"
            android:maxLines="13"
            android:singleLine="false"
            android:textAppearance="@style/TextAppearance.AppCompat.Medium"
            android:textColor="@drawable/selectable_item_color"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toEndOf="@+id/listItemElementBarrier"
            app:layout_constraintTop_toBottomOf="@+id/listItemElementBarrierHorizontal"
            app:layout_constraintVertical_bias="0.0"
            app:textAndVisibility="@{element.description}" />

        <TextView
            android:id="@+id/listItemElementDelayedText"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="16dp"
            android:layout_marginEnd="8dp"
            android:layout_marginBottom="16dp"
            android:text="@string/delayed"
            android:textAppearance="@style/TextAppearance.AppCompat.Small"
            android:textColor="@color/delayedTextColour"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toStartOf="@+id/listItemElementGrade"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintVertical_bias="0.0" />

        <TextView
            android:id="@+id/listItemElementGrade"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="16dp"
            android:layout_marginEnd="16dp"
            android:layout_marginBottom="16dp"
            android:textAppearance="@style/TextAppearance.AppCompat.Medium"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintVertical_bias="0.0" />

        <View
            android:id="@+id/divider"
            android:layout_width="0dp"
            android:layout_height="1dp"
            android:background="?android:attr/listDivider"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintStart_toStartOf="parent" />

        <ImageView
            android:id="@+id/listItemElementImageType"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="16dp"
            android:layout_marginTop="16dp"
            android:layout_marginBottom="16dp"
            android:visibility="gone"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintVertical_bias="0.0"
            tools:ignore="ContentDescription" />

        <ImageView
            android:id="@+id/listItemElementImageSelected"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="16dp"
            android:layout_marginTop="16dp"
            android:layout_marginBottom="16dp"
            android:contentDescription="@string/selected"
            android:tint="@color/secondaryColor"
            android:visibility="gone"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintVertical_bias="0.0"
            app:srcCompat="@drawable/check_circle_black_24dp" />

        <androidx.constraintlayout.widget.Barrier
            android:id="@+id/listItemElementBarrier"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:barrierDirection="end"
            app:constraint_referenced_ids="listItemElementImageType,listItemElementImageSelected"
            tools:layout_editor_absoluteX="411dp" />

        <androidx.constraintlayout.widget.Barrier
            android:id="@+id/listItemElementBarrierHorizontal"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:barrierDirection="bottom"
            app:constraint_referenced_ids="listItemElementTitle,listItemElementDelayedText"
            tools:layout_editor_absoluteY="102dp" />

        <androidx.constraintlayout.widget.Barrier
            android:id="@+id/listItemElementBarrierGrade"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:barrierDirection="left"
            app:constraint_referenced_ids="listItemElementGrade,listItemElementDelayedText"
            tools:layout_editor_absoluteX="395dp" />

    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>