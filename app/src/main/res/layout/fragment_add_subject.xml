<?xml version="1.0" encoding="utf-8"?><!--
  ~ /*
  ~  * This file is part of EduApp.
  ~  *
  ~  *     EduApp is free software: you can redistribute it and/or modify
  ~  *     it under the terms of the GNU General Public License as published by
  ~  *     the Free Software Foundation, either version 3 of the License, or
  ~  *     (at your option) any later version.
  ~  *
  ~  *     EduApp is distributed in the hope that it will be useful,
  ~  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~  *     GNU General Public License for more details.
  ~  *
  ~  *     You should have received a copy of the GNU General Public License
  ~  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
  ~  *
  ~  *     Copyright Cosme José Nieto Pérez, 2020
  ~  */
  -->

<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="viewModel"
            type="com.educosystem.eduapp.viewmodels.AddSubjectViewModel" />
    </data>

    <androidx.coordinatorlayout.widget.CoordinatorLayout
        android:id="@+id/addSubjectCoordinatorLayout"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="@color/primaryColor">

        <com.google.android.material.appbar.AppBarLayout
            android:id="@+id/addSubjectAppbar"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:liftOnScroll="true">

            <androidx.appcompat.widget.Toolbar
                android:id="@+id/addSubjectToolbar"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                app:menu="@menu/save_menu" />
        </com.google.android.material.appbar.AppBarLayout>

        <androidx.core.widget.NestedScrollView
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            app:layout_behavior="com.google.android.material.appbar.AppBarLayout$ScrollingViewBehavior"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/addSubjectAppbar">

            <androidx.constraintlayout.widget.ConstraintLayout
                android:id="@+id/linearLayout"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="vertical">

                <EditText
                    android:id="@+id/addSubjectName"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:ems="10"
                    android:hint="@string/Name"
                    android:importantForAutofill="no"
                    android:inputType="textPersonName"
                    android:textAppearance="@style/TextAppearance.AppCompat.Large"
                    android:textColorHighlight="@color/secondaryColor"
                    android:visibility="@{viewModel.subjectAttributesVisibility}"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    tools:layout_conversion_absoluteHeight="82dp"
                    tools:layout_conversion_absoluteWidth="0dp" />

                <CheckBox
                    android:id="@+id/addSubjectContinuousAssessment"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:checked="true"
                    android:gravity="start|center_vertical"
                    android:text="@string/continuousAssessment"
                    android:textAppearance="@style/TextAppearance.AppCompat.Medium"
                    android:visibility="@{viewModel.subjectAttributesVisibility}"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/addSubjectName" />

                <androidx.recyclerview.widget.RecyclerView
                    android:id="@+id/addSubjectColorRecyclerView"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:orientation="horizontal"
                    android:visibility="@{viewModel.subjectAttributesVisibility}"
                    app:layoutManager="androidx.recyclerview.widget.LinearLayoutManager"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/addSubjectContinuousAssessment" />

                <androidx.constraintlayout.widget.Barrier
                    android:id="@+id/addSubjectBarrier"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    app:barrierDirection="bottom"
                    app:constraint_referenced_ids="addSubjectRecyclerView,addSubjectStructureGroup"
                    tools:layout_editor_absoluteY="651dp" />

                <androidx.recyclerview.widget.RecyclerView
                    android:id="@+id/addSubjectRecyclerView"
                    android:layout_width="0dp"
                    android:layout_height="150dp"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:fadeScrollbars="false"
                    android:paddingBottom="16dp"
                    android:requiresFadingEdge="vertical"
                    android:scrollbarAlwaysDrawVerticalTrack="true"
                    android:scrollbars="vertical"
                    android:visibility="@{viewModel.recyclerVisibility}"
                    app:layoutManager="androidx.recyclerview.widget.LinearLayoutManager"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/addSubjectColorRecyclerView" />

                <RadioGroup
                    android:id="@+id/addSubjectStructureGroup"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:checkedButton="@id/addSubjectStandardStructureButton"
                    android:paddingBottom="16dp"
                    android:visibility="@{viewModel.groupStructureVisibility}"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/addSubjectRecyclerView">

                    <TextView
                        android:id="@+id/textView"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/subjectStructure"
                        android:textAppearance="@style/TextAppearance.AppCompat.Medium" />

                    <RadioButton
                        android:id="@+id/addSubjectStandardStructureButton"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/standardStructure" />

                    <RadioButton
                        android:id="@+id/addSubjectUnitsWithStandardStructureButton"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/unitsWithStandardStructure" />

                    <RadioButton
                        android:id="@+id/addSubjectPersonalizedButton"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/personalized" />

                    <LinearLayout
                        android:id="@+id/addSubjectPercentageGroup"
                        android:layout_width="match_parent"
                        android:layout_height="match_parent"
                        android:layout_weight="9"
                        android:orientation="horizontal"
                        android:visibility="@{viewModel.percentageGroupVisibility}">

                        <EditText
                            android:id="@+id/addSubjectNumberExams"
                            android:layout_width="0dp"
                            android:layout_height="wrap_content"
                            android:layout_weight="2"
                            android:ems="10"
                            android:gravity="end"
                            android:hint="@string/defaultValueExams"
                            android:importantForAutofill="no"
                            android:inputType="number" />

                        <TextView
                            android:id="@+id/addSubjectNumberTextExams"
                            android:layout_width="0dp"
                            android:layout_height="wrap_content"
                            android:layout_weight="1"
                            android:text="@string/percentExams"
                            android:textAppearance="@style/TextAppearance.AppCompat.Medium" />

                        <EditText
                            android:id="@+id/addSubjectNumberWorks"
                            android:layout_width="0dp"
                            android:layout_height="wrap_content"
                            android:layout_weight="2"
                            android:ems="10"
                            android:gravity="end"
                            android:hint="@string/defaultValueWorks"
                            android:importantForAutofill="no"
                            android:inputType="number" />

                        <TextView
                            android:id="@+id/addSubjectNumberTextWorks"
                            android:layout_width="0dp"
                            android:layout_height="wrap_content"
                            android:layout_weight="1"
                            android:text="@string/percentWorks"
                            android:textAppearance="@style/TextAppearance.AppCompat.Medium" />

                        <EditText
                            android:id="@+id/addSubjectNumberHomework"
                            android:layout_width="0dp"
                            android:layout_height="wrap_content"
                            android:layout_weight="2"
                            android:ems="10"
                            android:gravity="end"
                            android:hint="@string/defaultValueHomework"
                            android:importantForAutofill="no"
                            android:inputType="number" />

                        <TextView
                            android:id="@+id/addSubjectNumberTextHomework"
                            android:layout_width="0dp"
                            android:layout_height="wrap_content"
                            android:layout_weight="1"
                            android:text="@string/percentHomework"
                            android:textAppearance="@style/TextAppearance.AppCompat.Medium" />
                    </LinearLayout>
                </RadioGroup>

                <RadioGroup
                    android:id="@+id/addSubjectStructureGroupExt"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:checkedButton="@id/addSubjectStandardStructureButtonExt"
                    android:paddingBottom="16dp"
                    android:visibility="@{viewModel.groupExtraordinaryStructureVisibility}"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/addSubjectBarrier">

                    <TextView
                        android:id="@+id/textView2"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/subjectStructureExtraordinary"
                        android:textAppearance="@style/TextAppearance.AppCompat.Medium" />

                    <RadioButton
                        android:id="@+id/addSubjectStandardStructureButtonExt"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/standardStructureExtraordinary" />

                    <RadioButton
                        android:id="@+id/add_subject_exams_and_work"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/workAndExamsExtraordinaryStructure" />

                    <LinearLayout
                        android:id="@+id/addSubjectPercentageGroupExt"
                        android:layout_width="match_parent"
                        android:layout_height="match_parent"
                        android:layout_weight="9"
                        android:orientation="horizontal"
                        android:visibility="@{viewModel.percentageExtraordinaryGroupVisibility}">

                        <EditText
                            android:id="@+id/addSubjectNumberExamsExt"
                            android:layout_width="0dp"
                            android:layout_height="wrap_content"
                            android:layout_weight="2"
                            android:ems="10"
                            android:gravity="end"
                            android:hint="@string/defaultValueExamsExtraordinary"
                            android:importantForAutofill="no"
                            android:inputType="number" />

                        <TextView
                            android:id="@+id/addSubjectNumberTextExamsExt"
                            android:layout_width="0dp"
                            android:layout_height="wrap_content"
                            android:layout_weight="1"
                            android:text="@string/percentExams"
                            android:textAppearance="@style/TextAppearance.AppCompat.Medium" />

                        <EditText
                            android:id="@+id/addSubjectNumberWorksExt"
                            android:layout_width="0dp"
                            android:layout_height="wrap_content"
                            android:layout_weight="2"
                            android:ems="10"
                            android:gravity="end"
                            android:hint="@string/defaultValueWorksExtraordinary"
                            android:importantForAutofill="no"
                            android:inputType="number" />

                        <TextView
                            android:id="@+id/addSubjectNumberTextWorksExt"
                            android:layout_width="0dp"
                            android:layout_height="wrap_content"
                            android:layout_weight="1"
                            android:text="@string/percentWorks"
                            android:textAppearance="@style/TextAppearance.AppCompat.Medium" />
                    </LinearLayout>
                </RadioGroup>

            </androidx.constraintlayout.widget.ConstraintLayout>
        </androidx.core.widget.NestedScrollView>

    </androidx.coordinatorlayout.widget.CoordinatorLayout>


</layout>