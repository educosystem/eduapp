<?xml version="1.0" encoding="utf-8"?><!--
  ~ /*
  ~  * This file is part of EduApp.
  ~  *
  ~  *     EduApp is free software: you can redistribute it and/or modify
  ~  *     it under the terms of the GNU General Public License as published by
  ~  *     the Free Software Foundation, either version 3 of the License, or
  ~  *     (at your option) any later version.
  ~  *
  ~  *     EduApp is distributed in the hope that it will be useful,
  ~  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~  *     GNU General Public License for more details.
  ~  *
  ~  *     You should have received a copy of the GNU General Public License
  ~  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
  ~  *
  ~  *     Copyright Cosme José Nieto Pérez, 2020
  ~  */
  -->

<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="viewModel"
            type="com.educosystem.eduapp.viewmodels.AddElementViewModel" />
    </data>

    <androidx.coordinatorlayout.widget.CoordinatorLayout
        android:id="@+id/addElementCoordinatorLayout"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="@color/primaryColor">

        <com.google.android.material.appbar.AppBarLayout
            android:id="@+id/addElementAppbar"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:liftOnScroll="true">

            <androidx.appcompat.widget.Toolbar
                android:id="@+id/addElementToolbar"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                app:menu="@menu/save_menu" />
        </com.google.android.material.appbar.AppBarLayout>

        <androidx.core.widget.NestedScrollView
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            app:layout_behavior="com.google.android.material.appbar.AppBarLayout$ScrollingViewBehavior"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/addElementAppbar">

            <androidx.constraintlayout.widget.ConstraintLayout
                android:id="@+id/linearLayout"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="vertical">

                <Spinner
                    android:id="@+id/addElementSubjectSpinner"
                    android:layout_width="0dp"
                    android:layout_height="0dp"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:visibility="@{viewModel.subjectSpinnerVisibility}"
                    app:layout_constraintBottom_toBottomOf="@+id/addElementSubjectTextView"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toEndOf="@+id/addElementSubjectTextView"
                    app:layout_constraintTop_toTopOf="@+id/addElementSubjectTextView" />

                <Spinner
                    android:id="@+id/addElementBlockSpinner"
                    android:layout_width="0dp"
                    android:layout_height="0dp"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:visibility="@{viewModel.blockSpinnerVisibility}"
                    app:layout_constraintBottom_toBottomOf="@+id/addElementBlockTextView"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toEndOf="@+id/addElementBlockTextView"
                    app:layout_constraintTop_toTopOf="@+id/addElementBlockTextView" />

                <TextView
                    android:id="@+id/addElementSubjectTextView"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:text="@string/subject"
                    android:textAppearance="@style/TextAppearance.AppCompat.Medium"
                    android:visibility="@{viewModel.subjectSpinnerVisibility}"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent" />

                <TextView
                    android:id="@+id/addElementBlockTextView"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:text="@string/block"
                    android:textAppearance="@style/TextAppearance.AppCompat.Medium"
                    android:visibility="@{viewModel.blockSpinnerVisibility}"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/addElementSubjectTextView" />

                <TextView
                    android:id="@+id/addElementSubBlockTextView"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:text="@string/subBlock"
                    android:textAppearance="@style/TextAppearance.AppCompat.Medium"
                    android:visibility="@{viewModel.subBlockSpinnerVisibility}"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/addElementGroup" />

                <androidx.constraintlayout.widget.Barrier
                    android:id="@+id/addElementBarrier"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    app:barrierDirection="bottom"
                    app:constraint_referenced_ids="addElementGroup,addElementSubBlockTextView,addElementSubBlockSpinner,addElementSubjectSpinner,addElementSubjectTextView,addElementBlockSpinner,addElementBlockTextView"
                    tools:layout_editor_absoluteY="651dp" />

                <RadioGroup
                    android:id="@+id/addElementGroup"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:orientation="horizontal"
                    android:paddingBottom="16dp"
                    android:visibility="@{viewModel.groupVisibility}"
                    android:weightSum="3"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/addElementBlockTextView">

                    <RadioButton
                        android:id="@+id/addElementHomeWorkButton"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_weight="1"
                        android:drawableStart="@drawable/ic_home_work_black_24dp"
                        android:drawableLeft="@drawable/ic_home_work_black_24dp"
                        android:text="@string/homeworksName"
                        android:visibility="@{viewModel.homeworkVisibility}" />

                    <RadioButton
                        android:id="@+id/addElementWorkButton"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_weight="1"
                        android:drawableStart="@drawable/ic_work_black_24dp"
                        android:drawableLeft="@drawable/ic_work_black_24dp"
                        android:text="@string/worksName"
                        android:visibility="@{viewModel.workVisibility}" />

                    <RadioButton
                        android:id="@+id/addElementExamsButton"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_weight="1"
                        android:drawableStart="@drawable/ic_exam_black_24dp"
                        android:drawableLeft="@drawable/ic_exam_black_24dp"
                        android:text="@string/examsName"
                        android:visibility="@{viewModel.examVisibility}" />
                </RadioGroup>

                <Spinner
                    android:id="@+id/addElementSubBlockSpinner"
                    android:layout_width="0dp"
                    android:layout_height="0dp"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:visibility="@{viewModel.subBlockSpinnerVisibility}"
                    app:layout_constraintBottom_toBottomOf="@+id/addElementSubBlockTextView"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toEndOf="@+id/addElementSubBlockTextView"
                    app:layout_constraintTop_toTopOf="@+id/addElementSubBlockTextView" />

                <EditText
                    android:id="@+id/addElementName"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:ems="10"
                    android:hint="@string/Name"
                    android:importantForAutofill="no"
                    android:inputType="textPersonName"
                    android:textAppearance="@style/TextAppearance.AppCompat.Large"
                    android:textColorHighlight="@color/secondaryColor"
                    android:visibility="@{viewModel.detailsVisibility}"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/addElementBarrier"
                    tools:layout_conversion_absoluteHeight="82dp"
                    tools:layout_conversion_absoluteWidth="0dp" />

                <Spinner
                    android:id="@+id/addElementDateSpinner"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="8dp"
                    android:layout_marginRight="8dp"
                    android:visibility="@{viewModel.detailsVisibility}"
                    app:layout_constraintEnd_toStartOf="@+id/guideline"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/addElementDateTextView" />

                <Spinner
                    android:id="@+id/addElementTimeSpinner"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="8dp"
                    android:layout_marginLeft="8dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:visibility="@{viewModel.detailsVisibility}"
                    app:layout_constraintBottom_toBottomOf="@+id/addElementDateSpinner"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="@+id/guideline"
                    app:layout_constraintTop_toTopOf="@+id/addElementDateSpinner" />

                <EditText
                    android:id="@+id/addElementDescription"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:ems="10"
                    android:gravity="start|top"
                    android:hint="@string/description"
                    android:importantForAutofill="no"
                    android:inputType="textMultiLine"
                    android:textAppearance="@style/TextAppearance.AppCompat.Small"
                    android:visibility="@{viewModel.detailsVisibility}"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/addElementDateSpinner" />

                <androidx.constraintlayout.widget.Guideline
                    android:id="@+id/guideline"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:orientation="vertical"
                    app:layout_constraintGuide_percent="0.50121653" />

                <TextView
                    android:id="@+id/addElementDateTextView"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:text="@string/deliveryDate"
                    android:textAppearance="@style/TextAppearance.AppCompat.Medium"
                    android:visibility="@{viewModel.detailsVisibility}"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/addElementName" />

            </androidx.constraintlayout.widget.ConstraintLayout>
        </androidx.core.widget.NestedScrollView>

    </androidx.coordinatorlayout.widget.CoordinatorLayout>


</layout>