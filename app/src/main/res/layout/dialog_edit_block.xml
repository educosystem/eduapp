<?xml version="1.0" encoding="utf-8"?><!--
  ~ /*
  ~  * This file is part of EduApp.
  ~  *
  ~  *     EduApp is free software: you can redistribute it and/or modify
  ~  *     it under the terms of the GNU General Public License as published by
  ~  *     the Free Software Foundation, either version 3 of the License, or
  ~  *     (at your option) any later version.
  ~  *
  ~  *     EduApp is distributed in the hope that it will be useful,
  ~  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~  *     GNU General Public License for more details.
  ~  *
  ~  *     You should have received a copy of the GNU General Public License
  ~  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
  ~  *
  ~  *     Copyright Cosme José Nieto Pérez, 2020
  ~  */
  -->

<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="viewModel"
            type="com.educosystem.eduapp.viewmodels.dialogs.EditBlockViewModel" />
    </data>

    <androidx.coordinatorlayout.widget.CoordinatorLayout
        android:id="@+id/editBlockCoordinatorLayout"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="@color/primaryColor">

        <com.google.android.material.appbar.AppBarLayout
            android:id="@+id/editBlockAppbar"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:liftOnScroll="true">

            <androidx.appcompat.widget.Toolbar
                android:id="@+id/editBlockToolbar"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                app:menu="@menu/save_menu"
                app:navigationIcon="@drawable/ic_close_black_24dp" />
        </com.google.android.material.appbar.AppBarLayout>

        <androidx.core.widget.NestedScrollView
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            app:layout_behavior="com.google.android.material.appbar.AppBarLayout$ScrollingViewBehavior"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/editBlockAppbar">

            <androidx.constraintlayout.widget.ConstraintLayout
                android:id="@+id/linearLayout"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="vertical">

                <EditText
                    android:id="@+id/editBlockName"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:ems="10"
                    android:hint="@string/Name"
                    android:importantForAutofill="no"
                    android:inputType="textPersonName"
                    android:textAppearance="@style/TextAppearance.AppCompat.Large"
                    android:textColorHighlight="@color/secondaryColor"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    tools:layout_conversion_absoluteHeight="82dp"
                    tools:layout_conversion_absoluteWidth="0dp" />

                <EditText
                    android:id="@+id/editBlockPercentage"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:layout_weight="2"
                    android:ems="10"
                    android:gravity="end"
                    android:hint="@string/percentageHint"
                    android:importantForAutofill="no"
                    android:inputType="number"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toEndOf="@+id/add_block_percentage_text_view"
                    app:layout_constraintTop_toBottomOf="@+id/editBlockName" />

                <TextView
                    android:id="@+id/add_block_percentage_text_view"
                    android:layout_width="wrap_content"
                    android:layout_height="0dp"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:gravity="center_vertical"
                    android:text="@string/percentage"
                    android:textAppearance="@style/TextAppearance.AppCompat.Large"
                    app:layout_constraintBottom_toBottomOf="@+id/editBlockPercentage"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="@+id/editBlockPercentage" />

                <TextView
                    android:id="@+id/editBlockPercentageSuggestion"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:layout_marginRight="16dp"
                    android:layout_marginBottom="16dp"
                    android:text="@{viewModel.messageTextView}"
                    android:textAppearance="@style/TextAppearance.AppCompat.Small"
                    app:layout_constraintBottom_toBottomOf="parent"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/add_block_percentage_text_view" />

            </androidx.constraintlayout.widget.ConstraintLayout>
        </androidx.core.widget.NestedScrollView>

    </androidx.coordinatorlayout.widget.CoordinatorLayout>


</layout>