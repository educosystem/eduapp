/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.ui

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.viewpager2.widget.ViewPager2
import com.educosystem.eduapp.MainActivity
import com.educosystem.eduapp.R
import com.educosystem.eduapp.adapters.pages.PageDetailBlockAdapter
import com.educosystem.eduapp.data.db.EduAppDatabase
import com.educosystem.eduapp.data.entity.Block
import com.educosystem.eduapp.databinding.FragmentDetailBlockBinding
import com.educosystem.eduapp.ui.dialogs.EditBlockDialog
import com.educosystem.eduapp.util.EditBlockDialogConstants
import com.educosystem.eduapp.util.PageListener
import com.educosystem.eduapp.util.TYPE_HOMEWORK
import com.educosystem.eduapp.util.obtainColour
import com.educosystem.eduapp.viewmodels.DetailBlockViewModel
import com.educosystem.eduapp.viewmodels.factories.DetailBlockViewModelFactory
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

/**
 * A simple [Fragment] subclass.
 */
class DetailBlockFragment : Fragment(), EditBlockDialogConstants {
    private lateinit var mainActivity: MainActivity
    private lateinit var toolbar: Toolbar
    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var viewModel: DetailBlockViewModel
    private lateinit var viewPager: ViewPager2
    private lateinit var fabButton: FloatingActionButton
    private lateinit var tabLayout: TabLayout
    private lateinit var navController: NavController
    private lateinit var args: DetailBlockFragmentArgs

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentDetailBlockBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_detail_block, container, false
        )
        args = DetailBlockFragmentArgs.fromBundle(requireArguments())
        coordinatorLayout = binding.detailBlockCoordinatorLayout
        toolbar = binding.detailBlockToolbar
        mainActivity = requireActivity() as MainActivity
        fabButton = binding.detailBlockAddButton
        tabLayout = binding.detailBlockTabLayout
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        val application = requireNotNull(this.activity).application
        viewPager = binding.detailBlockViewPager
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = DetailBlockViewModelFactory(
            dataSource.subjectDao,
            dataSource.blockDao,
            dataSource.subBlockDao,
            args.active,
            args.codSubject,
            args.codBlock,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(DetailBlockViewModel::class.java)

        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        toolbar.inflateMenu(R.menu.contextual_edit)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        addObservers()
        addListeners()
        if (viewModel.block.value != null)
            loadViewPager(viewModel.block.value!!)

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.subject.observe(viewLifecycleOwner, Observer {
            val colour = obtainColour(it.colour, resources, requireContext())
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                requireActivity().window.statusBarColor = colour.color
            }
            toolbar.setBackgroundColor(colour.color)
            tabLayout.setBackgroundColor(colour.color)
            coordinatorLayout.setBackgroundColor(colour.color)
            viewModel.evaluateData()
        })

        viewModel.block.observe(viewLifecycleOwner, Observer {
            if (it != null && viewModel.viewPageLoad) {
                loadViewPager(it)
                viewModel.doneLoadViewPager()
            }
        })

        viewModel.subBlocks.observe(viewLifecycleOwner, Observer {
            viewModel.evaluateData()
        })

        viewModel.navigateToAddSubBlock.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                navController.navigate(
                    DetailBlockFragmentDirections.actionDetailBlockFragmentToAddBlockFragment(
                        args.codSubject,
                        null,
                        it
                    )
                )
                viewModel.doneNavigatingToAddSubBlock()
            }
        })

        viewModel.navigateToAddElement.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                navController.navigate(
                    DetailBlockFragmentDirections.actionDetailBlockFragmentToAddElementFragment(
                        args.codSubject,
                        null,
                        it,
                        null,
                        null
                    )
                )
                viewModel.doneNavigatingToAddElement()
            }
        })

        viewModel.navigateToDetailSubBlock.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                navController.navigate(
                    DetailBlockFragmentDirections.actionDetailBlockFragmentToDetailSubBlockFragment(
                        args.codSubject,
                        it,
                        viewModel.active
                    )
                )
                viewModel.doneNavigatingToDetailSubBlock()
            }
        })

        viewModel.navigateToDetailElement.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                navController.navigate(
                    DetailBlockFragmentDirections.actionDetailBlockFragmentToDetailElementFragment(
                        it
                    )
                )
                viewModel.doneNavigatingToDetailElement()
            }
        })

        viewModel.showEditDialog.observe(viewLifecycleOwner, Observer { showDialog ->
            if (showDialog) {
                val colour =
                    obtainColour(viewModel.subject.value!!.colour, resources, requireContext())
                val fragmentManager = mainActivity.supportFragmentManager
                val bundle = Bundle()
                val dialogFragment = EditBlockDialog()
                val transaction = fragmentManager.beginTransaction()
                bundle.let {
                    it.putString(ARG_SUB_SUBJECT, viewModel.block.value!!.codSubSubject)
                    it.putString(ARG_BLOCK, args.codBlock)
                    it.putInt(ARG_COLOR, colour.color)
                }
                dialogFragment.arguments = bundle

                transaction.setCustomAnimations(
                    R.anim.enter_save,
                    0,
                    0,
                    R.anim.pop_back_stack_exit_save
                )
                transaction
                    .add(android.R.id.content, dialogFragment)
                    .addToBackStack(null)
                    .commit()

                viewModel.doneShowEditDialog()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if (viewModel.function == null)
                    Snackbar.make(
                        coordinatorLayout,
                        it,
                        Snackbar.LENGTH_SHORT
                    ).show()
                else
                    Snackbar.make(
                        coordinatorLayout,
                        it,
                        Snackbar.LENGTH_SHORT
                    ).setAction(R.string.undo) {
                        viewModel.function!!.invoke()
                        viewModel.doneUseFunction()
                    }.show()
                viewModel.showSnackBarDone()
            }
        })

        viewModel.hideButton.observe(viewLifecycleOwner, Observer {
            if (it)
                fabButton.hide()
            else
                fabButton.show()
        })
    }

    /**
     * Load the listeners, the view pager adapter and the tabLayout
     *
     * @param block
     */
    private fun loadViewPager(block: Block) {
        val subBlockListener = PageListener(startNavigate = {
            viewModel.startNavigateToDetailSubBlock(it)
        }, showSnackBarMessage = { it, _ ->
            viewModel.startSnackBarMessage(it)
        }, hideFabButton = { hide ->
            viewModel.hideFabButton(hide)
        })
        val elementListener = PageListener(startNavigate = {
            viewModel.startNavigateToDetailElement(it)
        }, showSnackBarMessage = { it, function ->
            viewModel.updateFunction(function)
            viewModel.startSnackBarMessage(it)
        }, hideFabButton = { hide ->
            viewModel.hideFabButton(hide)
        })
        val pageAdapter =
            PageDetailBlockAdapter(
                this,
                args.codBlock,
                block.type != null,
                block.type == TYPE_HOMEWORK,
                elementListener,
                subBlockListener
            )
        viewPager.adapter = pageAdapter
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position) {
                0 -> tab.setText(R.string.activePageElements)
                1 -> tab.setText(R.string.delayedPageElements)
                2 -> if (block.type == TYPE_HOMEWORK) tab.setText(R.string.finishedPageElements)
                else tab.setText(R.string.withoutGradePageElements)
                3 -> if (block.type == TYPE_HOMEWORK) tab.setText(R.string.subBlocks)
                else tab.setText(R.string.finishedPageElements)
                4 -> tab.setText(R.string.subBlocks)
            }
        }.attach()
    }

    /**
     * Try to show fab button hiding and showing for make the necessary checks.
     *
     */
    private fun tryShowButton() {
        viewModel.hideFabButton(true)
        viewModel.hideFabButton(false)
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        viewPager.registerOnPageChangeCallback(obtainViewPagerCallbackObject())
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            if (viewPager.currentItem > 0) {
                viewPager.currentItem = 0
            } else {
                this@DetailBlockFragment.findNavController().popBackStack()
            }
        }

        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.menuEdit -> viewModel.startShowEditDialog()
            }
            true
        }
    }

    /**
     * @return an ViewPager2.OnPageChangeCallback object.
     */
    private fun obtainViewPagerCallbackObject(): ViewPager2.OnPageChangeCallback =
        object : ViewPager2.OnPageChangeCallback() {

            /**
             * Perform the necessary action to maintain the correct behaviour.
             *
             * @param position of the actual page. Start in 0.
             */
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (viewModel.oldPosition != position) {
                    mainActivity.requestHideActionMode()
                    viewModel.setLastPosition(position)
                    tryShowButton()
                }
            }
        }
}
