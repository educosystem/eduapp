/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.ui.dialogs

import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.educosystem.eduapp.MainActivity
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.db.EduAppDatabase
import com.educosystem.eduapp.databinding.DialogEditBlockBinding
import com.educosystem.eduapp.util.EditBlockDialogConstants
import com.educosystem.eduapp.viewmodels.dialogs.EditBlockViewModel
import com.educosystem.eduapp.viewmodels.dialogs.factories.EditBlockViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar


class EditBlockDialog : DialogFragment(), EditBlockDialogConstants {
    private lateinit var viewModel: EditBlockViewModel
    private lateinit var toolbar: Toolbar
    private lateinit var coordinatorLayout: CoordinatorLayout
    private var titleTextView: TextView? = null
    private var percentageEditText: EditText? = null
    private val BUNDLE_TITLE = "title"
    private val BUNDLE_PERCENT = "percentage"

    /**
     * Create the dialog and apply no title attribute to windows
     * because we provide a toolbar instead.
     *
     * @param savedInstanceState
     * @return a new Dialog.
     */
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: DialogEditBlockBinding = DataBindingUtil.inflate(
            inflater, R.layout.dialog_edit_block, container, false
        )
        val application = requireNotNull(this.activity).application
        toolbar = binding.editBlockToolbar
        coordinatorLayout = binding.editBlockCoordinatorLayout
        toolbar.title = getString(R.string.edit)
        titleTextView = binding.editBlockName
        percentageEditText = binding.editBlockPercentage
        binding.lifecycleOwner = viewLifecycleOwner
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory =
            EditBlockViewModelFactory(
                dataSource.blockDao,
                dataSource.subBlockDao,
                requireArguments().getString(ARG_SUB_SUBJECT),
                requireArguments().getString(ARG_BLOCK)!!,
                requireArguments().getString(ARG_SUB_BLOCK),
                application
            )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(EditBlockViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        addObservers()
        changeAppearance()
        restoreContent(savedInstanceState)
        addListeners()

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.block.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                viewModel.updateNameAndPercentage()
            }
        })

        viewModel.subBlock.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                viewModel.updateNameAndPercentage()
            }
        })

        viewModel.name.observe(viewLifecycleOwner, Observer {
            if (it != null)
                if (titleTextView?.text!!.isEmpty() && it.isNotEmpty()) {
                    titleTextView?.text = it
                }
        })

        viewModel.percentage.observe(viewLifecycleOwner, Observer {
            if (viewModel.firstSetting && it != null) {
                percentageEditText!!.setText(it.toString())
                viewModel.doneFirstSetting()
                viewModel.evaluateData(
                    titleTextView!!.text.toString(),
                    percentageEditText!!.text.toString()
                )
            }
        })

        viewModel.navigateToBackStack.observe(viewLifecycleOwner, Observer {
            if (it) {
                hideKeyboard()
                viewModel.doneNavigatingToBackStack()
                this@EditBlockDialog.dismiss()
            }
        })

        viewModel.showDialog.observe(viewLifecycleOwner, Observer {
            if (it) {
                MaterialAlertDialogBuilder(context)
                    .setMessage(getString(R.string.discardQuestion))
                    .setPositiveButton(
                        getString(R.string.continueEditing)
                        , null
                    )
                    .setNegativeButton(getString(R.string.discard)) { _, _ ->
                        viewModel.startNavigateToBackStack()
                    }
                    .show()
                viewModel.showDialogDone()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                Snackbar.make(
                    (activity as MainActivity).findViewById(android.R.id.content),
                    it,
                    Snackbar.LENGTH_SHORT
                ).show()
                viewModel.showSnackBarMessageDone()
            }
        })

        viewModel.showErrorPercentages.observe(viewLifecycleOwner, Observer {
            if (it)
                percentageEditText?.error = getString(R.string.errorRangePercentage)
            else
                percentageEditText?.error = null

        })

        viewModel.validData.observe(viewLifecycleOwner, Observer {
            toolbar.menu.findItem(R.id.saveMenuSave).isEnabled = it
        })

        viewModel.clearData.observe(viewLifecycleOwner, Observer {
            if (it) {
                clear()
                viewModel.clearDataDone()
            }
        })
    }

    /**
     * Hide the keyboard before exit from dialog for prevent a keyboard opened.
     *
     */
    private fun hideKeyboard() {
        val iim: InputMethodManager =
            getSystemService(requireContext(), InputMethodManager::class.java)!!
        iim.hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    /**
     * Clear the text.
     *
     */
    private fun clear() {
        titleTextView?.text = ""
    }

    /**
     * Restore information on configuration changes.
     *
     * @param savedInstanceState
     */
    private fun restoreContent(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            titleTextView?.text = it.getString(BUNDLE_TITLE)
            percentageEditText?.setText(it.getString(BUNDLE_PERCENT))
        }
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        titleTextView?.addTextChangedListener {
            viewModel.evaluateData(it.toString(), percentageEditText!!.text.toString())
        }

        percentageEditText?.addTextChangedListener {
            viewModel.evaluateData(titleTextView!!.text.toString(), it.toString())
        }

        toolbar.setOnMenuItemClickListener {
            viewModel.edit(titleTextView?.text.toString(), percentageEditText?.text.toString())
            true
        }

        toolbar.setNavigationOnClickListener {
            handleBackButton()
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            handleBackButton()
        }
    }

    /**
     * Change the color appearance according to the color of the subject.
     *
     */
    private fun changeAppearance() {
        val colour = requireArguments().getInt(ARG_COLOR)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            requireActivity().window.statusBarColor = colour
        }
        toolbar.setBackgroundColor(colour)
        coordinatorLayout.setBackgroundColor(colour)
    }

    /**
     * Handle the action when back button is pressed.
     *
     */
    private fun handleBackButton() {
        if (viewModel.validData.value!!) {
            viewModel.startShowDialog()
        } else {
            viewModel.startNavigateToBackStack()
        }
    }

    /**
     * Save the volatile data when configuration change.
     *
     * @param outState
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (titleTextView != null)
            outState.putString(BUNDLE_TITLE, titleTextView!!.text.toString())
        if (percentageEditText != null)
            outState.putString(BUNDLE_PERCENT, percentageEditText!!.text.toString())
    }
}
