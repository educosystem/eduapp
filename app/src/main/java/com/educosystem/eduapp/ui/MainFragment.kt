/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.viewpager2.widget.ViewPager2
import com.educosystem.eduapp.MainActivity
import com.educosystem.eduapp.R
import com.educosystem.eduapp.adapters.pages.PageElementAdapter
import com.educosystem.eduapp.data.db.EduAppDatabase
import com.educosystem.eduapp.databinding.FragmentMainBinding
import com.educosystem.eduapp.util.GRID_LAYOUT_ELEMENTS
import com.educosystem.eduapp.util.LINEAR_LAYOUT_ELEMENTS
import com.educosystem.eduapp.util.ListElementsTypes
import com.educosystem.eduapp.util.PageListener
import com.educosystem.eduapp.viewmodels.MainFragmentViewModel
import com.educosystem.eduapp.viewmodels.factories.MainFragmentViewModelFactory
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar

/**
 * A simple [Fragment] subclass.
 */
open class MainFragment : Fragment() {
    private lateinit var mainActivity: MainActivity
    private lateinit var toolbar: Toolbar
    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var viewModel: MainFragmentViewModel
    private lateinit var viewPager: ViewPager2
    private lateinit var fabButton: FloatingActionButton
    protected lateinit var navController: NavController
    open val typeList: ListElementsTypes = ListElementsTypes.ACTIVE
    open val canAdd: Boolean = true

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentMainBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_main, container, false
        )
        coordinatorLayout = binding.mainFragmentCoordinatorLayout
        toolbar = binding.mainFragmentToolbar
        mainActivity = requireActivity() as MainActivity
        fabButton = binding.mainFragmentAddButton
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        val application = requireNotNull(this.activity).application
        val elementListener = PageListener(startNavigate = {
            viewModel.startNavigateToDetailElement(it)
        }, showSnackBarMessage = { it, function ->
            viewModel.updateFunction(function)
            viewModel.startSnackBarMessage(it)
        }, hideFabButton = { hide ->
            viewModel.hideFabButton(hide)
        })
        val pageAdapter =
            PageElementAdapter(
                this,
                elementListener,
                typeList
            )
        viewPager = binding.mainFragmentViewPager
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = MainFragmentViewModelFactory(
            canAdd,
            dataSource.subjectDao,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(MainFragmentViewModel::class.java)
        inflateToolbarMenu()
        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        viewPager.adapter = pageAdapter
        addObservers()
        addListeners()

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.cadAddElements.observe(viewLifecycleOwner, Observer {
            viewModel.hideFabButton(true)
            viewModel.hideFabButton(false)
        })

        viewModel.navigateToAddElement.observe(viewLifecycleOwner, Observer {
            if (it && canAdd) {
                navController.navigate(
                    MainFragmentDirections.actionMainFragmentToAddElementFragment(
                        null,
                        null,
                        null,
                        null,
                        null
                    )
                )
                viewModel.doneNavigatingToAddElement()
            }
        })

        viewModel.navigateToDetailElement.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                navigateToDetailElement(it)
                viewModel.doneNavigatingToDetailElement()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if (viewModel.function == null)
                    Snackbar.make(
                        coordinatorLayout,
                        it,
                        Snackbar.LENGTH_SHORT
                    ).show()
                else
                    Snackbar.make(
                        coordinatorLayout,
                        it,
                        Snackbar.LENGTH_SHORT
                    ).setAction(R.string.undo) {
                        viewModel.function!!.invoke()
                    }.show()
                viewModel.showSnackBarDone()
            }
        })

        viewModel.hideButton.observe(viewLifecycleOwner, Observer {
            if (it)
                fabButton.hide()
            else
                fabButton.show()
        })
    }

    /**
     * Navigate to detail element.
     *
     * @param it cod of the element.
     */
    protected open fun navigateToDetailElement(it: String) {
        navController.navigate(
            MainFragmentDirections.actionMainFragmentToDetailElementFragment(it)
        )
    }

    /**
     * Inflate the toolbar menu depending of the actual layout.
     *
     */
    private fun inflateToolbarMenu() {
        toolbar.menu.clear()
        toolbar.inflateMenu(R.menu.layout_menu)
        if (mainActivity.sharedPreferences.getInt(
                mainActivity.SP_ELEMENT_LAYOUT,
                LINEAR_LAYOUT_ELEMENTS
            ) == GRID_LAYOUT_ELEMENTS
        ) {
            toolbar.menu.findItem(R.id.layoutMenuLayout).setTitle(R.string.listMode)
            toolbar.menu.findItem(R.id.layoutMenuLayout).setIcon(R.drawable.view_list_24px)
        }
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        toolbar.setOnMenuItemClickListener {
            mainActivity.changeElementsLayout()
            inflateToolbarMenu()
            true
        }
    }
}
