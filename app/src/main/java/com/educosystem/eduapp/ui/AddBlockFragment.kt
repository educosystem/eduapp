/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.ui

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.educosystem.eduapp.MainActivity
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.db.EduAppDatabase
import com.educosystem.eduapp.databinding.FragmentAddBlockBinding
import com.educosystem.eduapp.util.obtainColour
import com.educosystem.eduapp.viewmodels.AddBlockViewModel
import com.educosystem.eduapp.viewmodels.factories.AddBlockViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar


class AddBlockFragment : Fragment() {
    private val BUNDLE_TITLE = "title"
    private val BUNDLE_RADIO_EXAM = "radio_exam"
    private val BUNDLE_RADIO_WORK = "radio_work"
    private val BUNDLE_RADIO_HOMEWORK = "radio_homework"
    private val BUNDLE_RADIO_UNIT = "radio_unit"
    private val BUNDLE_EDIT_TEXT_PERCENTAGE = "edit_text_percentage"
    private lateinit var mainActivity: MainActivity
    private lateinit var viewModel: AddBlockViewModel
    private lateinit var toolbar: Toolbar
    private lateinit var navController: NavController
    private lateinit var args: AddBlockFragmentArgs
    private lateinit var titleTextView: EditText
    private lateinit var radioExam: RadioButton
    private lateinit var radioWork: RadioButton
    private lateinit var radioHomework: RadioButton
    private lateinit var radioUnit: RadioButton
    private lateinit var editTextPercentage: EditText
    private lateinit var radioGroup: RadioGroup
    private lateinit var coordinatorLayout: CoordinatorLayout

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentAddBlockBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_add_block, container, false
        )
        val application = requireNotNull(this.activity).application
        mainActivity = requireActivity() as MainActivity
        toolbar = binding.addBlockToolbar
        titleTextView = binding.addBlockName
        radioExam = binding.addBlockExamsButton
        radioWork = binding.addBlockWorkButton
        radioHomework = binding.addBlockHomeWorkButton
        radioUnit = binding.addBlockUnitButton
        editTextPercentage = binding.addBlockPercentage
        radioGroup = binding.addBlockGroup
        coordinatorLayout = binding.addBlockCoordinatorLayout
        binding.lifecycleOwner = viewLifecycleOwner
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        args = AddBlockFragmentArgs.fromBundle(requireArguments())
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = AddBlockViewModelFactory(
            dataSource.subjectDao,
            dataSource.blockDao,
            dataSource.subBlockDao,
            args.codSubject,
            args.codSubSubject,
            args.codBlock,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(AddBlockViewModel::class.java)

        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        toolbar.navigationIcon =
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_close_black_24dp)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        addObservers()
        restoreContent(savedInstanceState)
        addListeners()

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.subject.observe(viewLifecycleOwner, Observer {
            it?.let {
                val colour = obtainColour(it.colour, resources, requireContext())
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    requireActivity().window.statusBarColor = colour.color
                }
                toolbar.setBackgroundColor(colour.color)
                coordinatorLayout.setBackgroundColor(colour.color)
            }
        })

        viewModel.block.observe(viewLifecycleOwner, Observer {
            viewModel.checkVisibility()
        })

        viewModel.blocks.observe(viewLifecycleOwner, Observer {
            viewModel.checkVisibility()
        })

        viewModel.subBlocks.observe(viewLifecycleOwner, Observer {
            viewModel.checkVisibility()
        })

        viewModel.isUnitAvailable.observe(viewLifecycleOwner, Observer {
            viewModel.checkVisibility()
        })

        viewModel.notifyUIUpdate.observe(viewLifecycleOwner, Observer {
            if (it) {
                checkRadioButton()
                viewModel.doneNotifyUIUpdate()
            }
        })

        viewModel.navigateToDetailBlock.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                hideKeyboard()
                navController.navigate(
                    AddBlockFragmentDirections.actionAddBlockFragmentToDetailBlockFragment(
                        codSubject = args.codSubject,
                        codBlock = it
                    )
                )
                viewModel.doneNavigatingToDetailBlock()
            }
        })

        viewModel.navigateToDetailSubBlock.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                hideKeyboard()
                navController.navigate(
                    AddBlockFragmentDirections.actionAddBlockFragmentToDetailSubBlockFragment(
                        codSubject = args.codSubject,
                        codSubBlock = it
                    )
                )
                viewModel.doneNavigatingToDetailSubBlock()
            }
        })

        viewModel.navigateToBackStack.observe(viewLifecycleOwner, Observer {
            if (it) {
                hideKeyboard()
                navController.popBackStack()
                viewModel.doneNavigatingToBackStack()
            }
        })

        viewModel.showDialog.observe(viewLifecycleOwner, Observer {
            if (it) {
                MaterialAlertDialogBuilder(context)
                    .setMessage(getString(R.string.discardQuestion))
                    .setPositiveButton(
                        getString(R.string.continueEditing)
                        , null
                    )
                    .setNegativeButton(getString(R.string.discard)) { _, _ ->
                        viewModel.startNavigateToBackStack()
                    }
                    .show()
                viewModel.showDialogDone()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                Snackbar.make(
                    mainActivity.findViewById(android.R.id.content),
                    it,
                    Snackbar.LENGTH_SHORT
                ).show()
                viewModel.showSnackBarMessageDone()
            }
        })

        viewModel.showErrorPercentages.observe(viewLifecycleOwner, Observer {
            if (it)
                editTextPercentage.error = getString(R.string.errorRangePercentage)
            else
                editTextPercentage.error = null

        })

        viewModel.validData.observe(viewLifecycleOwner, Observer {
            toolbar.menu.findItem(R.id.saveMenuSave).isEnabled = it
        })
    }

    /**
     * Perform auto check depending of available types
     *
     */
    private fun checkRadioButton() {
        if (viewModel.firstCheck && viewModel.isUnitAvailable.value != null) {
            radioGroup.check(
                when {
                    viewModel.isExamAvailable.value!! -> radioExam.id
                    viewModel.isWorkAvailable.value!! -> radioWork.id
                    viewModel.isHomeworkAvailable.value!! -> radioHomework.id
                    viewModel.isUnitAvailable.value!! -> radioUnit.id
                    else -> -1
                }
            )
            viewModel.doneFirstCheck()
        }
    }

    /**
     * Hide the keyboard before exit from dialog for prevent a keyboard opened.
     *
     */
    private fun hideKeyboard() {
        val iim: InputMethodManager =
            getSystemService(requireContext(), InputMethodManager::class.java)!!
        iim.hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    /**
     * Restore information on configuration changes.
     *
     * @param savedInstanceState
     */
    private fun restoreContent(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            titleTextView.setText(it.getString(BUNDLE_TITLE))
            radioExam.isChecked = it.getBoolean(BUNDLE_RADIO_EXAM)
            radioUnit.isChecked = it.getBoolean(BUNDLE_RADIO_UNIT)
            radioWork.isChecked = it.getBoolean(BUNDLE_RADIO_WORK)
            radioHomework.isChecked = it.getBoolean(BUNDLE_RADIO_HOMEWORK)
            editTextPercentage.setText(it.getString(BUNDLE_EDIT_TEXT_PERCENTAGE))
        }
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        titleTextView.addTextChangedListener { evaluateData() }

        toolbar.setOnMenuItemClickListener {
            viewModel.save(
                titleTextView.text.toString(),
                radioExam.isChecked,
                radioWork.isChecked,
                radioHomework.isChecked,
                radioUnit.isChecked,
                editTextPercentage.text.toString()
            )
            true
        }

        editTextPercentage.addTextChangedListener { evaluateData() }

        toolbar.setNavigationOnClickListener {
            handleBackButton()
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            handleBackButton()
        }
    }

    /**
     * Simply call to viewModel evaluate data function for do the checks
     *
     */
    private fun evaluateData() {
        viewModel.evaluateData(
            titleTextView.text.toString(),
            editTextPercentage.text.toString()
        )
    }

    /**
     * Handle the action when back button is pressed.
     *
     */
    private fun handleBackButton() {
        if (viewModel.validData.value!!) {
            viewModel.startShowDialog()
        } else {
            viewModel.startNavigateToBackStack()
        }
    }

    /**
     * Save the volatile data when configuration change.
     *
     * @param outState
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.let {
            it.putString(BUNDLE_TITLE, titleTextView.text.toString())
            it.putBoolean(BUNDLE_RADIO_EXAM, radioExam.isChecked)
            it.putBoolean(BUNDLE_RADIO_UNIT, radioUnit.isChecked)
            it.putBoolean(BUNDLE_RADIO_WORK, radioWork.isChecked)
            it.putBoolean(BUNDLE_RADIO_HOMEWORK, radioHomework.isChecked)
            it.putString(BUNDLE_EDIT_TEXT_PERCENTAGE, editTextPercentage.text.toString())
        }
    }
}
