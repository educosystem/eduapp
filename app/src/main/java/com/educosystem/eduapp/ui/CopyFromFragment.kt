/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *  
 */

package com.educosystem.eduapp.ui

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.RecyclerView
import com.educosystem.eduapp.MainActivity
import com.educosystem.eduapp.R
import com.educosystem.eduapp.adapters.BlockWithSubBlockAdapter
import com.educosystem.eduapp.data.db.EduAppDatabase
import com.educosystem.eduapp.data.entity.Evaluation
import com.educosystem.eduapp.databinding.FragmentCopyFromBinding
import com.educosystem.eduapp.util.Colour
import com.educosystem.eduapp.util.obtainColour
import com.educosystem.eduapp.viewmodels.CopyFromViewModel
import com.educosystem.eduapp.viewmodels.factories.CopyFromViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar


class CopyFromFragment : Fragment() {
    private val BUNDLE_CLONE_MODE = "clone_mode"
    private lateinit var mainActivity: MainActivity
    private lateinit var viewModel: CopyFromViewModel
    private lateinit var toolbar: Toolbar
    private lateinit var structureAdapter: BlockWithSubBlockAdapter
    private lateinit var cloneMode: CheckBox
    private lateinit var evaluationSpinner: Spinner
    private lateinit var structureRecyclerView: RecyclerView
    private lateinit var descriptionTextView: TextView
    private lateinit var navController: NavController
    private lateinit var args: CopyFromFragmentArgs
    private lateinit var coordinatorLayout: CoordinatorLayout
    private var arrayAdapter: ArrayAdapter<Evaluation>? = null

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentCopyFromBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_copy_from, container, false
        )
        val application = requireNotNull(this.activity).application
        mainActivity = requireActivity() as MainActivity
        toolbar = binding.copyFromToolbar
        cloneMode = binding.copyFromCloneModeCheckBox
        evaluationSpinner = binding.copyFromSpinner
        structureRecyclerView = binding.copyFromStructureRecyclerView
        coordinatorLayout = binding.copyFromCoordinatorLayout
        descriptionTextView = binding.copyFromDescriptionCloneMode
        binding.lifecycleOwner = viewLifecycleOwner
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        args = CopyFromFragmentArgs.fromBundle(requireArguments())
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = CopyFromViewModelFactory(
            dataSource,
            args.codSubject,
            args.codSubSubject,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(CopyFromViewModel::class.java)
        structureAdapter = BlockWithSubBlockAdapter()

        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        toolbar.navigationIcon =
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_close_black_24dp)
        binding.copyFromStructureRecyclerView.adapter = structureAdapter
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        addObservers()
        restoreContent(savedInstanceState)
        addListeners()

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.subject.observe(viewLifecycleOwner, Observer {
            it?.let {
                changeAppearanceColor(obtainColour(it.colour, resources, requireContext()))
            }
        })

        viewModel.structure.observe(viewLifecycleOwner, Observer {
            it?.let {
                structureAdapter.submitList(it)
            }
        })

        viewModel.navigateToDetailSubSubject.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                navController.navigate(
                    CopyFromFragmentDirections.actionCopyFromFragmentToDetailSubSubjectFragment(
                        codSubSubject = args.codSubSubject,
                        codSubject = args.codSubject
                    )
                )
                viewModel.doneNavigatingToDetailSubject()
            }
        })

        viewModel.navigateToBackStack.observe(viewLifecycleOwner, Observer {
            if (it) {
                navController.popBackStack()
                viewModel.doneNavigatingToBackStack()
            }
        })

        viewModel.evaluations.observe(viewLifecycleOwner, Observer {
            it?.let {
                val swap = ArrayList<Evaluation>()
                swap.add(Evaluation("", "", "", 0))
                swap.addAll(it)
                arrayAdapter = ArrayAdapter(
                    this@CopyFromFragment.requireContext(),
                    android.R.layout.simple_spinner_item,
                    swap
                ).also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    evaluationSpinner.adapter = adapter
                }
            }
        })

        viewModel.showDialog.observe(viewLifecycleOwner, Observer {
            if (it) {
                MaterialAlertDialogBuilder(context)
                    .setTitle(getString(R.string.enableCloneModeQuestion))
                    .setMessage(getString(R.string.enableCloneModeExplanation))
                    .setPositiveButton(getString(android.R.string.yes)) { _, _ ->
                        viewModel.updateSelectedItem(true)
                    }
                    .setNegativeButton(getString(android.R.string.no)) { _, _ ->
                        viewModel.startDisableCloneMode()
                    }
                    .show()
                viewModel.showDialogDone()
            }
        })

        viewModel.disableCloneMode.observe(viewLifecycleOwner, Observer {
            if (it) {
                cloneMode.isChecked = false
                viewModel.doneDisableCloneMode()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                Snackbar.make(
                    mainActivity.findViewById(android.R.id.content),
                    it,
                    Snackbar.LENGTH_SHORT
                ).show()
                viewModel.showSnackBarMessageDone()
            }
        })

        viewModel.showToastMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
                viewModel.showToastMessageDone()
            }
        })

        viewModel.restoreSelectedItem.observe(viewLifecycleOwner, Observer {
            if (it) {
                if (arrayAdapter != null) {
                    evaluationSpinner.setSelection(arrayAdapter!!.getPosition(viewModel.selectedItem.value))
                    viewModel.restoreContentDone()
                }
            }
        })

        viewModel.validData.observe(viewLifecycleOwner, Observer {
            toolbar.menu.findItem(R.id.copyMenuCopy).isEnabled = it
        })
    }

    /**
     * Restore information on configuration changes.
     *
     * @param savedInstanceState
     */
    private fun restoreContent(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            cloneMode.isChecked = it.getBoolean(BUNDLE_CLONE_MODE)
            viewModel.startRestoreContent()
        }
        if (cloneMode.isChecked) {
            descriptionTextView.text = getString(R.string.enabledCloneModeExplanation)
        } else {
            descriptionTextView.text = getString(R.string.disabledCloneModeExplanation)
        }
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {

        toolbar.setOnMenuItemClickListener {
            viewModel.startCopy(cloneMode.isChecked)
            true
        }

        toolbar.setNavigationOnClickListener {
            handleBackButton()
        }

        evaluationSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                viewModel.updateSelectedItem(Evaluation("", "", "", 0), cloneMode.isChecked)
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                viewModel.updateSelectedItem(
                    parent?.getItemAtPosition(position) as Evaluation,
                    cloneMode.isChecked
                )
            }
        }

        cloneMode.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                viewModel.startShowDialog()
                descriptionTextView.text = getString(R.string.enabledCloneModeExplanation)
            } else {
                viewModel.updateSelectedItem(false)
                descriptionTextView.text = getString(R.string.disabledCloneModeExplanation)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            handleBackButton()
        }
    }

    /**
     * Change the color appearance according to the color selected.
     *
     * @param colour
     */
    private fun changeAppearanceColor(colour: Colour) {
        toolbar.setBackgroundColor(colour.color)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            requireActivity().window.statusBarColor = colour.color
        }
        coordinatorLayout.setBackgroundColor(colour.color)
    }

    /**
     * Handle the action when back button is pressed.
     *
     */
    private fun handleBackButton() {
        viewModel.startNavigateToBackStack()
    }

    /**
     * Save the volatile data when configuration change.
     *
     * @param outState
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(BUNDLE_CLONE_MODE, cloneMode.isChecked)
    }
}
