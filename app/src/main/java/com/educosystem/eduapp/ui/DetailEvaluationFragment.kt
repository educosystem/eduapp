/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.viewpager2.widget.ViewPager2
import com.educosystem.eduapp.MainActivity
import com.educosystem.eduapp.R
import com.educosystem.eduapp.adapters.pages.PageDetailEvaluationAdapter
import com.educosystem.eduapp.data.db.EduAppDatabase
import com.educosystem.eduapp.databinding.FragmentDetailEvaluationBinding
import com.educosystem.eduapp.ui.dialogs.EditEvaluationDialog
import com.educosystem.eduapp.util.DetailEvaluationFragmentConstants
import com.educosystem.eduapp.util.PageListener
import com.educosystem.eduapp.viewmodels.DetailEvaluationViewModel
import com.educosystem.eduapp.viewmodels.factories.DetailEvaluationViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar

/**
 * A simple [Fragment] subclass.
 */
class DetailEvaluationFragment : Fragment(), DetailEvaluationFragmentConstants {
    private lateinit var mainActivity: MainActivity
    private lateinit var toolbar: Toolbar
    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var viewModel: DetailEvaluationViewModel
    private lateinit var viewPager: ViewPager2
    private lateinit var fabButton: FloatingActionButton
    private lateinit var navController: NavController
    private lateinit var args: DetailEvaluationFragmentArgs

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentDetailEvaluationBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_detail_evaluation, container, false
        )
        args = DetailEvaluationFragmentArgs.fromBundle(requireArguments())
        coordinatorLayout = binding.detailEvaluationCoordinatorLayout
        toolbar = binding.detailEvaluationToolbar
        mainActivity = requireActivity() as MainActivity
        fabButton = binding.detailEvaluationAddButton
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        val application = requireNotNull(this.activity).application
        val subjectListener = PageListener(startNavigate = {
            viewModel.startNavigateToDetailSubSubject(it)
        }, showSnackBarMessage = { it, _ ->
            viewModel.startSnackBarMessage(it)
        }, hideFabButton = { hide ->
            viewModel.hideFabButton(hide)
        })
        val pageAdapter =
            PageDetailEvaluationAdapter(
                this,
                args.codEvaluation,
                subjectListener
            )
        viewPager = binding.detailEvaluationViewPager
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = DetailEvaluationViewModelFactory(
            dataSource.evaluationDao,
            dataSource.subjectDao,
            dataSource.subSubjectDao,
            args.codEvaluation,
            args.activeSchoolYear,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(DetailEvaluationViewModel::class.java)

        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        viewPager.adapter = pageAdapter
        addObservers()
        addListeners()

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.navigateToAddSubSubjectFromEvaluation.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                navController.navigate(
                    DetailEvaluationFragmentDirections.actionDetailEvaluationFragmentToAddEvaluationFragment(
                        it.codSchoolYear,
                        it.cod
                    )
                )
                viewModel.doneNavigatingToAddSubSubjectFromEvaluation()
            }
        })

        viewModel.navigateToDetailSubSubject.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                navController.navigate(
                    DetailEvaluationFragmentDirections.actionDetailEvaluationFragmentToDetailSubSubjectFragment(
                        it.codSubject,
                        it.cod,
                        viewModel.activeSchoolYear
                    )
                )
                viewModel.doneNavigatingToDetailSubSubject()
            }
        })

        viewModel.activeEvaluation.observe(viewLifecycleOwner, Observer {
            it?.let {
                toolbar.menu.clear()
                toolbar.inflateMenu(R.menu.contextual_edit)
                if (it)
                    toolbar.menu.add(R.string.markAsComplete)
                else
                    toolbar.menu.add(R.string.markAsIncomplete)
                viewModel.hideFabButton(!it)
            }
        })

        viewModel.activeLink.observe(viewLifecycleOwner, Observer {
            viewModel.hideFabButton(!it)
        })

        viewModel.showEditDialog.observe(viewLifecycleOwner, Observer {
            if (it) {
                val fragmentManager = mainActivity.supportFragmentManager
                val bundle = Bundle()
                val dialogFragment = EditEvaluationDialog()
                val transaction = fragmentManager.beginTransaction()
                bundle.putString(ARG_OBJECT, args.codEvaluation)
                dialogFragment.arguments = bundle

                transaction.setCustomAnimations(
                    R.anim.enter_save,
                    0,
                    0,
                    R.anim.pop_back_stack_exit_save
                )
                transaction
                    .add(android.R.id.content, dialogFragment)
                    .addToBackStack(null)
                    .commit()

                viewModel.doneShowEditDialog()
            }
        })

        viewModel.showDialogUpdate.observe(viewLifecycleOwner, Observer {
            if (it) {
                val messageResource = if (viewModel.activeEvaluation.value!!)
                    getString(R.string.updateToFinishEvaluationExplanation)
                else
                    getString(R.string.updateToActiveEvaluationExplanation)
                val titleResource = if (viewModel.activeEvaluation.value!!)
                    getString(R.string.finishQuestion)
                else
                    getString(R.string.incompleteQuestion)

                MaterialAlertDialogBuilder(context)
                    .setTitle(titleResource)
                    .setMessage(messageResource)
                    .setPositiveButton(
                        getString(android.R.string.yes)
                    ) { _, _ ->
                        viewModel.update()
                    }
                    .setNegativeButton(getString(android.R.string.no), null)
                    .show()
                viewModel.dialogUpdateDone()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                Snackbar.make(
                    coordinatorLayout,
                    it,
                    Snackbar.LENGTH_SHORT
                ).show()
                viewModel.showSnackBarDone()
            }
        })

        viewModel.hideButton.observe(viewLifecycleOwner, Observer {
            if (it)
                fabButton.hide()
            else
                fabButton.show()
        })
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            if (viewPager.currentItem > 0) {
                viewPager.currentItem = 0
            } else {
                this@DetailEvaluationFragment.findNavController().popBackStack()
            }
        }

        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.menuEdit -> viewModel.startShowEditDialog()
                else -> viewModel.startDialogUpdate()
            }
            true
        }
    }
}
