/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *  
 */

package com.educosystem.eduapp.ui.pages

import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.educosystem.eduapp.MainActivity
import com.educosystem.eduapp.R
import com.educosystem.eduapp.adapters.ElementAdapter
import com.educosystem.eduapp.data.db.EduAppDatabase
import com.educosystem.eduapp.data.entity.Element
import com.educosystem.eduapp.databinding.FragmentPageElementBinding
import com.educosystem.eduapp.util.*
import com.educosystem.eduapp.viewmodels.pages.PageElementViewModel
import com.educosystem.eduapp.viewmodels.pages.factories.PageElementViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class PageElementFragment(val listener: PageListener? = null) : Fragment(),
    PageElementConstants {
    private lateinit var viewModel: PageElementViewModel
    private lateinit var adapter: ElementAdapter
    private lateinit var mainActivity: MainActivity
    private lateinit var window: Window
    private lateinit var recyclerView: RecyclerView
    private var actionMode: ActionMode? = null
    private lateinit var actionModeCallback: ActionMode.Callback

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentPageElementBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_page_element,
            container,
            false
        )
        actionModeCallback = obtainActionModeObject()
        window = requireActivity().window
        mainActivity = requireActivity() as MainActivity
        recyclerView = binding.listElementRecyclerView
        val application = requireNotNull(this.activity).application
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory =
            PageElementViewModelFactory(
                dataSource.elementDao,
                arguments?.getSerializable(ARG_TYPE_LIST)!! as ListElementsTypes,
                arguments?.getString(ARG_SUBJECT),
                arguments?.getString(ARG_SUB_SUBJECT),
                arguments?.getString(ARG_BLOCK),
                arguments?.getString(ARG_SUB_BLOCK),
                application
            )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(PageElementViewModel::class.java)
        if (listener != null) {
            viewModel.setListener(listener)
        }
        adapter = ElementAdapter(
            HolderListener({ codElement ->
                viewModel.startNavigateToDetailElement(codElement)
            }, { element ->
                viewModel.activateActionMode()
                viewModel.toggleItemSelected(element)
            }),
            dataSource.subjectDao,
            viewLifecycleOwner
        )
        if (arguments?.getString(ARG_SUBJECT) == null && arguments?.getString(ARG_SUB_SUBJECT) == null
            && arguments?.getString(ARG_BLOCK) == null && arguments?.getString(ARG_SUB_BLOCK) == null
        ) {
            ItemTouchHelper(obtainItemTouchHelper()).attachToRecyclerView(recyclerView)
            changeLayoutManager()
        } else {
            adapter.setTranslucentBackground()
        }
        if (arguments?.getBoolean(ARG_TYPED_BLOCK) != null && arguments?.getBoolean(ARG_TYPED_BLOCK)!!)
            adapter.setTypedElements()
        binding.viewModel = viewModel
        recyclerView.adapter = adapter
        if (viewModel.listener != null)
            recyclerView.addOnScrollListener(RecyclerViewListener(viewModel.listener!!))
        binding.lifecycleOwner = this
        addObservers()

        return binding.root
    }

    /**
     * Change the layout manager for elements list adapter.
     *
     */
    private fun changeLayoutManager() {
        if (mainActivity.sharedPreferences.getInt(
                mainActivity.SP_ELEMENT_LAYOUT,
                1
            ) == LINEAR_LAYOUT_ELEMENTS
        ) {
            recyclerView.layoutManager = LinearLayoutManager(requireContext())
        } else {
            recyclerView.layoutManager =
                StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        }
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.elements.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.submitList(it)
                if (arguments?.getString(ARG_SUBJECT) == null && arguments?.getString(
                        ARG_SUB_SUBJECT
                    ) == null
                    && arguments?.getString(ARG_BLOCK) == null && arguments?.getString(ARG_SUB_BLOCK) == null
                )
                    viewModel.startUpdateService()
            }
        })

        viewModel.navigateToDetailElement.observe(viewLifecycleOwner, Observer {
            it?.let {
                viewModel.listener!!.navigate(it)
                viewModel.doneNavigatingToDetailElement()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                viewModel.listener!!.showMessage(it) {
                    viewModel.undoLastAction()
                }
                viewModel.showSnackBarDone()
            }
        })

        viewModel.actionModeActivated.observe(viewLifecycleOwner, Observer {
            if (it) {
                if (actionMode == null) {
                    actionMode = activity?.startActionMode(actionModeCallback)
                    if (viewModel.selectedItems.isEmpty())
                        adapter.activateActionMode()
                    else {
                        adapter.activateActionMode(viewModel.selectedItems)
                        viewModel.notifyChangeTitleActionMode()
                    }
                }
            } else {
                if (actionMode != null) {
                    adapter.actionModeDone()
                    actionMode!!.finish()
                }
            }
        })

        viewModel.selectAllItems.observe(viewLifecycleOwner, Observer {
            adapter.changeSelectedAllTo(it)
        })

        viewModel.showDialogDelete.observe(viewLifecycleOwner, Observer {
            if (it) {
                MaterialAlertDialogBuilder(context)
                    .setTitle(getString(R.string.deleteQuestion))
                    .setMessage(getString(R.string.deleteElementExplanation))
                    .setPositiveButton(
                        getString(android.R.string.yes)
                    ) { _, _ ->
                        viewModel.delete()
                    }
                    .setNegativeButton(getString(android.R.string.no), null)
                    .show()
                viewModel.dialogDeleteDone()
            }
        })

        viewModel.changeTitleActionMode.observe(viewLifecycleOwner, Observer {
            if (actionMode != null && it) {
                if (viewModel.titleActionMode > 1)
                    actionMode!!.title =
                        resources.getString(R.string.selectedTitlePlural, viewModel.titleActionMode)
                else
                    actionMode!!.title = resources.getString(R.string.selectedTitle, 1)
                viewModel.changeTitleActionModeDone()
            }
        })

        mainActivity.hideActionMode.observe(viewLifecycleOwner, Observer {
            if (it) {
                viewModel.actionModeDone()
            }
        })

        mainActivity.notifyLayoutElementsChange.observe(viewLifecycleOwner, Observer {
            if (it) {
                changeLayoutManager()
                mainActivity.receivedNotifyLayoutElementChange()
            }
        })
    }

    /**
     * @return an ActionMode.Callback object.
     */
    private fun obtainActionModeObject(): ActionMode.Callback =
        object : ActionMode.Callback {

            /**
             * Called when action mode is first created. The menu supplied will be used to
             * generate action buttons for the action mode and change appearance.
             *
             * @param mode ActionMode being created.
             * @param menu Menu used to populate action buttons.
             * @return true if the action mode should be created, false if entering this
             *              mode should be aborted.
             */
            override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                val inflater = mode?.menuInflater
                inflater?.inflate(R.menu.contextual_delete_finish, menu)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    viewModel.setColour(window.statusBarColor)
                    window.statusBarColor =
                        ContextCompat.getColor(context!!, R.color.secondaryLightColor)
                }
                if (!viewModel.menuFinishElement) {
                    val menuItem = menu?.findItem(R.id.menuMarkAsComplete)
                    menuItem?.icon =
                        ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.ic_assignment_black_24dp,
                            null
                        )
                    menuItem?.setTitle(R.string.markAsIncomplete)
                }
                return true
            }

            /**
             * Called to refresh an action mode's action menu whenever it is invalidated.
             *
             * @param mode ActionMode being prepared.
             * @param menu Menu used to populate action buttons.
             * @return true if the menu or action mode was updated, false otherwise.
             */
            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                return false
            }

            /**
             * Called to report a user click on an action button.
             *
             * @param mode The current ActionMode.
             * @param item The item that was clicked.
             * @return true if this callback handled the event, false if the standard MenuItem
             *          invocation should continue.
             */
            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
                when (item?.itemId) {
                    R.id.menuSelectAll -> {
                        viewModel.selectAll()
                        viewModel.selectAllDone()
                    }
                    R.id.menuDelete -> {
                        viewModel.startDialogDelete()
                    }
                    R.id.menuMarkAsComplete -> {
                        viewModel.update()
                    }
                }
                return true
            }

            /**
             * Called when an action mode is about to be exited and destroyed.
             * Appearance color is restored too.
             *
             * @param mode The current ActionMode being destroyed.
             */
            override fun onDestroyActionMode(mode: ActionMode?) {
                actionMode = null
                viewModel.actionModeDone()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window.statusBarColor = viewModel.colour
                }
                adapter.actionModeDone()
            }
        }

    /**
     * @return an ItemTouchHelper.SimpleCallback object.
     */
    private fun obtainItemTouchHelper(): ItemTouchHelper.SimpleCallback = object :
        ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

        /**
         * When the item change their position. In this case is not handled.
         *
         * @param recyclerView The RecyclerView to which ItemTouchHelper is attached to.
         * @param viewHolder The ViewHolder which is being dragged by the user.
         * @param target The ViewHolder over which the currently active item is being
         *               dragged.
         * @return always false in this case.
         */
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            return false
        }

        /**
         * Called when a ViewHolder is swiped by the user.
         * Only if the [swipeDir] is from left, right, end or start this method will be called.
         *
         * @param viewHolder The ViewHolder which has been swiped by the user.
         * @param swipeDir  The direction to which the ViewHolder is swiped.
         */
        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
            val position = viewHolder.adapterPosition //get position which is swipe
            val element = adapter.currentList[position] as Element
            if (swipeDir == ItemTouchHelper.LEFT || swipeDir == ItemTouchHelper.RIGHT
                || swipeDir == ItemTouchHelper.START || swipeDir == ItemTouchHelper.END
            ) {
                viewModel.update(element)
            }
        }
    }
}