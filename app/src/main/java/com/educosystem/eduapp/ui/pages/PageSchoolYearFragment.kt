/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.ui.pages

import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.educosystem.eduapp.MainActivity
import com.educosystem.eduapp.R
import com.educosystem.eduapp.adapters.SchoolYearAdapter
import com.educosystem.eduapp.data.db.EduAppDatabase
import com.educosystem.eduapp.databinding.FragmentPageSchoolYearBinding
import com.educosystem.eduapp.util.HolderListener
import com.educosystem.eduapp.util.PageListSchoolYearConstants
import com.educosystem.eduapp.util.PageListener
import com.educosystem.eduapp.util.RecyclerViewListener
import com.educosystem.eduapp.viewmodels.pages.PageSchoolYearViewModel
import com.educosystem.eduapp.viewmodels.pages.factories.PageSchoolYearViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class PageSchoolYearFragment(val listener: PageListener? = null) : Fragment(),
    PageListSchoolYearConstants {

    private lateinit var viewModel: PageSchoolYearViewModel
    private lateinit var adapter: SchoolYearAdapter
    private lateinit var mainActivity: MainActivity
    private lateinit var window: Window
    private var actionMode: ActionMode? = null
    private lateinit var actionModeCallback: ActionMode.Callback

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentPageSchoolYearBinding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_page_school_year,
                container,
                false
            )
        actionModeCallback = obtainActionModeObject()
        window = requireActivity().window
        mainActivity = requireActivity() as MainActivity
        val application = requireNotNull(this.activity).application
        val dataSource = EduAppDatabase.getInstance(
            application
        ).schoolYearDao
        val viewModelFactory =
            PageSchoolYearViewModelFactory(
                dataSource,
                arguments?.getBoolean(ARG_OBJECT)!!,
                application
            )
        viewModel =
            ViewModelProvider(this, viewModelFactory)
                .get(PageSchoolYearViewModel::class.java)
        if (listener != null) {
            viewModel.setListener(listener)
        }
        adapter = SchoolYearAdapter(
            HolderListener({ codSchoolYear ->
                viewModel.startNavigateToDetailSchoolYear(codSchoolYear)
            }, { schoolYear ->
                viewModel.activateActionMode()
                viewModel.toggleItemSelected(schoolYear)
            }),
            viewLifecycleOwner
        )
        binding.viewModel = viewModel
        binding.listSchoolYearRecyclerView.adapter = adapter
        if (viewModel.listener != null)
            binding.listSchoolYearRecyclerView.addOnScrollListener(
                RecyclerViewListener(
                    viewModel.listener!!
                )
            )
        binding.lifecycleOwner = this
        addObservers()

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.schoolYears.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.submitList(it)
            }
        })

        viewModel.navigateToDetailSchoolYear.observe(viewLifecycleOwner, Observer {
            it?.let {
                viewModel.listener!!.navigate(it)
                viewModel.doneNavigatingToDetailSchoolYear()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                viewModel.listener!!.showMessage(it)
                viewModel.showSnackBarDone()
            }
        })

        viewModel.actionModeActivated.observe(viewLifecycleOwner, Observer {
            if (it) {
                if (actionMode == null) {
                    actionMode = activity?.startActionMode(actionModeCallback)
                    if (viewModel.selectedItems.isEmpty())
                        adapter.activateActionMode()
                    else {
                        adapter.activateActionMode(viewModel.selectedItems)
                        viewModel.notifyChangeTitleActionMode()
                    }
                }
            } else {
                if (actionMode != null) {
                    adapter.actionModeDone()
                    actionMode!!.finish()
                }
            }
        })

        viewModel.selectAllItems.observe(viewLifecycleOwner, Observer {
            adapter.changeSelectedAllTo(it)
        })

        viewModel.showDialogUpdate.observe(viewLifecycleOwner, Observer {
            if (it) {
                val messageResource = if (viewModel.active)
                    getString(R.string.updateToFinishSchoolYearExplanation)
                else
                    getString(R.string.updateToActiveSchoolYearExplanation)
                val titleResource = if (viewModel.active)
                    getString(R.string.finishQuestion)
                else
                    getString(R.string.incompleteQuestion)

                MaterialAlertDialogBuilder(
                    context
                )
                    .setTitle(titleResource)
                    .setMessage(messageResource)
                    .setPositiveButton(
                        getString(android.R.string.yes)
                    ) { _, _ ->
                        viewModel.update()
                    }
                    .setNegativeButton(getString(android.R.string.no), null)
                    .show()
                viewModel.dialogUpdateDone()
            }
        })

        viewModel.showDialogDelete.observe(viewLifecycleOwner, Observer {
            if (it) {
                MaterialAlertDialogBuilder(
                    context
                )
                    .setTitle(getString(R.string.deleteQuestion))
                    .setMessage(getString(R.string.deleteSchoolYearExplanation))
                    .setPositiveButton(
                        getString(android.R.string.yes)
                    ) { _, _ ->
                        viewModel.delete()
                    }
                    .setNegativeButton(getString(android.R.string.no), null)
                    .show()
                viewModel.dialogDeleteDone()
            }
        })

        viewModel.changeTitleActionMode.observe(viewLifecycleOwner, Observer {
            if (actionMode != null && it) {
                if (viewModel.titleActionMode > 1)
                    actionMode!!.title =
                        resources.getString(R.string.selectedTitlePlural, viewModel.titleActionMode)
                else
                    actionMode!!.title = resources.getString(R.string.selectedTitle, 1)
                viewModel.changeTitleActionModeDone()
            }
        })

        mainActivity.hideActionMode.observe(viewLifecycleOwner, Observer {
            if (it) {
                viewModel.actionModeDone()
            }
        })
    }

    /**
     * @return an ActionMode.Callback object.
     */
    private fun obtainActionModeObject(): ActionMode.Callback =
        object : ActionMode.Callback {

            /**
             * Called when action mode is first created. The menu supplied will be used to
             * generate action buttons for the action mode and change appearance.
             *
             * @param mode ActionMode being created.
             * @param menu Menu used to populate action buttons.
             * @return true if the action mode should be created, false if entering this
             *              mode should be aborted.
             */
            override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                val inflater = mode?.menuInflater
                inflater?.inflate(R.menu.contextual_delete_finish, menu)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window.statusBarColor =
                        ContextCompat.getColor(
                            context!!,
                            R.color.secondaryLightColor
                        )
                }
                if (!viewModel.active) {
                    val menuItem = menu?.findItem(R.id.menuMarkAsComplete)
                    menuItem?.icon =
                        ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.ic_assignment_black_24dp,
                            null
                        )
                    menuItem?.setTitle(R.string.markAsIncomplete)
                }
                return true
            }

            /**
             * Called to refresh an action mode's action menu whenever it is invalidated.
             *
             * @param mode ActionMode being prepared.
             * @param menu Menu used to populate action buttons.
             * @return true if the menu or action mode was updated, false otherwise.
             */
            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                return false
            }

            /**
             * Called to report a user click on an action button.
             *
             * @param mode The current ActionMode.
             * @param item The item that was clicked.
             * @return true if this callback handled the event, false if the standard MenuItem
             *          invocation should continue.
             */
            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
                when (item?.itemId) {
                    R.id.menuSelectAll -> {
                        viewModel.selectAll()
                        viewModel.selectAllDone()
                    }
                    R.id.menuMarkAsComplete -> {
                        viewModel.startDialogUpdate()
                    }
                    R.id.menuDelete -> {
                        viewModel.startDialogDelete()
                    }
                }
                return true
            }

            /**
             * Called when an action mode is about to be exited and destroyed.
             * Appearance color is restored too.
             *
             * @param mode The current ActionMode being destroyed.
             */
            override fun onDestroyActionMode(mode: ActionMode?) {
                actionMode = null
                viewModel.actionModeDone()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window.statusBarColor =
                        ContextCompat.getColor(
                            context!!,
                            R.color.primaryColor
                        )
                }
                adapter.actionModeDone()
            }
        }
}