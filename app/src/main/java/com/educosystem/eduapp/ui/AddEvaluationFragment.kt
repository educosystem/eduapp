/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *  
 */

package com.educosystem.eduapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.CheckBox
import android.widget.NumberPicker
import android.widget.TextView
import android.widget.Toast
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.educosystem.eduapp.MainActivity
import com.educosystem.eduapp.R
import com.educosystem.eduapp.adapters.SubjectAdapterForAddEvaluation
import com.educosystem.eduapp.data.db.EduAppDatabase
import com.educosystem.eduapp.databinding.FragmentAddEvaluationBinding
import com.educosystem.eduapp.util.HolderListener
import com.educosystem.eduapp.viewmodels.AddEvaluationViewModel
import com.educosystem.eduapp.viewmodels.factories.AddEvaluationViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar


class AddEvaluationFragment : Fragment() {
    private val BUNDLE_TITLE = "title"
    private val BUNDLE_POSITION = "position"
    private val BUNDLE_COPY = "copy"
    private val BUNDLE_COPY_EXTRAORDINARY = "copy_extraordinary"
    private lateinit var mainActivity: MainActivity
    private lateinit var viewModel: AddEvaluationViewModel
    private lateinit var toolbar: Toolbar
    private lateinit var adapter: SubjectAdapterForAddEvaluation
    private lateinit var titleTextView: TextView
    private lateinit var extraordinaryCheckBox: CheckBox
    private lateinit var navController: NavController
    private lateinit var args: AddEvaluationFragmentArgs
    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var numberPicker: NumberPicker
    private lateinit var copyCheckBox: CheckBox
    private lateinit var copyExtraordinaryCheckBox: CheckBox

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentAddEvaluationBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_add_evaluation, container, false
        )
        val application = requireNotNull(this.activity).application
        mainActivity = requireActivity() as MainActivity
        toolbar = binding.addEvaluationToolbar
        titleTextView = binding.addEvaluationName
        extraordinaryCheckBox = binding.addEvaluationExtraordinary
        coordinatorLayout = binding.addEvaluationCoordinatorLayout
        numberPicker = binding.addEvaluationPicker
        copyCheckBox = binding.addEvaluationCopy
        copyExtraordinaryCheckBox = binding.addEvaluationCopyExtraordinary
        binding.lifecycleOwner = viewLifecycleOwner
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        args = AddEvaluationFragmentArgs.fromBundle(requireArguments())
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = AddEvaluationViewModelFactory(
            dataSource,
            args.codSchoolYear,
            args.codEvaluation,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(AddEvaluationViewModel::class.java)
        adapter = SubjectAdapterForAddEvaluation(
            HolderListener({
                // For simplicity this function is not implemented, instead we use the long click for a single click
            }, {
                viewModel.toggleItemSelected(it)
                evaluateData()
            }),
            viewLifecycleOwner
        )

        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        toolbar.navigationIcon =
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_close_black_24dp)
        binding.addEvaluationRecyclerView.adapter = adapter
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        addObservers()
        restoreContent(savedInstanceState)
        addListeners()

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.evaluation.observe(viewLifecycleOwner, Observer {
            it?.let {
                toolbar.title = getString(R.string.linkNewSubjects)
            }
        })

        viewModel.navigateToDetailEvaluation.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                hideKeyboard()
                navController.navigate(
                    AddEvaluationFragmentDirections.actionAddEvaluationFragmentToDetailEvaluationFragment(
                        codEvaluation = it
                    )
                )
                viewModel.doneNavigatingToDetailEvaluation()
            }
        })

        viewModel.navigateToBackStack.observe(viewLifecycleOwner, Observer {
            if (it) {
                hideKeyboard()
                navController.popBackStack()
                viewModel.doneNavigatingToBackStack()
            }
        })

        viewModel.subjects.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.submitList(it)
                if (viewModel.selectAll.value!!) {
                    viewModel.selectAllSubjects()
                    evaluateData()
                    viewModel.doneSelectAll()
                }
            }
        })

        viewModel.numberOfEvaluations.observe(viewLifecycleOwner, Observer {
            it?.let {
                numberPicker.minValue = 1
                numberPicker.maxValue = it + 1
                numberPicker.wrapSelectorWheel = true
                if (viewModel.positionSelected == 0) {
                    numberPicker.value = it + 1
                } else {
                    numberPicker.value = viewModel.positionSelected
                }
                if (viewModel.generateName) {
                    titleTextView.text = getString(R.string.nameOfEvaluation, it + 1)
                    viewModel.doneGenerateName()
                }
            }
        })

        viewModel.showDialog.observe(viewLifecycleOwner, Observer {
            if (it) {
                MaterialAlertDialogBuilder(context)
                    .setMessage(getString(R.string.discardQuestion))
                    .setPositiveButton(
                        getString(R.string.continueEditing)
                        , null
                    )
                    .setNegativeButton(getString(R.string.discard)) { _, _ ->
                        viewModel.startNavigateToBackStack()
                    }
                    .show()
                viewModel.showDialogDone()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                Snackbar.make(
                    mainActivity.findViewById(android.R.id.content),
                    it,
                    Snackbar.LENGTH_SHORT
                ).show()
                viewModel.showSnackBarMessageDone()
            }
        })

        viewModel.showToastMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
                viewModel.showToastMessageDone()
            }
        })

        viewModel.restoreSelectedItems.observe(viewLifecycleOwner, Observer {
            if (it) {
                adapter.changeSelected(viewModel.selectedItems)
                viewModel.restoreContentDone()
            }
        })

        viewModel.validData.observe(viewLifecycleOwner, Observer {
            toolbar.menu.findItem(R.id.saveMenuSave).isEnabled = it
        })
    }

    /**
     * Hide the keyboard before exit from dialog for prevent a keyboard opened.
     *
     */
    private fun hideKeyboard() {
        val iim: InputMethodManager =
            getSystemService(requireContext(), InputMethodManager::class.java)!!
        iim.hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    /**
     * Restore information on configuration changes.
     *
     * @param savedInstanceState
     */
    private fun restoreContent(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            titleTextView.text = it.getString(BUNDLE_TITLE)
            extraordinaryCheckBox.isChecked = viewModel.extraordinaryEvaluation
            numberPicker.value = it.getInt(BUNDLE_POSITION)
            copyCheckBox.isChecked = it.getBoolean(BUNDLE_COPY)
            copyExtraordinaryCheckBox.isChecked = it.getBoolean(BUNDLE_COPY_EXTRAORDINARY)
            viewModel.startRestoreContent()
        }
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        titleTextView.addTextChangedListener { evaluateData() }

        toolbar.setOnMenuItemClickListener {
            viewModel.saveEvaluation(
                titleTextView.text.toString(),
                numberPicker.value,
                copyCheckBox.isChecked,
                copyExtraordinaryCheckBox.isChecked
            )
            true
        }

        toolbar.setNavigationOnClickListener {
            handleBackButton()
        }

        extraordinaryCheckBox.setOnCheckedChangeListener { _, isChecked ->
            viewModel.updateExtraordinaryEvaluationCheck(isChecked)
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            handleBackButton()
        }
    }

    /**
     * Simply call to viewModel evaluate data function for do the checks
     *
     */
    private fun evaluateData() {
        viewModel.evaluateData(
            titleTextView.text.toString()
        )
    }

    /**
     * Handle the action when back button is pressed.
     *
     */
    private fun handleBackButton() {
        if (viewModel.validData.value!!) {
            viewModel.startShowDialog()
        } else {
            viewModel.startNavigateToBackStack()
        }
    }

    /**
     * Save the volatile data when configuration change.
     *
     * @param outState
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        viewModel.savePositionSelected(numberPicker.value)
        outState.let {
            it.putString(BUNDLE_TITLE, titleTextView.text.toString())
            it.putInt(BUNDLE_POSITION, numberPicker.value)
            it.putBoolean(BUNDLE_COPY, copyCheckBox.isChecked)
            it.putBoolean(BUNDLE_COPY_EXTRAORDINARY, copyExtraordinaryCheckBox.isChecked)
        }
    }
}
