/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.ui

import com.educosystem.eduapp.util.ListElementsTypes

class PendingGradesElementFragment : MainFragment() {
    override val typeList: ListElementsTypes = ListElementsTypes.PENDING_GRADES
    override val canAdd: Boolean = false

    /**
     * Navigate to detail element.
     * Overrated for prevent crash an do the correct action navigation.
     *
     * @param it cod of the element.
     */
    override fun navigateToDetailElement(it: String) {
        navController.navigate(
            PendingGradesElementFragmentDirections.actionPendingGradesElementFragmentToDetailElementFragment(
                it
            )
        )
    }
}