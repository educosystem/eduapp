/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.ui

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.viewpager2.widget.ViewPager2
import com.educosystem.eduapp.MainActivity
import com.educosystem.eduapp.R
import com.educosystem.eduapp.adapters.pages.PageDetailSubBlockAdapter
import com.educosystem.eduapp.data.db.EduAppDatabase
import com.educosystem.eduapp.data.entity.SubBlock
import com.educosystem.eduapp.databinding.FragmentDetailSubBlockBinding
import com.educosystem.eduapp.ui.dialogs.EditBlockDialog
import com.educosystem.eduapp.util.EditBlockDialogConstants
import com.educosystem.eduapp.util.PageListener
import com.educosystem.eduapp.util.TYPE_HOMEWORK
import com.educosystem.eduapp.util.obtainColour
import com.educosystem.eduapp.viewmodels.DetailSubBlockViewModel
import com.educosystem.eduapp.viewmodels.factories.DetailSubBlockViewModelFactory
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

/**
 * A simple [Fragment] subclass.
 */
class DetailSubBlockFragment : Fragment(), EditBlockDialogConstants {
    private lateinit var mainActivity: MainActivity
    private lateinit var toolbar: Toolbar
    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var viewModel: DetailSubBlockViewModel
    private lateinit var viewPager: ViewPager2
    private lateinit var fabButton: FloatingActionButton
    private lateinit var tabLayout: TabLayout
    private lateinit var navController: NavController
    private lateinit var args: DetailSubBlockFragmentArgs

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentDetailSubBlockBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_detail_sub_block, container, false
        )
        args = DetailSubBlockFragmentArgs.fromBundle(requireArguments())
        coordinatorLayout = binding.detailSubBlockCoordinatorLayout
        toolbar = binding.detailSubBlockToolbar
        mainActivity = requireActivity() as MainActivity
        fabButton = binding.detailSubBlockAddButton
        tabLayout = binding.detailSubBlockTabLayout
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        val application = requireNotNull(this.activity).application
        viewPager = binding.detailSubBlockViewPager
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = DetailSubBlockViewModelFactory(
            dataSource.subjectDao,
            dataSource.blockDao,
            dataSource.subBlockDao,
            args.active,
            args.codSubject,
            args.codSubBlock,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(DetailSubBlockViewModel::class.java)

        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        toolbar.inflateMenu(R.menu.contextual_edit)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        addObservers()
        addListeners()
        if (viewModel.block.value != null && viewModel.subBlock.value != null)
            loadViewPager(viewModel.subBlock.value!!)

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.subject.observe(viewLifecycleOwner, Observer {
            val colour = obtainColour(it.colour, resources, requireContext())
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                requireActivity().window.statusBarColor = colour.color
            }
            toolbar.setBackgroundColor(colour.color)
            tabLayout.setBackgroundColor(colour.color)
            coordinatorLayout.setBackgroundColor(colour.color)
        })

        viewModel.subBlock.observe(viewLifecycleOwner, Observer {
            if (it != null && viewModel.viewPageLoad && viewModel.block.value != null) {
                loadViewPager(it)
                viewModel.doneLoadViewPager()
            }
        })

        viewModel.block.observe(viewLifecycleOwner, Observer {
            if (it != null && viewModel.viewPageLoad && viewModel.subBlock.value != null) {
                loadViewPager(viewModel.subBlock.value!!)
                viewModel.doneLoadViewPager()
            }
        })

        viewModel.navigateToAddElement.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                navController.navigate(
                    DetailSubBlockFragmentDirections.actionDetailSubBlockFragmentToAddElementFragment(
                        args.codSubject,
                        null,
                        null,
                        it,
                        null
                    )
                )
                viewModel.doneNavigatingToAddElement()
            }
        })

        viewModel.navigateToDetailElement.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                navController.navigate(
                    DetailSubBlockFragmentDirections.actionDetailSubBlockFragmentToDetailElementFragment(
                        it
                    )
                )
                viewModel.doneNavigatingToDetailElement()
            }
        })

        viewModel.showEditDialog.observe(viewLifecycleOwner, Observer { showDialog ->
            if (showDialog) {
                val colour =
                    obtainColour(viewModel.subject.value!!.colour, resources, requireContext())
                val fragmentManager = mainActivity.supportFragmentManager
                val bundle = Bundle()
                val dialogFragment = EditBlockDialog()
                val transaction = fragmentManager.beginTransaction()
                bundle.let {
                    it.putString(ARG_BLOCK, viewModel.block.value!!.cod)
                    it.putString(ARG_SUB_BLOCK, args.codSubBlock)
                    it.putInt(ARG_COLOR, colour.color)
                }
                dialogFragment.arguments = bundle

                transaction.setCustomAnimations(
                    R.anim.enter_save,
                    0,
                    0,
                    R.anim.pop_back_stack_exit_save
                )
                transaction
                    .add(android.R.id.content, dialogFragment)
                    .addToBackStack(null)
                    .commit()

                viewModel.doneShowEditDialog()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if (viewModel.function == null)
                    Snackbar.make(
                        coordinatorLayout,
                        it,
                        Snackbar.LENGTH_SHORT
                    ).show()
                else
                    Snackbar.make(
                        coordinatorLayout,
                        it,
                        Snackbar.LENGTH_SHORT
                    ).setAction(R.string.undo) {
                        viewModel.function!!.invoke()
                        viewModel.doneUseFunction()
                    }.show()
                viewModel.showSnackBarDone()
            }
        })

        viewModel.hideButton.observe(viewLifecycleOwner, Observer {
            if (it)
                fabButton.hide()
            else
                fabButton.show()
        })
    }

    /**
     * Load the listeners, the view pager adapter and the tabLayout
     *
     * @param subBlock
     */
    private fun loadViewPager(subBlock: SubBlock) {
        val elementListener = PageListener(startNavigate = {
            viewModel.startNavigateToDetailElement(it)
        }, showSnackBarMessage = { it, function ->
            viewModel.updateFunction(function)
            viewModel.startSnackBarMessage(it)
        }, hideFabButton = { hide ->
            viewModel.hideFabButton(hide)
        })
        val pageAdapter =
            PageDetailSubBlockAdapter(
                this,
                args.codSubBlock,
                viewModel.isHomeworkType(),
                elementListener
            )
        viewPager.adapter = pageAdapter
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position) {
                0 -> tab.setText(R.string.activePageElements)
                1 -> tab.setText(R.string.delayedPageElements)
                2 -> if (subBlock.type == TYPE_HOMEWORK) tab.setText(R.string.finishedPageElements)
                else tab.setText(R.string.withoutGradePageElements)
                3 -> tab.setText(R.string.finishedPageElements)
            }
        }.attach()
    }

    /**
     * Try to show fab button hiding and showing for make the necessary checks.
     *
     */
    private fun tryShowButton() {
        viewModel.hideFabButton(true)
        viewModel.hideFabButton(false)
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        viewPager.registerOnPageChangeCallback(obtainViewPagerCallbackObject())
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            if (viewPager.currentItem > 0) {
                viewPager.currentItem = 0
            } else {
                this@DetailSubBlockFragment.findNavController().popBackStack()
            }
        }

        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.menuEdit -> viewModel.startShowEditDialog()
            }
            true
        }
    }

    /**
     * @return an ViewPager2.OnPageChangeCallback object.
     */
    private fun obtainViewPagerCallbackObject(): ViewPager2.OnPageChangeCallback =
        object : ViewPager2.OnPageChangeCallback() {

            /**
             * Perform the necessary action to maintain the correct behaviour.
             *
             * @param position of the actual page. Start in 0.
             */
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (viewModel.oldPosition != position) {
                    mainActivity.requestHideActionMode()
                    viewModel.setLastPosition(position)
                    tryShowButton()
                }
            }
        }
}
