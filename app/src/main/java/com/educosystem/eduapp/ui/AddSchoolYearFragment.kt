/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.CheckBox
import android.widget.NumberPicker
import android.widget.TextView
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.educosystem.eduapp.MainActivity
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.db.EduAppDatabase
import com.educosystem.eduapp.databinding.FragmentAddSchoolYearBinding
import com.educosystem.eduapp.viewmodels.AddSchoolYearViewModel
import com.educosystem.eduapp.viewmodels.factories.AddSchoolYearViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar


class AddSchoolYearFragment : Fragment() {
    private val BUNDLE_TITLE = "title"
    private val BUNDLE_NUMBER = "number"
    private val BUNDLE_CHECKED = "checkBox"
    private lateinit var mainActivity: MainActivity
    private lateinit var viewModel: AddSchoolYearViewModel
    private lateinit var toolbar: Toolbar
    private lateinit var numberPicker: NumberPicker
    private lateinit var titleTextView: TextView
    private lateinit var checkBox: CheckBox
    private lateinit var navController: NavController

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentAddSchoolYearBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_add_school_year, container, false
        )
        val application = requireNotNull(this.activity).application
        mainActivity = requireActivity() as MainActivity
        toolbar = binding.addSchoolYearToolbar
        numberPicker = binding.addSchoolYearPicker
        titleTextView = binding.addSchoolYearName
        checkBox = binding.addSchoolYearCheckBox
        binding.lifecycleOwner = viewLifecycleOwner
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = AddSchoolYearViewModelFactory(
            dataSource.schoolYearDao,
            dataSource.evaluationDao,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(AddSchoolYearViewModel::class.java)

        numberPicker.minValue = 1
        numberPicker.maxValue = 10
        numberPicker.wrapSelectorWheel = true
        numberPicker.value = 3
        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        toolbar.navigationIcon =
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_close_black_24dp)
        addObservers()
        restoreContent(savedInstanceState)
        addListeners()

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.navigateToDetailSchoolYear.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                hideKeyboard()
                navController.navigate(
                    AddSchoolYearFragmentDirections
                        .actionAddSchoolYearFragmentToDetailSchoolYearFragment(it)
                )
                viewModel.doneNavigatingToDetailSchoolYear()
            }
        })

        viewModel.navigateToBackStack.observe(viewLifecycleOwner, Observer {
            if (it) {
                hideKeyboard()
                navController.popBackStack()
                viewModel.doneNavigatingToBackStack()
            }
        })

        viewModel.showDialog.observe(viewLifecycleOwner, Observer {
            if (it) {
                MaterialAlertDialogBuilder(context)
                    .setMessage(getString(R.string.discardQuestion))
                    .setPositiveButton(
                        getString(R.string.continueEditing)
                        , null
                    )
                    .setNegativeButton(getString(R.string.discard)) { _, _ ->
                        viewModel.startNavigateToBackStack()
                    }
                    .show()
                viewModel.showDialogDone()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            it?.let {
                Snackbar.make(
                    mainActivity.findViewById(android.R.id.content),
                    it,
                    Snackbar.LENGTH_SHORT
                ).show()
                viewModel.showSnackBarMessageDone()
            }
        })

        viewModel.validData.observe(viewLifecycleOwner, Observer {
            toolbar.menu.findItem(R.id.saveMenuSave).isEnabled = it
        })

        viewModel.clearData.observe(viewLifecycleOwner, Observer {
            if (it) {
                clear()
                viewModel.clearDataDone()
            }
        })
    }

    /**
     * Hide the keyboard before exit from dialog for prevent a keyboard opened.
     *
     */
    private fun hideKeyboard() {
        val iim: InputMethodManager =
            getSystemService(requireContext(), InputMethodManager::class.java)!!
        iim.hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    /**
     * Clear the text.
     *
     */
    private fun clear() {
        titleTextView.text = ""
        numberPicker.value = 3
        checkBox.isSelected = false
    }

    /**
     * Restore information on configuration changes.
     *
     * @param savedInstanceState
     */
    private fun restoreContent(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            titleTextView.text = it.getString(BUNDLE_TITLE)
            numberPicker.value = it.getInt(BUNDLE_NUMBER)
            checkBox.isChecked = it.getBoolean(BUNDLE_CHECKED)
        }
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        titleTextView.addTextChangedListener {
            viewModel.evaluateData(it.toString())
        }

        toolbar.setOnMenuItemClickListener {
            viewModel.saveSchoolYear(
                titleTextView.text.toString(),
                numberPicker.value,
                checkBox.isChecked
            )
            true
        }

        toolbar.setNavigationOnClickListener {
            handleBackButton()
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            handleBackButton()
        }
    }

    /**
     * Handle the action when back button is pressed.
     *
     */
    private fun handleBackButton() {
        if (viewModel.validData.value!!) {
            viewModel.startShowDialog()
        } else {
            viewModel.startNavigateToBackStack()
        }
    }

    /**
     * Save the volatile data when configuration change.
     *
     * @param outState
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.let {
            it.putString(BUNDLE_TITLE, titleTextView.text.toString())
            it.putInt(BUNDLE_NUMBER, numberPicker.value)
            it.putBoolean(BUNDLE_CHECKED, checkBox.isChecked)
        }
    }
}
