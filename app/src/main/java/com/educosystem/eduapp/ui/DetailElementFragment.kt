/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.ui

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.educosystem.eduapp.MainActivity
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.db.EduAppDatabase
import com.educosystem.eduapp.data.entity.Subject
import com.educosystem.eduapp.databinding.FragmentDetailElementBinding
import com.educosystem.eduapp.util.TYPE_HOMEWORK
import com.educosystem.eduapp.util.obtainColour
import com.educosystem.eduapp.viewmodels.DetailElementViewModel
import com.educosystem.eduapp.viewmodels.factories.DetailElementViewModelFactory


class DetailElementFragment : Fragment() {
    private lateinit var mainActivity: MainActivity
    private lateinit var viewModel: DetailElementViewModel
    private lateinit var toolbar: Toolbar
    private lateinit var navController: NavController
    private lateinit var args: DetailElementFragmentArgs
    private lateinit var coordinatorLayout: CoordinatorLayout

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentDetailElementBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_detail_element, container, false
        )
        val application = requireNotNull(this.activity).application
        mainActivity = requireActivity() as MainActivity
        toolbar = binding.detailElementToolbar
        coordinatorLayout = binding.detailElementCoordinatorLayout
        binding.lifecycleOwner = viewLifecycleOwner
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        args = DetailElementFragmentArgs.fromBundle(requireArguments())
        val dataSource = EduAppDatabase.getInstance(application)
        val viewModelFactory = DetailElementViewModelFactory(
            dataSource.subjectDao,
            dataSource.elementDao,
            args.codElement,
            application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(DetailElementViewModel::class.java)

        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        addObservers()
        addListeners()
        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.subject.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                changeAppearance(it)
            }
        })

        viewModel.element.observe(viewLifecycleOwner, Observer {
            if (it != null)
                inflateToolbarMenu()
        })

        viewModel.navigateToEditFragment.observe(viewLifecycleOwner, Observer {
            if (it) {
                //TODO
                viewModel.doneNavigatingToEditFragment()
            }
        })

        viewModel.navigateToEditGrade.observe(viewLifecycleOwner, Observer {
            if (it) {
                //TODO
                viewModel.doneNavigatingToEditGrade()
            }
        })
    }

    /**
     * Change the color appearance according to the subject.
     *
     * @param subject
     */
    private fun changeAppearance(subject: Subject) {
        val colour = obtainColour(subject.colour, resources, requireContext())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            requireActivity().window.statusBarColor = colour.color
        }
        toolbar.setBackgroundColor(colour.color)
        coordinatorLayout.setBackgroundColor(colour.color)
    }

    /**
     * Inflate the toolbar menu.
     * Remove the add grade item if the element have [TYPE_HOMEWORK] type.
     *
     */
    private fun inflateToolbarMenu() {
        toolbar.menu.clear()
        toolbar.inflateMenu(R.menu.contextual_edit_add_grade)
        if (viewModel.element.value != null && viewModel.element.value!!.type == TYPE_HOMEWORK) {
            toolbar.menu.removeItem(R.id.menuEditAddGradeAdd)
        }
    }

    /**
     * Add listeners to perform actions when the user interact with the screen.
     *
     */
    private fun addListeners() {
        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.menuEditAddGradeEdit -> viewModel.startNavigateToEditFragment()
                else -> viewModel.startNavigateToEditGrade()
            }
            true
        }
    }
}
