/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.viewpager2.widget.ViewPager2
import com.educosystem.eduapp.MainActivity
import com.educosystem.eduapp.R
import com.educosystem.eduapp.adapters.pages.PageSchoolYearAdapter
import com.educosystem.eduapp.databinding.FragmentListSchoolYearBinding
import com.educosystem.eduapp.util.PageListener
import com.educosystem.eduapp.viewmodels.ListSchoolYearViewModel
import com.educosystem.eduapp.viewmodels.factories.ListSchoolYearViewModelFactory
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

/**
 * A simple [Fragment] subclass.
 */
class ListSchoolYearFragment : Fragment() {
    private lateinit var mainActivity: MainActivity
    private lateinit var toolbar: Toolbar
    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var viewModel: ListSchoolYearViewModel
    private lateinit var viewPager: ViewPager2
    private lateinit var tabLayout: TabLayout
    private lateinit var fabButton: FloatingActionButton
    private lateinit var navController: NavController

    /**
     * Prepare the view and make the necessary changes to attach correctly
     * the fragment with the viewModel.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentListSchoolYearBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_list_school_year, container, false
        )
        coordinatorLayout = binding.listSchoolYearCoordinatorLayout
        toolbar = binding.listSchoolYearToolbar
        mainActivity = requireActivity() as MainActivity
        fabButton = binding.listSchoolYearAddButton
        navController = mainActivity.findNavController(R.id.myNavHostFragment)
        val application = requireNotNull(this.activity).application
        val pageAdapter =
            PageSchoolYearAdapter(
                this,
                PageListener(startNavigate = {
                    viewModel.startNavigateToDetailSchoolYear(it)
                }, showSnackBarMessage = { it, _ ->
                    viewModel.startSnackBarMessage(it)
                }, hideFabButton = { hide ->
                    viewModel.hideFabButton(hide)
                })
            )
        viewPager = binding.listSchoolYearViewPager
        tabLayout = binding.listSchoolYearTabLayout
        val viewModelFactory = ListSchoolYearViewModelFactory(application)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(ListSchoolYearViewModel::class.java)

        toolbar.setupWithNavController(navController, mainActivity.appBarConfiguration)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        viewPager.adapter = pageAdapter
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position) {
                0 -> tab.setText(R.string.current)
                1 -> tab.setText(R.string.finished)
            }
        }.attach()
        addObservers()
        viewPager.registerOnPageChangeCallback(obtainViewPagerCallbackObject())
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            if (viewPager.currentItem > 0) {
                viewPager.currentItem = 0
            } else {
                this@ListSchoolYearFragment.findNavController().popBackStack()
            }
        }

        return binding.root
    }

    /**
     * Add the necessary observers to update the view with the data and to perform events.
     *
     */
    private fun addObservers() {
        viewModel.navigateToAddSchoolYear.observe(viewLifecycleOwner, Observer {
            if (it) {
                navController.navigate(
                    ListSchoolYearFragmentDirections.actionListActiveSchoolYearFragmentToAddSchoolYearFragment()
                )
                viewModel.doneNavigatingToAddSchoolYear()
            }
        })

        viewModel.navigateToDetailSchoolYear.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                navController.navigate(
                    ListSchoolYearFragmentDirections.actionListActiveSchoolYearFragmentToDetailSchoolYearFragment(
                        it
                    )
                )
                viewModel.doneNavigatingToDetailSchoolYear()
            }
        })

        viewModel.showSnackBarMessage.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                Snackbar.make(
                    coordinatorLayout,
                    it,
                    Snackbar.LENGTH_SHORT
                ).show()
                viewModel.showSnackBarDone()
            }
        })

        viewModel.hideButton.observe(viewLifecycleOwner, Observer {
            if (it)
                fabButton.hide()
            else
                fabButton.show()
        })
    }

    /**
     * @return an ViewPager2.OnPageChangeCallback object.
     */
    private fun obtainViewPagerCallbackObject(): ViewPager2.OnPageChangeCallback =
        object : ViewPager2.OnPageChangeCallback() {

            /**
             * Perform the necessary action to maintain the correct behaviour.
             *
             * @param position of the actual page. Start in 0.
             */
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (viewModel.oldPosition != position) {
                    mainActivity.requestHideActionMode()
                    viewModel.setLastPosition(position)
                    viewModel.hideFabButton(true)
                    viewModel.hideFabButton(false)
                }
            }
        }
}
