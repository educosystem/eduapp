/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.dao.EvaluationDao
import com.educosystem.eduapp.data.dao.SubSubjectDao
import com.educosystem.eduapp.data.dao.SubjectDao
import com.educosystem.eduapp.data.entity.Evaluation
import com.educosystem.eduapp.data.entity.SubSubject
import kotlinx.coroutines.*

class DetailEvaluationViewModel(
    private val evaluationDao: EvaluationDao,
    subjectDao: SubjectDao,
    private val subSubjectDao: SubSubjectDao,
    private val codEvaluation: String,
    var activeSchoolYear: Boolean,
    application: Application
) : AndroidViewModel(application) {
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val resources = application.resources
    private val evaluation = evaluationDao.get(codEvaluation)
    private val listAvailableSubjects =
        subjectDao.getAllPendingSubjectsFromEvaluation(codEvaluation)
    val toolbarTitle = Transformations.map(evaluation) {
        it.name
    }
    private val _navigateToAddSubSubjectFromEvaluation = MutableLiveData<Evaluation?>()
    val navigateToAddSubSubjectFromEvaluation: LiveData<Evaluation?>
        get() = _navigateToAddSubSubjectFromEvaluation
    private val _navigateToDetailSubSubject = MutableLiveData<SubSubject?>()
    val navigateToDetailSubSubject: LiveData<SubSubject?>
        get() = _navigateToDetailSubSubject
    private val _showEditDialog = MutableLiveData(false)
    val showEditDialog: LiveData<Boolean>
        get() = _showEditDialog
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    val activeEvaluation = Transformations.map(evaluation) {
        it.finishDate == null
    }
    val activeLink = Transformations.map(listAvailableSubjects) {
        it.isNotEmpty()
    }
    private val _hideButton = MutableLiveData<Boolean>()
    val hideButton: LiveData<Boolean>
        get() = _hideButton
    private val _showDialogUpdate = MutableLiveData<Boolean>()
    val showDialogUpdate: LiveData<Boolean>
        get() = _showDialogUpdate

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Calculate the action of the fab button.
     *
     */
    fun onAdd() {
        startNavigateToAddSubSubjectFromEvaluation()
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.AddSubjectFragment].
     *
     */
    private fun startNavigateToAddSubSubjectFromEvaluation() {
        _navigateToAddSubSubjectFromEvaluation.value = evaluation.value
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddSubSubjectFromEvaluation() {
        _navigateToAddSubSubjectFromEvaluation.value = null
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.DetailSubSubjectFragment].
     *
     * @param codSubject
     */
    fun startNavigateToDetailSubSubject(codSubject: String) {
        uiScope.launch {
            val subSubject = withContext(Dispatchers.IO) {
                return@withContext subSubjectDao.get(codEvaluation, codSubject)
            }
            _navigateToDetailSubSubject.value = subSubject
        }
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailSubSubject() {
        _navigateToDetailSubSubject.value = null
    }

    /**
     * Start show the edit dialog.
     *
     */
    fun startShowEditDialog() {
        _showEditDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun doneShowEditDialog() {
        _showEditDialog.value = false
    }

    /**
     * Start show message on snackBar.
     *
     * @param message
     */
    fun startSnackBarMessage(message: String) {
        _showSnackBarMessage.value = message
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Set the hide state of the fab button making necessary checks.
     *
     * @param hide status of fab button.
     */
    fun hideFabButton(hide: Boolean) {
        if (activeEvaluation.value != null && activeEvaluation.value!!
            && activeLink.value != null && activeLink.value!! || hide
        )
            _hideButton.value = hide
    }

    /**
     * Start show the update dialog.
     *
     */
    fun startDialogUpdate() {
        _showDialogUpdate.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun dialogUpdateDone() {
        _showDialogUpdate.value = false
    }

    /**
     * Update the state of this evaluation.
     *
     */
    fun update() {
        if (activeEvaluation.value!!)
            markAsFinished()
        else
            markAsIncomplete()
    }

    /**
     * Mark this evaluation as finished.
     *
     */
    private fun markAsFinished() {
        uiScope.launch {
            val evaluation = evaluation.value!!
            try {
                activeSchoolYear = withContext(Dispatchers.IO) {
                    evaluationDao.markAsFinished(evaluation)
                    evaluationDao.isSchoolYearActiveOnly(evaluation.codSchoolYear)
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }

    /**
     * Mark this evaluation as active.
     *
     */
    private fun markAsIncomplete() {
        uiScope.launch {
            val evaluation = evaluation.value!!
            try {
                activeSchoolYear = withContext(Dispatchers.IO) {
                    evaluationDao.markAsIncomplete(evaluation)
                    evaluationDao.isSchoolYearActiveOnly(evaluation.codSchoolYear)
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }
}