/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels.dialogs

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.dao.EvaluationDao
import kotlinx.coroutines.*

class EditEvaluationViewModel(
    private val dataSource: EvaluationDao,
    codEvaluation: String,
    application: Application
) : AndroidViewModel(application) {
    private val resources = application.resources
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val evaluation = dataSource.get(codEvaluation)
    val name = Transformations.map(evaluation) {
        it.name
    }
    private val _validData = MutableLiveData(false)
    val validData: LiveData<Boolean>
        get() = _validData
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _clearData = MutableLiveData(false)
    val clearData: LiveData<Boolean>
        get() = _clearData
    private val _showDialog = MutableLiveData(false)
    val showDialog: LiveData<Boolean>
        get() = _showDialog
    private val _navigateToBackStack = MutableLiveData(false)
    val navigateToBackStack: LiveData<Boolean>
        get() = _navigateToBackStack

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Evaluate if the data are different and if this are valid.
     *
     * @param title
     */
    fun evaluateData(title: String) {
        _validData.value = title.trim().isNotEmpty() && title != name.value
    }

    /**
     * Start to navigate to back stack.
     *
     */
    fun startNavigateToBackStack() {
        _clearData.value = true
        _navigateToBackStack.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToBackStack() {
        _navigateToBackStack.value = false
    }

    /**
     * Start show the confirm dialog.
     *
     */
    fun startShowDialog() {
        _showDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun showDialogDone() {
        _showDialog.value = false
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarMessageDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Finish notification of clear data for prevent unwanted behaviour.
     *
     */
    fun clearDataDone() {
        _clearData.value = false
    }

    /**
     * Edit and save changes of this evaluation.
     *
     * @param newName
     */
    fun edit(newName: String) {
        uiScope.launch {
            val evaluation = evaluation.value!!
            evaluation.name = newName.trim()
            try {
                withContext(Dispatchers.IO) {
                    dataSource.update(evaluation)
                }
                startNavigateToBackStack()
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }
}
