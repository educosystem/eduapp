/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.educosystem.eduapp.data.dao.BlockDao
import com.educosystem.eduapp.data.dao.SubBlockDao
import com.educosystem.eduapp.data.dao.SubjectDao
import com.educosystem.eduapp.util.TYPE_HOMEWORK

class DetailSubBlockViewModel(
    subjectDao: SubjectDao,
    blockDao: BlockDao,
    subBlockDao: SubBlockDao,
    val active: Boolean,
    codSubject: String,
    private val codSubBlock: String,
    application: Application
) : AndroidViewModel(application) {
    val subject = subjectDao.get(codSubject)
    val subBlock = subBlockDao.get(codSubBlock)
    val block = blockDao.getFromSubBlock(codSubBlock)
    private var _canExecuteFunction = false
    private val _navigateToDetailElement = MutableLiveData<String?>(null)
    val navigateToDetailElement: LiveData<String?>
        get() = _navigateToDetailElement
    private val _navigateToAddElement = MutableLiveData<String?>(null)
    val navigateToAddElement: LiveData<String?>
        get() = _navigateToAddElement
    private val _showEditDialog = MutableLiveData(false)
    val showEditDialog: LiveData<Boolean>
        get() = _showEditDialog
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    val toolbarTitle = Transformations.map(subBlock) {
        it.name
    }
    private var _viewPagerLoad = true
    val viewPageLoad: Boolean
        get() = _viewPagerLoad
    private var _function: (() -> Unit)? = null
    val function
        get() = _function
    private var _oldPosition = 0
    val oldPosition: Int
        get() = _oldPosition
    private val _hideButton = MutableLiveData<Boolean>()
    val hideButton: LiveData<Boolean>
        get() = _hideButton

    /**
     * Calculate the action of the fab button depending of the actual page.
     *
     */
    fun onAdd() {
        when (oldPosition) {
            0 -> startNavigateToAddElement()
        }
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.AddElementFragment].
     *
     */
    private fun startNavigateToAddElement() {
        _navigateToAddElement.value = codSubBlock
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddElement() {
        _navigateToAddElement.value = null
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.DetailElementFragment].
     *
     * @param codElement
     */
    fun startNavigateToDetailElement(codElement: String) {
        _navigateToDetailElement.value = codElement
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailElement() {
        _navigateToDetailElement.value = null
    }

    /**
     * Start show the edit dialog.
     *
     */
    fun startShowEditDialog() {
        _showEditDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun doneShowEditDialog() {
        _showEditDialog.value = false
    }

    /**
     * Start show message on snackBar and check the if the undo action should be shown.
     *
     * @param message
     */
    fun startSnackBarMessage(message: String) {
        if (_canExecuteFunction)
            _canExecuteFunction = false
        else {
            _function = null
        }
        _showSnackBarMessage.value = message
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Finish to load dynamic viewPager to prevent unwanted behaviour until a configuration change.
     *
     */
    fun doneLoadViewPager() {
        _viewPagerLoad = false
    }

    /**
     * Update the actual page position.
     *
     * @param position of the viewPager.
     */
    fun setLastPosition(position: Int) {
        _oldPosition = position
        hideFabButton(!active)
    }

    /**
     * Update the undo function to execute when the snackBar message is shown.
     *
     * @param function
     */
    fun updateFunction(function: (() -> Unit)?) {
        _function = function
        _canExecuteFunction = true
    }

    /**
     * Set to null [_function] to prevent repeated actions.
     *
     */
    fun doneUseFunction() {
        _function = null
    }

    /**
     * Set the hide state of the fab button making necessary checks.
     *
     * @param hide status of fab button.
     */
    fun hideFabButton(hide: Boolean) {
        if (active && oldPosition == 0 || hide)
            _hideButton.value = hide
    }

    /**
     *
     * @return if this subBlocks is homeWork type.
     */
    fun isHomeworkType(): Boolean =
        subBlock.value!!.type == TYPE_HOMEWORK || block.value!!.type == TYPE_HOMEWORK

}