/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.dao.EvaluationDao
import com.educosystem.eduapp.data.dao.SubSubjectDao
import com.educosystem.eduapp.data.dao.SubjectDao
import kotlinx.coroutines.*

class
DetailSubSubjectViewModel(
    subjectDao: SubjectDao,
    private val subSubjectDao: SubSubjectDao,
    private val evaluationDao: EvaluationDao,
    codSubject: String,
    var activeSchoolYear: Boolean,
    private val codSubSubject: String,
    application: Application
) : AndroidViewModel(application) {
    private val resources = application.resources
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private var _canExecuteFunction = false
    val subject = subjectDao.get(codSubject)
    val existOtherEvaluations =
        subSubjectDao.existOtherSubSubjectAndSubSubjectIsValid(codSubject, codSubSubject)
    val existOtherValidStructureEvaluations =
        subSubjectDao.existOtherSubSubjectWithValidStructure(codSubject, codSubSubject)
    val canAddElements = subSubjectDao.subSubjectCanAddElements(codSubSubject)
    private val subSubject = subSubjectDao.get(codSubSubject)
    private val _navigateToAddBlock = MutableLiveData<String?>()
    val navigateToAddBlock: LiveData<String?>
        get() = _navigateToAddBlock
    private val _navigateToDetailBlock = MutableLiveData<String?>()
    val navigateToDetailBlock: LiveData<String?>
        get() = _navigateToDetailBlock
    private val _navigateToCopyTo = MutableLiveData<String?>()
    val navigateToCopyTo: LiveData<String?>
        get() = _navigateToCopyTo
    private val _navigateToCopyFrom = MutableLiveData<String?>()
    val navigateToCopyFrom: LiveData<String?>
        get() = _navigateToCopyFrom
    private val _navigateToDetailElement = MutableLiveData<String?>(null)
    val navigateToDetailElement: LiveData<String?>
        get() = _navigateToDetailElement
    private val _navigateToAddElement = MutableLiveData<String?>(null)
    val navigateToAddElement: LiveData<String?>
        get() = _navigateToAddElement
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private var _function: (() -> Unit)? = null
    val function
        get() = _function
    private var _oldPosition = 0
    val oldPosition: Int
        get() = _oldPosition
    val active = Transformations.map(subSubject) {
        it.active
    }
    private val _notifyUpdateUI = MutableLiveData<Boolean>()
    val notifyUpdateUI: LiveData<Boolean>
        get() = _notifyUpdateUI
    private val _hideButton = MutableLiveData<Boolean>()
    val hideButton: LiveData<Boolean>
        get() = _hideButton

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Calculate the action of the fab button depending of the actual page.
     *
     */
    fun onAdd() {
        when (oldPosition) {
            0 -> startNavigateToAddElement()
            4 -> startNavigateToAddBlock()
        }
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.AddBlockFragment].
     *
     */
    private fun startNavigateToAddBlock() {
        _navigateToAddBlock.value = codSubSubject
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddBlock() {
        _navigateToAddBlock.value = null
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.DetailBlockFragment].
     *
     * @param codBlock
     */
    fun startNavigateToDetailBlock(codBlock: String) {
        uiScope.launch {
            _navigateToDetailBlock.value = codBlock
        }
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailBlock() {
        _navigateToDetailBlock.value = null
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.CopyToFragment].
     *
     */
    fun startNavigateToCopyTo() {
        _navigateToCopyTo.value = codSubSubject

    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToCopyTo() {
        _navigateToCopyTo.value = null
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.CopyFromFragment].
     *
     */
    fun startNavigateToCopyFrom() {
        _navigateToCopyFrom.value = codSubSubject

    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToCopyFrom() {
        _navigateToCopyFrom.value = null
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.AddElementFragment].
     *
     */
    private fun startNavigateToAddElement() {
        _navigateToAddElement.value = codSubSubject
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddElement() {
        _navigateToAddElement.value = null
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.DetailElementFragment].
     *
     * @param codElement
     */
    fun startNavigateToDetailElement(codElement: String) {
        _navigateToDetailElement.value = codElement
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailElement() {
        _navigateToDetailElement.value = null
    }

    /**
     * Finish notification of update ui for prevent unwanted behaviour.
     *
     */
    fun doneUpdateUI() {
        _notifyUpdateUI.value = false
    }

    /**
     * Start show message on snackBar and check the if the undo action should be shown.
     *
     * @param message
     */
    fun startSnackBarMessage(message: String) {
        if (_canExecuteFunction) {
            _canExecuteFunction = false
        } else {
            _function = null
        }
        _showSnackBarMessage.value = message
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Update the actual page position.
     *
     * @param position of the viewPager.
     */
    fun setLastPosition(position: Int) {
        _oldPosition = position
    }

    /**
     * Set the hide state of the fab button making necessary checks.
     *
     * @param hide status of fab button.
     */
    fun hideFabButton(hide: Boolean) {
        if (activeSchoolYear && (addElementCondition() || addBlockCondition())
            || hide
        )
            _hideButton.value = hide
    }

    /**
     * Condition for show the fab button in the element page.
     *
     */
    private fun addElementCondition() =
        oldPosition == 0 && canAddElements.value != null && canAddElements.value!!


    /**
     * Condition for show the fab button in the block page.
     *
     */
    private fun addBlockCondition() =
        oldPosition == 4

    /**
     * Update the undo function to execute when the snackBar message is shown.
     *
     * @param function
     */
    fun updateFunction(function: (() -> Unit)?) {
        _function = function
        _canExecuteFunction = true
    }

    /**
     * Set to null [_function] to prevent repeated actions.
     *
     */
    fun doneUseFunction() {
        _function = null
    }

    /**
     * Update the status of this subSubject to finished or active.
     *
     */
    fun startUpdate() {
        val subSubject = subSubject.value!!
        val oldValue = activeSchoolYear
        uiScope.launch {
            try {
                if (subSubject.active) {
                    withContext(Dispatchers.IO) {
                        subSubjectDao.finishAndActiveNext(subSubject)
                        if (!evaluationDao.haveActiveSubSubject(subSubject.codEvaluation)) {
                            evaluationDao.markAsFinished(evaluationDao.getOnly(subSubject.codEvaluation))
                            activeSchoolYear =
                                evaluationDao.isSchoolYearActiveOnly(
                                    evaluationDao.getOnly(
                                        subSubject.codEvaluation
                                    ).codSchoolYear
                                )
                        }
                    }
                    _notifyUpdateUI.value = true
                } else {
                    withContext(Dispatchers.IO) {
                        subSubjectDao.active(subSubject)
                        val evaluation = evaluationDao.getOnly(subSubject.codEvaluation)
                        if (evaluation.finishDate != null) {
                            evaluationDao.markAsIncomplete(evaluation)
                            activeSchoolYear =
                                evaluationDao.isSchoolYearActiveOnly(
                                    evaluationDao.getOnly(
                                        subSubject.codEvaluation
                                    ).codSchoolYear
                                )
                        }
                    }
                    _notifyUpdateUI.value = true
                }
                if (activeSchoolYear != oldValue) {
                    _showSnackBarMessage.value =
                        if (activeSchoolYear)
                            resources.getString(R.string.schoolYearActivated)
                        else
                            resources.getString(R.string.schoolYearFinished)
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }
}