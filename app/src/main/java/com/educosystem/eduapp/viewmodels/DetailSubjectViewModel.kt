/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.educosystem.eduapp.data.dao.EvaluationDao
import com.educosystem.eduapp.data.dao.SubSubjectDao
import com.educosystem.eduapp.data.dao.SubjectDao
import com.educosystem.eduapp.data.entity.SubSubject
import com.educosystem.eduapp.data.entity.Subject
import kotlinx.coroutines.*

class DetailSubjectViewModel(
    subjectDao: SubjectDao,
    evaluationDao: EvaluationDao,
    private val subSubjectDao: SubSubjectDao,
    private val codSubject: String,
    codSchoolYear: String,
    application: Application
) : AndroidViewModel(application) {
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    val subject = subjectDao.get(codSubject)
    val subSubjectForAddElements = subjectDao.subjectCanAddElements(codSubject)
    private val listAvailableEvaluations =
        evaluationDao.getAllPendingEvaluationsFromSubject(codSubject)
    private var _canExecuteFunction = false
    val toolbarTitle = Transformations.map(subject) {
        it.name
    }
    private val _navigateToAddSubSubjectFromSubject = MutableLiveData<Subject?>()
    val navigateToAddSubSubjectFromSubject: LiveData<Subject?>
        get() = _navigateToAddSubSubjectFromSubject
    private val _navigateToDetailSubSubject = MutableLiveData<SubSubject?>()
    val navigateToDetailSubSubject: LiveData<SubSubject?>
        get() = _navigateToDetailSubSubject
    private val _navigateToDetailElement = MutableLiveData<String?>(null)
    val navigateToDetailElement: LiveData<String?>
        get() = _navigateToDetailElement
    private val _navigateToAddElement = MutableLiveData<String?>()
    val navigateToAddElement: LiveData<String?>
        get() = _navigateToAddElement
    private val _showEditDialog = MutableLiveData(false)
    val showEditDialog: LiveData<Boolean>
        get() = _showEditDialog
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    val active = Transformations.map(listAvailableEvaluations) {
        it.isNotEmpty()
    }
    val activeSchoolYear = evaluationDao.isSchoolYearActive(codSchoolYear)
    private var _function: (() -> Unit)? = null
    val function
        get() = _function
    private var _oldPosition = 0
    val oldPosition: Int
        get() = _oldPosition
    private val _hideButton = MutableLiveData<Boolean>()
    val hideButton: LiveData<Boolean>
        get() = _hideButton

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Calculate the action of the fab button depending of the actual page.
     *
     */
    fun onAdd() {
        when (oldPosition) {
            0 -> startNavigateToAddElement()
            4 -> startNavigateToAddSubSubjectFromSubject()
        }
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.AddSubjectFragment].
     *
     */
    private fun startNavigateToAddSubSubjectFromSubject() {
        _navigateToAddSubSubjectFromSubject.value = subject.value
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddSubSubjectFromSubject() {
        _navigateToAddSubSubjectFromSubject.value = null
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.DetailSubSubjectFragment].
     *
     * @param codEvaluation
     */
    fun startNavigateToDetailSubSubject(codEvaluation: String) {
        uiScope.launch {
            val subSubject = withContext(Dispatchers.IO) {
                return@withContext subSubjectDao.get(codEvaluation, codSubject)
            }
            _navigateToDetailSubSubject.value = subSubject
        }
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailSubSubject() {
        _navigateToDetailSubSubject.value = null
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.AddElementFragment].
     *
     */
    private fun startNavigateToAddElement() {
        _navigateToAddElement.value = subSubjectForAddElements.value!!
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddElement() {
        _navigateToAddElement.value = null
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.DetailElementFragment].
     *
     * @param codElement
     */
    fun startNavigateToDetailElement(codElement: String) {
        _navigateToDetailElement.value = codElement
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailElement() {
        _navigateToDetailElement.value = null
    }

    /**
     * Start show the edit dialog.
     *
     */
    fun startShowEditDialog() {
        _showEditDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun doneShowEditDialog() {
        _showEditDialog.value = false
    }

    /**
     * Start show message on snackBar and check the if the undo action should be shown.
     *
     * @param message
     */
    fun startSnackBarMessage(message: String) {
        if (_canExecuteFunction)
            _canExecuteFunction = false
        else {
            _function = null
        }
        _showSnackBarMessage.value = message
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Update the actual page position.
     *
     * @param position of the viewPager.
     */
    fun setLastPosition(position: Int) {
        _oldPosition = position
    }

    /**
     * Update the undo function to execute when the snackBar message is shown.
     *
     * @param function
     */
    fun updateFunction(function: (() -> Unit)?) {
        _function = function
        _canExecuteFunction = true
    }

    /**
     * Set to null [_function] to prevent repeated actions.
     *
     */
    fun doneUseFunction() {
        _function = null
    }

    /**
     * Set the hide state of the fab button making necessary checks.
     *
     * @param hide status of fab button.
     */
    fun hideFabButton(hide: Boolean) {
        if (activeSchoolYear.value != null && activeSchoolYear.value!!
            && (addSubSubjectFromSubject() || addElementCondition())
            || hide
        )
            _hideButton.value = hide
    }

    /**
     * Condition for show the fab button in the subSubject page.
     *
     */
    private fun addSubSubjectFromSubject() =
        active.value != null && active.value!! && oldPosition == 4

    /**
     * Condition for show the fab button in the element page.
     *
     */
    private fun addElementCondition() =
        oldPosition == 0 && subSubjectForAddElements.value != null
}
