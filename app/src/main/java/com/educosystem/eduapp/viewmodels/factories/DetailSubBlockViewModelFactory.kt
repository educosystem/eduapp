/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.viewmodels.factories

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.educosystem.eduapp.data.dao.BlockDao
import com.educosystem.eduapp.data.dao.SubBlockDao
import com.educosystem.eduapp.data.dao.SubjectDao
import com.educosystem.eduapp.viewmodels.DetailSubBlockViewModel

/**
 * Provide the database and context to the viewModel
 */
class DetailSubBlockViewModelFactory(
    private val subjectDao: SubjectDao,
    private val blockDao: BlockDao,
    private val subBlockDao: SubBlockDao,
    private val active: Boolean,
    private val codSubject: String,
    private val codBlock: String,
    private val application: Application
) : ViewModelProvider.Factory {

    /**
     * Creates a new instance of the given {@code Class}.
     * <p>
     *
     * @param modelClass a {@code Class} whose instance is requested
     * @param <T>        The type parameter for the ViewModel.
     * @return a newly created ViewModel
     */
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailSubBlockViewModel::class.java)) {
            return DetailSubBlockViewModel(
                subjectDao,
                blockDao,
                subBlockDao,
                active,
                codSubject,
                codBlock,
                application
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}