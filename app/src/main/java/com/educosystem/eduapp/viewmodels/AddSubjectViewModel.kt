/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.db.EduAppDatabase
import com.educosystem.eduapp.data.entity.*
import com.educosystem.eduapp.util.*
import kotlinx.coroutines.*

class AddSubjectViewModel(
    private val dataSource: EduAppDatabase,
    private val codSchoolYear: String,
    private val codSubject: String?,
    application: Application
) : AndroidViewModel(application) {
    private val STANDARD_STRUCTURE: Int
        get() = 1
    private val STANDARD_UNITS_STRUCTURE: Int
        get() = 2
    private val PERSONALIZED_STRUCTURE: Int
        get() = 3
    private val EXTRAORDINARY_STANDARD_STRUCTURE: Int
        get() = 1
    private val EXTRAORDINARY_EXAMS_WORK_STRUCTURE: Int
        get() = 2
    private val resources = application.resources
    private val context = application.applicationContext
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    val evaluations =
        if (codSubject == null) dataSource.evaluationDao.getAllFromSchoolYear(codSchoolYear)
        else dataSource.evaluationDao.getAllPendingEvaluationsFromSubject(codSubject)
    val subject =
        if (codSubject != null) dataSource.subjectDao.get(codSubject)
        else MutableLiveData<Subject?>(null)
    private val _validData = MutableLiveData(false)
    val validData: LiveData<Boolean>
        get() = _validData
    private val _restoreSelectedItems = MutableLiveData(false)
    val restoreSelectedItems: LiveData<Boolean>
        get() = _restoreSelectedItems
    private val _navigateToDetailSubject = MutableLiveData<String?>()
    val navigateToDetailSubject: LiveData<String?>
        get() = _navigateToDetailSubject
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _showToastMessage = MutableLiveData<String?>()
    val showToastMessage: LiveData<String?>
        get() = _showToastMessage
    private val _showDialog = MutableLiveData(false)
    val showDialog: LiveData<Boolean>
        get() = _showDialog
    private val _navigateToBackStack = MutableLiveData(false)
    val navigateToBackStack: LiveData<Boolean>
        get() = _navigateToBackStack
    var selectedItems = ArrayList<Evaluation>()
    private val _selectedColour = MutableLiveData<Colour?>(null)
    val selectedColour: LiveData<Colour?>
        get() = _selectedColour
    val subjectAttributesVisibility = Transformations.map(subject) {
        if (it == null)
            View.VISIBLE
        else
            View.GONE
    }
    val recyclerVisibility = Transformations.map(evaluations) {
        if (it.isEmpty()) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }
    private val _groupStructureVisibility = MutableLiveData(false)
    val groupStructureVisibility = Transformations.map(_groupStructureVisibility) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    private val _groupExtraordinaryStructureVisibility = MutableLiveData(false)
    val groupExtraordinaryStructureVisibility =
        Transformations.map(_groupExtraordinaryStructureVisibility) {
            if (it)
                View.VISIBLE
            else
                View.GONE
        }
    private val _percentageGroupVisibility = MutableLiveData(true)
    val percentageGroupVisibility = Transformations.map(_percentageGroupVisibility) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    private val _percentageExtraordinaryGroupVisibility = MutableLiveData(false)
    val percentageExtraordinaryGroupVisibility =
        Transformations.map(_percentageExtraordinaryGroupVisibility) {
            if (it)
                View.VISIBLE
            else
                View.GONE
        }
    private val _showErrorPercentages = MutableLiveData(false)
    val showErrorPercentages: LiveData<Boolean>
        get() = _showErrorPercentages
    private val _showErrorExtPercentages = MutableLiveData(false)
    val showErrorExtPercentages: LiveData<Boolean>
        get() = _showErrorExtPercentages
    private val _selectAll = MutableLiveData(true)
    val selectAll: LiveData<Boolean>
        get() = _selectAll

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Evaluate if the data are different and if this are valid.
     *
     * @param title
     * @param percentageExams
     * @param percentageWorks
     * @param percentageHomeWork
     * @param percentageExtExams
     * @param percentageExtWork
     */
    fun evaluateData(
        title: String,
        percentageExams: String,
        percentageWorks: String,
        percentageHomeWork: String,
        percentageExtExams: String,
        percentageExtWork: String
    ) {
        var valid = title.trim().isNotEmpty()
        if (codSubject != null) {
            valid = selectedItems.isNotEmpty()
        }
        if (valid) {
            if (_groupStructureVisibility.value!! && _percentageGroupVisibility.value!!) {
                val exam =
                    if (percentageExams.isNotEmpty()) Integer.valueOf(percentageExams) else 70
                val work =
                    if (percentageWorks.isNotEmpty()) Integer.valueOf(percentageWorks) else 20
                val homework =
                    if (percentageHomeWork.isNotEmpty()) Integer.valueOf(percentageHomeWork) else 10
                if (exam + work + homework != 100) {
                    valid = false
                    _showErrorPercentages.value = true
                } else
                    _showErrorPercentages.value = false
            }
            if (_groupExtraordinaryStructureVisibility.value!! && _percentageExtraordinaryGroupVisibility.value!!) {
                val examExt =
                    if (percentageExtExams.isNotEmpty()) Integer.valueOf(percentageExtExams) else 80
                val workExt =
                    if (percentageExtWork.isNotEmpty()) Integer.valueOf(percentageExtWork) else 20
                if (examExt + workExt != 100) {
                    valid = false
                    _showErrorExtPercentages.value = true
                } else
                    _showErrorExtPercentages.value = false
            }
        }
        _validData.value = valid
    }

    fun doneNavigatingToDetailSubject() {
        _navigateToDetailSubject.value = null
    }

    /**
     * Start to navigate to back stack.
     *
     */
    fun startNavigateToBackStack() {
        _navigateToBackStack.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToBackStack() {
        _navigateToBackStack.value = false
    }

    /**
     * Start show the confirm dialog.
     *
     */
    fun startShowDialog() {
        _showDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun showDialogDone() {
        _showDialog.value = false
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarMessageDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showToastMessageDone() {
        _showToastMessage.value = null
    }

    /**
     * Notify to start restore the ui data content when the configuration change.
     *
     */
    fun startRestoreContent() {
        _restoreSelectedItems.value = true
        reloadColour()
    }

    /**
     * Finish restore content for prevent unwanted behaviour.
     *
     */
    fun restoreContentDone() {
        _restoreSelectedItems.value = false
    }

    /**
     * Change the selected state of the [evaluation].
     *
     * @param evaluation to select or deselect.
     */
    fun toggleItemSelected(evaluation: Evaluation) {
        if (selectedItems.contains(evaluation)) {
            selectedItems.remove(evaluation)
        } else {
            selectedItems.add(evaluation)
        }
        checkVisibility()
    }

    /**
     * Select a new colour value.
     *
     * @param colour
     */
    fun selectColour(colour: Colour) {
        _selectedColour.value = colour
    }

    /**
     * Change the selected state of all evaluations to selected.
     * This function is called in the first load.
     *
     */
    fun selectAllEvaluations() {
        selectedItems.addAll(evaluations.value!!)
        _restoreSelectedItems.value = true
        checkVisibility()
    }

    /**
     * Finish notification of selection for prevent unwanted behaviour.
     *
     */
    fun doneSelectAll() {
        _selectAll.value = false
    }

    /**
     * reload the [_selectedColour] to update the colour of the ui.
     *
     */
    private fun reloadColour() {
        _selectedColour.value = _selectedColour.value
    }

    /**
     * Check the visibility of the user interface items.
     *
     */
    private fun checkVisibility() {
        var foundExtraordinaryEvaluation = false
        var foundNormalEvaluation = false
        var i = 0

        while (i < selectedItems.size && (!foundExtraordinaryEvaluation || !foundNormalEvaluation)) {
            if (selectedItems[i].extraOrdinary) {
                foundExtraordinaryEvaluation = true
            } else {
                foundNormalEvaluation = true
            }
            i++
        }
        _groupStructureVisibility.value = foundNormalEvaluation
        _groupExtraordinaryStructureVisibility.value = foundExtraordinaryEvaluation
    }

    /**
     * Check the visibility of structure group.
     *
     */
    fun checkVisibilityStandard(personalized: Boolean) {
        _percentageGroupVisibility.value = !personalized
    }

    /**
     * Check the visibility of extraordinary structure group.
     *
     */
    fun checkVisibilityExtraordinary(workWithExams: Boolean) {
        _percentageExtraordinaryGroupVisibility.value = workWithExams
    }

    /**
     * Save the subject or link the subject with the selected evaluations
     * depending of constructor parameters.
     *
     * @param name of the subject.
     * @param continuousAssessment if the subject is a continuous assessment.
     * @param createStandardStructure of this subject in normal evaluations.
     * @param createStandardStructureWithUnits of this subject in normal evaluations.
     * @param createStandardExtraordinaryStructure of this subject in extraordinary evaluations.
     * @param percentageExams in standard structure.
     * @param percentageWorks in standard structure.
     * @param percentageHomeWork in standard structure.
     * @param percentageExtExams in extraordinary structure.
     * @param percentageExtWork in extraordinary structure.
     */
    fun saveSubject(
        name: String,
        continuousAssessment: Boolean,
        createStandardStructure: Boolean,
        createStandardStructureWithUnits: Boolean,
        createStandardExtraordinaryStructure: Boolean,
        percentageExams: String,
        percentageWorks: String,
        percentageHomeWork: String,
        percentageExtExams: String,
        percentageExtWork: String
    ) {
        val exam =
            if (percentageExams.isNotEmpty()) Integer.valueOf(percentageExams) else 70
        val work =
            if (percentageWorks.isNotEmpty()) Integer.valueOf(percentageWorks) else 20
        val homework =
            if (percentageHomeWork.isNotEmpty()) Integer.valueOf(percentageHomeWork) else 10
        val examExt =
            if (percentageExtExams.isNotEmpty()) Integer.valueOf(percentageExtExams) else 80
        val workExt =
            if (percentageExtWork.isNotEmpty()) Integer.valueOf(percentageExtWork) else 20
        val oldSelectedItems = ArrayList<Evaluation>(selectedItems)
        oldSelectedItems.sortBy { it.position }
        val oldColour = obtainTypeColour(selectedColour.value!!, context)
        val typeStructure =
            when {
                createStandardStructure -> STANDARD_STRUCTURE
                createStandardStructureWithUnits -> STANDARD_UNITS_STRUCTURE
                else -> PERSONALIZED_STRUCTURE
            }
        val typeExtraordinaryStructure =
            when {
                createStandardExtraordinaryStructure -> EXTRAORDINARY_STANDARD_STRUCTURE
                else -> EXTRAORDINARY_EXAMS_WORK_STRUCTURE
            }

        uiScope.launch {
            try {
                val cod = codSubject ?: saveSubject(name.trim(), continuousAssessment, oldColour)
                if (oldSelectedItems.isNotEmpty()) {
                    try {
                        val subSubjects = saveSubSubject(oldSelectedItems, cod)
                        try {
                            createStructure(
                                oldSelectedItems,
                                subSubjects,
                                typeStructure,
                                typeExtraordinaryStructure,
                                exam,
                                work,
                                homework,
                                examExt,
                                workExt
                            )
                        } catch (exception: Exception) {
                            _showToastMessage.value =
                                resources.getString(R.string.errorCreateStructure)
                        }
                    } catch (exception: Exception) {
                        _showToastMessage.value = resources.getString(R.string.errorSaveSubSubjects)
                    }
                }
                _navigateToDetailSubject.value = cod
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }

    /**
     * Save Subject into database.
     *
     * @param name
     * @param continuousAssessment
     * @param colour
     * @return subject cod.
     */
    private suspend fun saveSubject(
        name: String,
        continuousAssessment: Boolean,
        colour: Int
    ): String =
        withContext(Dispatchers.IO) {
            val cod = dataSource.subjectDao.getNewSubjectCod(codSchoolYear)
            val subject = Subject(cod, codSchoolYear, name, continuousAssessment, colour = colour)
            dataSource.subjectDao.add(subject)
            return@withContext cod
        }

    /**
     * Save the links of this [codSubject] with all selected [evaluations]
     *
     * @param evaluations
     * @param codSubject
     * @return a list of subSubjects.
     */
    private suspend fun saveSubSubject(
        evaluations: ArrayList<Evaluation>,
        codSubject: String
    ): ArrayList<SubSubject> =
        withContext(Dispatchers.IO) {
            var numberCodSubSubject = dataSource.subSubjectDao.getNewNumberSubSubjectCod(codSubject)
            val subSubjects = ArrayList<SubSubject>()
            var firstTimeActivated = false
            for (evaluation: Evaluation in evaluations) {
                val codSubSubject =
                    dataSource.subSubjectDao.parseIntToCod(numberCodSubSubject, codSubject)
                val subSubject = SubSubject(
                    codSubSubject,
                    codSubject,
                    evaluation.cod,
                    active = (evaluation.startDate != null && evaluation.finishDate == null && !firstTimeActivated)
                )
                if (subSubject.active)
                    firstTimeActivated = true
                subSubjects.add(subSubject)
                numberCodSubSubject++
            }
            dataSource.subSubjectDao.add(*subSubjects.toTypedArray())
            return@withContext subSubjects
        }

    /**
     * Create the structure fo subSubjects.
     *
     * @param evaluations of subSubjects. Previous selected.
     * @param subSubjects previous generated.
     * @param typeStructure to create.
     * @param typeExtraordinaryStructure to create.
     * @param exam percentage.
     * @param work percentage.
     * @param homework percentage.
     * @param examExt percentage.
     * @param workExt percentage.
     */
    private suspend fun createStructure(
        evaluations: ArrayList<Evaluation>,
        subSubjects: ArrayList<SubSubject>,
        typeStructure: Int,
        typeExtraordinaryStructure: Int,
        exam: Int,
        work: Int,
        homework: Int,
        examExt: Int,
        workExt: Int
    ) {
        withContext(Dispatchers.IO) {
            val blocksWithSubBlocks = ArrayList<BlockWithSubBlocks>()
            var numberCod = dataSource.blockDao.getNewNumberBlockCod()
            val namesArray = listOf(
                resources.getString(R.string.examsName),
                resources.getString(R.string.worksName),
                resources.getString(R.string.homeworksName)
            )
            val percentageArray = listOf(exam, work, homework)
            val percentageExtArray = listOf(examExt, workExt)
            val typesArray = listOf(TYPE_EXAM, TYPE_WORK, TYPE_HOMEWORK)
            var unitCount = 1
            for (i in 0 until evaluations.size) {
                if (codSubject != null)
                    unitCount = 1
                if (evaluations[i].extraOrdinary) {
                    when (typeExtraordinaryStructure) {
                        EXTRAORDINARY_STANDARD_STRUCTURE -> {
                            val block = createBlock(
                                numberCod,
                                subSubjects[i].cod,
                                resources.getString(R.string.examsName),
                                100,
                                TYPE_EXAM
                            )
                            numberCod++
                            blocksWithSubBlocks.add(BlockWithSubBlocks(block, ArrayList()))
                        }
                        EXTRAORDINARY_EXAMS_WORK_STRUCTURE -> {
                            for (j in 0 until 2) {
                                val block = createBlock(
                                    numberCod,
                                    subSubjects[i].cod,
                                    namesArray[j],
                                    percentageExtArray[j],
                                    typesArray[j]
                                )
                                numberCod++
                                blocksWithSubBlocks.add(BlockWithSubBlocks(block, ArrayList()))
                            }
                        }
                    }
                } else {
                    when (typeStructure) {
                        STANDARD_STRUCTURE -> {
                            for (j in 0 until 3) {
                                val block = createBlock(
                                    numberCod,
                                    subSubjects[i].cod,
                                    namesArray[j],
                                    percentageArray[j],
                                    typesArray[j]
                                )
                                numberCod++
                                blocksWithSubBlocks.add(BlockWithSubBlocks(block, ArrayList()))
                            }
                        }
                        STANDARD_UNITS_STRUCTURE -> {
                            for (j in 1..3) {
                                val name =
                                    if (codSubject == null) resources.getString(
                                        R.string.nameOfUnit,
                                        unitCount
                                    )
                                    else "${evaluations[i].name} ${resources.getString(
                                        R.string.nameOfUnit,
                                        unitCount
                                    )}"
                                val block = createBlock(
                                    numberCod,
                                    subSubjects[i].cod,
                                    name
                                )
                                val subBlocks = ArrayList<SubBlock>()
                                var numberCodSubBlock =
                                    dataSource.subBlockDao.getNewNumberSubBlockCod(block.cod)
                                for (l in 0 until 3) {
                                    val subBlock = createSubBlock(
                                        numberCodSubBlock,
                                        block.cod,
                                        namesArray[l],
                                        percentageArray[l],
                                        typesArray[l]
                                    )
                                    numberCodSubBlock++
                                    subBlocks.add(subBlock)
                                }
                                unitCount++
                                numberCod++
                                blocksWithSubBlocks.add(BlockWithSubBlocks(block, subBlocks))
                            }
                        }
                    }
                }
            }
            dataSource.blockDao.addWithSubBlocks(*blocksWithSubBlocks.toTypedArray())
        }
    }

    /**
     * Create a block for this [codSubSubject] with the [numberCod].
     *
     * @param numberCod of the block.
     * @param codSubSubject
     * @param name of the block.
     * @param percentage of the block.
     * @param type of the block.
     * @return the generated block.
     */
    private fun createBlock(
        numberCod: Int,
        codSubSubject: String,
        name: String,
        percentage: Int? = null,
        type: Int? = null
    ): Block {
        val cod = dataSource.blockDao.parseIntToCod(numberCod)
        return Block(cod, codSubSubject, name, percent = percentage, type = type)
    }

    /**
     * create a subBlock for this [codBlock] with the [numberCod].
     *
     * @param numberCod of the subBlock.
     * @param codBlock
     * @param name of the subBlock.
     * @param percentage of the subBlock.
     * @param type of the subBlock.
     * @return the generated subBlock.
     */
    private fun createSubBlock(
        numberCod: Int,
        codBlock: String,
        name: String,
        percentage: Int,
        type: Int
    ): SubBlock {
        val cod = dataSource.subBlockDao.parseIntToCod(numberCod, codBlock)
        return SubBlock(cod, codBlock, name, percent = percentage, type = type)
    }
}

