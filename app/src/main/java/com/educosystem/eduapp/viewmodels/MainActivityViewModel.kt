/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.educosystem.eduapp.data.dao.SchoolYearDao
import kotlinx.coroutines.SupervisorJob

/**
 * ViewModel for ListActiveSchoolYearFragment
 */
class MainActivityViewModel(
    schoolYearDao: SchoolYearDao,
    application: Application
) : AndroidViewModel(application) {
    private var viewModelJob = SupervisorJob()
    val schoolYearAndSubjects = schoolYearDao.getActiveWithSubjects()
    private val _navigateToDetailSubject = MutableLiveData<Array<String?>>(null)
    val navigateToDetailSubject: LiveData<Array<String?>>
        get() = _navigateToDetailSubject
    private var _lastMenuId = 0
    private val lastMenuId: Int
        get() = _lastMenuId
    private var _firstCheck = true
    val firstCheck: Boolean
        get() = _firstCheck

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.DetailSubjectFragment].
     *
     * @param codParsedToInt
     */
    fun startNavigateToDetailSubject(codParsedToInt: Int) {
        if (lastMenuId != codParsedToInt) {
            val cod = fixCode(codParsedToInt)
            _lastMenuId = codParsedToInt
            _navigateToDetailSubject.value = arrayOf(cod, searchCodSchoolYear(cod))
        }
    }

    /**
     * Parse the cod of the id menu to a valid cod of subject.
     *
     * @param number
     * @return parsed cod.
     */
    private fun fixCode(number: Int): String {
        val code = number.toString()
        return when (code.length) {
            3 -> "00000$code"
            4 -> "0000$code"
            5 -> "000$code"
            6 -> "00$code"
            7 -> "0$code"
            else -> code
        }
    }

    /**
     * Search the cod of the school year of [codSubject].
     *
     * @param codSubject
     * @return cod of school year.
     */
    private fun searchCodSchoolYear(codSubject: String): String {
        val schoolYearsWithSubjects = schoolYearAndSubjects.value!!
        lateinit var codSchoolYear: String
        var found = false
        var i = 0
        while (i < schoolYearsWithSubjects.size && !found) {
            var j = 0
            while (j < schoolYearsWithSubjects[i].subjects.size && !found) {
                if (schoolYearsWithSubjects[i].subjects[j].cod == codSubject) {
                    found = true
                    codSchoolYear = schoolYearsWithSubjects[i].schoolYear.cod
                }
                j++
            }
            i++
        }
        return codSchoolYear
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigateToDetailSubject() {
        _navigateToDetailSubject.value = null
    }

    /**
     * Finish notification of first check of nav drawer for prevent unwanted behaviour.
     *
     */
    fun doneFirstCheck() {
        _firstCheck = false
    }

    /**
     * When the user navigate to other part that is not [com.educosystem.eduapp.ui.DetailSubjectFragment].
     *
     */
    fun invalidateLastMenuItem() {
        _lastMenuId = 0
    }

    /**
     * Set the last menu item checked if this menu is for go to [com.educosystem.eduapp.ui.DetailSubjectFragment].
     *
     * @param id
     */
    fun setLastMenuItem(id: Int) {
        _lastMenuId = id
    }
}