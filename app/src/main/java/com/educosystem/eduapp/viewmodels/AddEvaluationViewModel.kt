/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.db.EduAppDatabase
import com.educosystem.eduapp.data.entity.BlockWithSubBlocks
import com.educosystem.eduapp.data.entity.Evaluation
import com.educosystem.eduapp.data.entity.SubSubject
import com.educosystem.eduapp.data.entity.Subject
import kotlinx.coroutines.*

class AddEvaluationViewModel(
    private val dataSource: EduAppDatabase,
    private val codSchoolYear: String,
    private val codEvaluation: String?,
    application: Application
) : AndroidViewModel(application) {
    private val resources = application.resources
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    val subjects =
        if (codEvaluation == null) dataSource.subjectDao.getAllFromSchoolYear(codSchoolYear)
        else dataSource.subjectDao.getAllPendingSubjectsFromEvaluation(codEvaluation)
    val evaluation =
        if (codEvaluation != null) dataSource.evaluationDao.get(codEvaluation)
        else MutableLiveData<Evaluation?>(null)
    val numberOfEvaluations = dataSource.evaluationDao.countAllFromSchoolYear(codSchoolYear)
    private val _validData = MutableLiveData(false)
    private var _extraordinaryEvaluation = false
    val extraordinaryEvaluation: Boolean
        get() = _extraordinaryEvaluation
    private var _positionSelected = 0
    val positionSelected: Int
        get() = _positionSelected
    val validData: LiveData<Boolean>
        get() = _validData
    private val _restoreSelectedItems = MutableLiveData(false)
    val restoreSelectedItems: LiveData<Boolean>
        get() = _restoreSelectedItems
    private var _generateName = true
    val generateName: Boolean
        get() = _generateName
    private val _navigateToDetailEvaluation = MutableLiveData<String?>()
    val navigateToDetailEvaluation: LiveData<String?>
        get() = _navigateToDetailEvaluation
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _showToastMessage = MutableLiveData<String?>()
    val showToastMessage: LiveData<String?>
        get() = _showToastMessage
    private val _showDialog = MutableLiveData(false)
    val showDialog: LiveData<Boolean>
        get() = _showDialog
    private val _navigateToBackStack = MutableLiveData(false)
    val navigateToBackStack: LiveData<Boolean>
        get() = _navigateToBackStack
    var selectedItems = ArrayList<Subject>()
    val evaluationAttributesVisibility = Transformations.map(evaluation) {
        if (it == null)
            View.VISIBLE
        else
            View.GONE
    }
    val recyclerVisibility = Transformations.map(subjects) {
        if (it.isEmpty()) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }
    private val _checkBoxCopyVisibility = MutableLiveData(false)
    val checkBoxCopyVisibility = Transformations.map(_checkBoxCopyVisibility) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    private val _checkBoxExtraordinaryCopyVisibility = MutableLiveData(false)
    val checkBoxExtraordinaryCopyVisibility =
        Transformations.map(_checkBoxExtraordinaryCopyVisibility) {
            if (it)
                View.VISIBLE
            else
                View.GONE
        }
    private val _selectAll = MutableLiveData(true)
    val selectAll: LiveData<Boolean>
        get() = _selectAll

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Update the checked state of the evaluation.
     *
     * @param checked
     */
    fun updateExtraordinaryEvaluationCheck(checked: Boolean) {
        _extraordinaryEvaluation = checked
        checkVisibility()
    }

    /**
     * Finish notification of generation name for prevent unwanted behaviour.
     *
     */
    fun doneGenerateName() {
        _generateName = false
    }

    /**
     * Save the selected position for restore in a configuration change.
     *
     * @param position
     */
    fun savePositionSelected(position: Int) {
        _positionSelected = position
    }

    /**
     * Evaluate if the data are valid.
     *
     * @param title
     */
    fun evaluateData(title: String) {
        var valid = title.trim().isNotEmpty()
        if (codEvaluation != null) {
            valid = selectedItems.isNotEmpty()
        }
        _validData.value = valid
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailEvaluation() {
        _navigateToDetailEvaluation.value = null
    }

    /**
     * Start to navigate to back stack.
     *
     */
    fun startNavigateToBackStack() {
        _navigateToBackStack.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToBackStack() {
        _navigateToBackStack.value = false
    }

    /**
     * Start show the confirm dialog.
     *
     */
    fun startShowDialog() {
        _showDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun showDialogDone() {
        _showDialog.value = false
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarMessageDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Finish notification of showing dialog for prevent unwanted messages.
     *
     */
    fun showToastMessageDone() {
        _showToastMessage.value = null
    }

    /**
     * Notify to start restore the ui data content when the configuration change.
     *
     */
    fun startRestoreContent() {
        _restoreSelectedItems.value = true
    }

    /**
     * Finish restore content for prevent unwanted behaviour.
     *
     */
    fun restoreContentDone() {
        _restoreSelectedItems.value = false
    }

    /**
     * Change the selected state of the [subject].
     *
     * @param subject to select or deselect.
     */
    fun toggleItemSelected(subject: Subject) {
        if (selectedItems.contains(subject)) {
            selectedItems.remove(subject)
        } else {
            selectedItems.add(subject)
        }
        checkVisibility()
    }

    /**
     * Change the selected state of all subjects to selected.
     * This function is called in the first load.
     *
     */
    fun selectAllSubjects() {
        selectedItems.addAll(subjects.value!!)
        _restoreSelectedItems.value = true
        checkVisibility()
    }

    /**
     * Finish notification of selection for prevent unwanted behaviour.
     *
     */
    fun doneSelectAll() {
        _selectAll.value = false
    }

    /**
     * Check the visibility of the user interface items.
     *
     */
    private fun checkVisibility() {
        var extraordinaryEvaluation = this.extraordinaryEvaluation
        if (codEvaluation != null && evaluation.value != null) {
            extraordinaryEvaluation = evaluation.value!!.extraOrdinary
        }
        _checkBoxCopyVisibility.value = !extraordinaryEvaluation && selectedItems.isNotEmpty()
        _checkBoxExtraordinaryCopyVisibility.value =
            extraordinaryEvaluation && selectedItems.isNotEmpty()
    }

    /**
     * Prepare evaluation to save this evaluation or this subSubject,
     * depending of the constructor parameters, and then save the items.
     *
     * @param name of the evaluation.
     * @param positionSelected of the evaluation.
     * @param copyStructure if the user want to copy the previous available structure.
     * @param copyExtraordinaryStructure if the user want to copy the previous extraordinary available structure.
     */
    fun saveEvaluation(
        name: String,
        positionSelected: Int,
        copyStructure: Boolean,
        copyExtraordinaryStructure: Boolean
    ) {
        val oldSelectedItems = ArrayList<Subject>(selectedItems)
        val isExtraordinary = extraordinaryEvaluation
        val position =
            if (codEvaluation != null) evaluation.value!!.position
            else positionSelected
        uiScope.launch {
            try {
                val cod = codEvaluation ?: saveEvaluation(name.trim(), position, isExtraordinary)
                if (oldSelectedItems.isNotEmpty()) {
                    for (subject in oldSelectedItems) {
                        try {
                            val codSubSubject = saveSubSubject(cod, subject.cod)
                            try {
                                val blocksWithSubBlocks =
                                    searchStructure(
                                        subject.cod,
                                        position,
                                        isExtraordinary,
                                        copyStructure,
                                        copyExtraordinaryStructure
                                    )
                                if (blocksWithSubBlocks.isNotEmpty()) {
                                    saveStructure(blocksWithSubBlocks, name.trim(), codSubSubject)
                                }
                            } catch (exception: Exception) {
                                _showToastMessage.value =
                                    resources.getString(
                                        R.string.errorCreateStructureOf,
                                        subject.name
                                    )
                            }
                        } catch (exception: Exception) {
                            _showToastMessage.value =
                                resources.getString(R.string.errorSaveSubSubject, subject.name)
                        }
                    }
                }
                _navigateToDetailEvaluation.value = cod
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }

    /**
     * Save the evaluation into database.
     *
     * @param name of the evaluation.
     * @param position of the evaluation.
     * @param extraordinary if this evaluation is an extraordinary evaluation.
     * @return evaluation cod.
     */
    private suspend fun saveEvaluation(
        name: String,
        position: Int,
        extraordinary: Boolean
    ): String =
        withContext(Dispatchers.IO) {
            val cod = dataSource.evaluationDao.getNewEvaluationCod(codSchoolYear)
            val evaluation =
                Evaluation(cod, codSchoolYear, name, position, extraOrdinary = extraordinary)
            dataSource.evaluationDao.addAndReorder(evaluation)
            return@withContext cod
        }

    /**
     * Save the link of subject with this evaluation.
     *
     * @param codEvaluation to link.
     * @param codSubject to link.
     * @return cod of subSubject.
     */
    private suspend fun saveSubSubject(
        codEvaluation: String,
        codSubject: String
    ): String =
        withContext(Dispatchers.IO) {
            val subSubject = SubSubject(
                dataSource.subSubjectDao.getNewSubSubjectCod(codSubject),
                codSubject,
                codEvaluation
            )
            dataSource.subSubjectDao.add(subSubject)
            return@withContext subSubject.cod
        }

    /**
     * Search the suitable structure for this [codSubject].
     *
     * @param codSubject to search.
     * @param position position of actual evaluation.
     * @param extraordinary if this evaluation is extraordinary.
     * @param copyStructure if the user want to copy the previous available structure.
     * @param copyExtraordinaryStructure if the user want to copy the previous extraordinary available structure.
     * @return subSubject cod.
     */
    private suspend fun searchStructure(
        codSubject: String,
        position: Int,
        extraordinary: Boolean,
        copyStructure: Boolean,
        copyExtraordinaryStructure: Boolean
    ): List<BlockWithSubBlocks> =
        withContext(Dispatchers.IO) {
            var blocksWithSubBlocks: List<BlockWithSubBlocks> = ArrayList()
            val codSubSubjects =
                if (extraordinary && copyExtraordinaryStructure)
                    dataSource.subSubjectDao.getCodesFromSubjectOnlyOfExtraordinaryEvaluations(
                        codSubject
                    )
                else if (!extraordinary && copyStructure)
                    dataSource.subSubjectDao.getCodesFromSubjectOnlyUtilPositionOfEvaluation(
                        codSubject,
                        position
                    )
                else
                    ArrayList()
            var valid = false
            var i = 1

            while (i <= codSubSubjects.size && !valid) {
                blocksWithSubBlocks =
                    dataSource.blockDao.getAllWithSubBlocksList(codSubSubjects[i - 1])
                if (blocksWithSubBlocks.isNotEmpty()) {
                    valid = true
                }
                i++
            }
            return@withContext blocksWithSubBlocks
        }

    /**
     * Save the subject structure.
     *
     * @param blocksWithSubBlocks the structure of the subject.
     * @param name of the evaluation
     * @param codSubSubject
     */
    private suspend fun saveStructure(
        blocksWithSubBlocks: List<BlockWithSubBlocks>,
        name: String,
        codSubSubject: String
    ) {
        withContext(Dispatchers.IO) {
            updateCodes(blocksWithSubBlocks, name, codSubSubject)
            dataSource.blockDao.addWithSubBlocks(*blocksWithSubBlocks.toTypedArray())
        }
    }

    /**
     * Update the blocks and subBlocks codes.
     *
     * @param blocksWithSubBlocks to update codes.
     * @param name of the evaluation.
     * @param codSubSubject
     */
    private fun updateCodes(
        blocksWithSubBlocks: List<BlockWithSubBlocks>,
        name: String,
        codSubSubject: String
    ) {
        var numberCod = dataSource.blockDao.getNewNumberBlockCod()
        for (blockWithSubBlock in blocksWithSubBlocks) {
            blockWithSubBlock.block.cod = dataSource.blockDao.parseIntToCod(numberCod)
            blockWithSubBlock.block.codSubSubject = codSubSubject
            blockWithSubBlock.block.id = 0L
            if (blockWithSubBlock.subBlocks.isNotEmpty()) {
                blockWithSubBlock.block.name = "$name ${blockWithSubBlock.block.name}"
                var numberSubBlockCod =
                    dataSource.subBlockDao.getNewNumberSubBlockCod(blockWithSubBlock.block.cod)
                for (subBlock in blockWithSubBlock.subBlocks) {
                    subBlock.cod = dataSource.subBlockDao.parseIntToCod(
                        numberSubBlockCod,
                        blockWithSubBlock.block.cod
                    )
                    subBlock.codBlock = blockWithSubBlock.block.cod
                    subBlock.id = 0L
                    numberSubBlockCod++
                }
            }
            numberCod++
        }
    }
}

