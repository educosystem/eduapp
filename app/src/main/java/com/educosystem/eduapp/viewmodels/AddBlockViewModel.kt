/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.dao.BlockDao
import com.educosystem.eduapp.data.dao.SubBlockDao
import com.educosystem.eduapp.data.dao.SubjectDao
import com.educosystem.eduapp.data.entity.Block
import com.educosystem.eduapp.data.entity.SubBlock
import com.educosystem.eduapp.util.TYPE_EXAM
import com.educosystem.eduapp.util.TYPE_HOMEWORK
import com.educosystem.eduapp.util.TYPE_OTHER
import com.educosystem.eduapp.util.TYPE_WORK
import kotlinx.coroutines.*

class AddBlockViewModel(
    subjectDao: SubjectDao,
    private val blockDao: BlockDao,
    private val subBlockDao: SubBlockDao,
    codSubject: String,
    private val codSubSubject: String?,
    private val codBlock: String?,
    application: Application
) : AndroidViewModel(application) {
    private val resources = application.resources
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private var cacheSum: Int? = null
    val subject = subjectDao.get(codSubject)
    val blocks =
        if (codSubSubject != null) blockDao.getAllFromSubSubject(codSubSubject)
        else MutableLiveData<List<Block>>(null)
    val subBlocks =
        if (codBlock != null) subBlockDao.getAllFromBlock(codBlock)
        else MutableLiveData<List<SubBlock>>(null)
    val block =
        if (codBlock != null) blockDao.get(codBlock)
        else MutableLiveData<Block?>(null)
    private val _validData = MutableLiveData(false)
    val validData: LiveData<Boolean>
        get() = _validData
    private val _navigateToDetailBlock = MutableLiveData<String?>()
    val navigateToDetailBlock: LiveData<String?>
        get() = _navigateToDetailBlock
    private val _navigateToDetailSubBlock = MutableLiveData<String?>()
    val navigateToDetailSubBlock: LiveData<String?>
        get() = _navigateToDetailSubBlock
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _showDialog = MutableLiveData(false)
    val showDialog: LiveData<Boolean>
        get() = _showDialog
    private val _notifyUIUpdate = MutableLiveData(false)
    val notifyUIUpdate: LiveData<Boolean>
        get() = _notifyUIUpdate
    private var _firstCheck = true
    val firstCheck: Boolean
        get() = _firstCheck
    private val _navigateToBackStack = MutableLiveData(false)
    val navigateToBackStack: LiveData<Boolean>
        get() = _navigateToBackStack
    private val _showErrorPercentages = MutableLiveData(false)
    val showErrorPercentages: LiveData<Boolean>
        get() = _showErrorPercentages
    private val _isExamAvailable = MutableLiveData(true)
    val isExamAvailable: LiveData<Boolean>
        get() = _isExamAvailable
    private val _isWorkAvailable = MutableLiveData(true)
    val isWorkAvailable: LiveData<Boolean>
        get() = _isWorkAvailable
    private val _isHomeworkAvailable = MutableLiveData(true)
    val isHomeworkAvailable: LiveData<Boolean>
        get() = _isHomeworkAvailable
    val isUnitAvailable = Transformations.map(block) {
        it == null || it.type != null
    }
    val examVisibility = Transformations.map(isExamAvailable) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    val workVisibility = Transformations.map(isWorkAvailable) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    val homeworkVisibility = Transformations.map(isHomeworkAvailable) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    val unitVisibility = Transformations.map(isUnitAvailable) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    val messageTextView = MutableLiveData(resources.getString(R.string.percentageSuggestion))

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Evaluate if the data are valid.
     *
     * @param title
     * @param percentage
     */
    fun evaluateData(
        title: String,
        percentage: String
    ) {
        var valid = title.trim().isNotEmpty()
        if (percentage.isNotEmpty()) {
            val numberValid = Integer.valueOf(percentage) in 0..100
            if (valid)
                valid = numberValid
            uiScope.launch {
                if (cacheSum == null)
                    if (blocks.value != null) {
                        cacheSum = 0
                        for (block in blocks.value!!) {
                            if (block.percent != null)
                                cacheSum = cacheSum!! + block.percent!!
                        }
                    } else if (subBlocks.value != null) {
                        cacheSum = 0
                        for (subBlock in subBlocks.value!!) {
                            if (subBlock.percent != null)
                                cacheSum = cacheSum!! + subBlock.percent!!
                        }
                    }
                if (cacheSum != null)
                    messageTextView.value = resources.getString(
                        R.string.percentageAdd,
                        cacheSum!! + Integer.valueOf(percentage)
                    )
            }
            _showErrorPercentages.value = !numberValid
        } else {
            messageTextView.value = resources.getString(R.string.percentageSuggestion)
        }
        _validData.value = valid
    }

    /**
     * Check the visibility of the user interface items.
     *
     */
    fun checkVisibility() {
        var examFound = false
        var workFound = false
        var homeworkFound = false
        var notify = false
        fun checkType(type: Int?) {
            when (type) {
                TYPE_EXAM -> examFound = true
                TYPE_WORK -> workFound = true
                TYPE_HOMEWORK -> homeworkFound = true
            }
        }

        if (codSubSubject != null && blocks.value != null) {
            val listBlocks = blocks.value!!
            var i = 0
            while (i < listBlocks.size && (!examFound || !workFound || !homeworkFound)) {
                checkType(listBlocks[i].type)
                i++
            }
            notify = true
        } else if (codBlock != null && block.value != null && subBlocks.value != null) {
            if (block.value!!.type != null) {
                examFound = true
                workFound = true
                homeworkFound = true
            } else {
                val listSubBlocks = subBlocks.value!!
                var i = 0
                while (i < listSubBlocks.size && (!examFound || !workFound || !homeworkFound)) {
                    checkType(listSubBlocks[i].type)
                    i++
                }
            }
            notify = true
        }
        _isExamAvailable.value = !examFound
        _isWorkAvailable.value = !workFound
        _isHomeworkAvailable.value = !homeworkFound
        _notifyUIUpdate.value = notify
    }

    /**
     * Finish notification of ui update for prevent unwanted navigation.
     *
     */
    fun doneNotifyUIUpdate() {
        _notifyUIUpdate.value = false
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailBlock() {
        _navigateToDetailBlock.value = null
    }

    /**
     * Finish first check for prevent unwanted behaviour.
     *
     */
    fun doneFirstCheck() {
        _firstCheck = false
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailSubBlock() {
        _navigateToDetailSubBlock.value = null
    }

    /**
     * Start to navigate to back stack.
     *
     */
    fun startNavigateToBackStack() {
        _navigateToBackStack.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToBackStack() {
        _navigateToBackStack.value = false
    }

    /**
     * Start show the confirm dialog.
     *
     */
    fun startShowDialog() {
        _showDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun showDialogDone() {
        _showDialog.value = false
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarMessageDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Save the new block, or subBlock depending of constructor parameters, with this information.
     *
     * @param name of the block.
     * @param examType if this block is [TYPE_EXAM].
     * @param workType if this block is [TYPE_WORK].
     * @param homeworkType if this block is [TYPE_HOMEWORK].
     * @param unitType if this block don't have any type specified.
     * @param percentageString the percentage string.
     */
    fun save(
        name: String,
        examType: Boolean,
        workType: Boolean,
        homeworkType: Boolean,
        unitType: Boolean,
        percentageString: String
    ) {
        val percentage =
            if (percentageString.trim().isNotEmpty())
                Integer.valueOf(percentageString)
            else null
        val type = when {
            examType -> TYPE_EXAM
            workType -> TYPE_WORK
            homeworkType -> TYPE_HOMEWORK
            unitType -> null
            else -> TYPE_OTHER
        }
        if (codSubSubject != null) {
            saveBlock(name.trim(), percentage, type)
        } else {
            saveSubBlock(name.trim(), percentage, type)
        }
    }

    /**
     * Save new block.
     *
     * @param name of the block.
     * @param percentage
     * @param type of the block.
     */
    private fun saveBlock(name: String, percentage: Int?, type: Int?) {
        uiScope.launch {
            try {
                _navigateToDetailBlock.value = withContext(Dispatchers.IO) {
                    val cod = blockDao.getNewBlockCod()
                    val block = Block(cod, codSubSubject!!, name, percent = percentage, type = type)
                    blockDao.add(block)
                    return@withContext cod
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }

    /**
     * Save new SubBlock.
     *
     * @param name of the subBlock.
     * @param percentage
     * @param type of the subBlock.
     */
    private fun saveSubBlock(name: String, percentage: Int?, type: Int?) {
        uiScope.launch {
            try {
                _navigateToDetailSubBlock.value = withContext(Dispatchers.IO) {
                    val cod = subBlockDao.getNewSubBlockCod(codBlock!!)
                    val subBlock = SubBlock(cod, codBlock, name, percent = percentage, type = type)
                    subBlockDao.add(subBlock)
                    return@withContext cod
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }
}

