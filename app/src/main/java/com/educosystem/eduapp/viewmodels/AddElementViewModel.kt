/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels

import android.app.Application
import android.os.Build
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.dao.*
import com.educosystem.eduapp.data.entity.*
import com.educosystem.eduapp.util.*
import kotlinx.coroutines.*
import java.util.*
import kotlin.collections.ArrayList

class AddElementViewModel(
    subjectDao: SubjectDao,
    private val subSubjectDao: SubSubjectDao,
    private val blockDao: BlockDao,
    private val subBlockDao: SubBlockDao,
    private val elementDao: ElementDao,
    private val codSubject: String?,
    private var codSubSubject: String?,
    private val codBlock: String?,
    private val codSubBlock: String?,
    private val codElement: String?,
    application: Application
) : AndroidViewModel(application) {
    private val resources = application.resources
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private var _calendarAdapters = MutableLiveData<List<CalendarAdapter>>()
    val calendarAdapters
        get() = _calendarAdapters
    private var _selectedDate: CalendarAdapter? = null
    val selectedDate
        get() = _selectedDate
    private var _timeAdapters = MutableLiveData<List<TimeAdapter>>()
    val timeAdapters
        get() = _timeAdapters
    private var _selectedTime: TimeAdapter? = null
    val selectedTime
        get() = _selectedTime
    private var _firstLoad = true
    val firstLoad
        get() = _firstLoad
    private var _firstCheck = true
    private val firstCheck
        get() = _firstCheck
    private var blocksWithSubBlocks: List<BlockWithSubBlocks> = ArrayList()
    private var _lockSelectedItems = false
    private val lockSelectedItems: Boolean
        get() = _lockSelectedItems
    private var _selectedType = -1
    val selectedType: Int
        get() = _selectedType
    private var _selectedBlock: Block? = null
    val selectedBlock
        get() = _selectedBlock
    private var _selectedSubBlock: SubBlock? = null
    val selectedSubBlock
        get() = _selectedSubBlock
    val subjectByCod =
        when {
            codSubject != null -> subjectDao.get(codSubject)
            codElement != null -> subjectDao.getFromElement(codElement)
            else -> MutableLiveData<Subject>(null)
        }
    private val _selectedSubject = MutableLiveData<Subject>(null)
    val selectedSubject: LiveData<Subject>
        get() = _selectedSubject
    val subjects =
        if (codSubject == null && codElement == null) subjectDao.getAllValidForAddElements()
        else MutableLiveData(ArrayList())
    val element =
        if (codElement != null) elementDao.get(codElement)
        else MutableLiveData<Element>(null)
    private val _validData = MutableLiveData(false)
    val validData: LiveData<Boolean>
        get() = _validData
    val unitsBlocks = MutableLiveData<List<Block>>(null)
    val unitsSubBlocks = MutableLiveData<List<SubBlock>>(null)
    private val _navigateToDetailElement = MutableLiveData<String?>()
    val navigateToDetailElement: LiveData<String?>
        get() = _navigateToDetailElement
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _showExitDialog = MutableLiveData(false)
    val showExitDialog: LiveData<Boolean>
        get() = _showExitDialog
    private val _showTimeDialog = MutableLiveData(false)
    val showTimeDialog: LiveData<Boolean>
        get() = _showTimeDialog
    private val _showDateDialog = MutableLiveData(false)
    val showDateDialog: LiveData<Boolean>
        get() = _showDateDialog
    private val _notifyUIUpdate = MutableLiveData(false)
    val notifyUIUpdate: LiveData<Boolean>
        get() = _notifyUIUpdate
    private val _navigateToBackStack = MutableLiveData(false)
    val navigateToBackStack: LiveData<Boolean>
        get() = _navigateToBackStack
    private val _restoreSelectedItem = MutableLiveData(false)
    val restoreSelectedItem: LiveData<Boolean>
        get() = _restoreSelectedItem
    private val _isGroupButtonAvailable = MutableLiveData(false)
    private val isGroupButtonAvailable: LiveData<Boolean>
        get() = _isGroupButtonAvailable
    private val _isExamAvailable = MutableLiveData(false)
    private val isExamAvailable: LiveData<Boolean>
        get() = _isExamAvailable
    private val _isWorkAvailable = MutableLiveData(false)
    private val isWorkAvailable: LiveData<Boolean>
        get() = _isWorkAvailable
    private val _isHomeworkAvailable = MutableLiveData(false)
    private val isHomeworkAvailable: LiveData<Boolean>
        get() = _isHomeworkAvailable
    private val _isUnitBlockAvailable = MutableLiveData(false)
    private val isUnitBlockAvailable: LiveData<Boolean>
        get() = _isUnitBlockAvailable
    private val _isUnitSubBlockAvailable = MutableLiveData(false)
    private val isUnitSubBlockAvailable: LiveData<Boolean>
        get() = _isUnitSubBlockAvailable
    private val _isHeaderValid = MutableLiveData(false)
    val isHeaderValid: LiveData<Boolean>
        get() = _isHeaderValid

    val subjectSpinnerVisibility =
        if (codSubject != null || codElement != null)
            View.GONE
        else
            View.VISIBLE
    val blockSpinnerVisibility = Transformations.map(isUnitBlockAvailable) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    val subBlockSpinnerVisibility = Transformations.map(isUnitSubBlockAvailable) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    val examVisibility = Transformations.map(isExamAvailable) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    val workVisibility = Transformations.map(isWorkAvailable) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    val homeworkVisibility = Transformations.map(isHomeworkAvailable) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    val groupVisibility = Transformations.map(isGroupButtonAvailable) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }
    val detailsVisibility = Transformations.map(isHeaderValid) {
        if (it)
            View.VISIBLE
        else
            View.GONE
    }

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Checks the constructor parameters for determine the criteria of validations and behaviour.
     *
     */
    fun checkCods() {
        if (firstCheck)
            uiScope.launch {
                when {
                    codElement != null -> _isHeaderValid.value = true
                    codSubSubject != null -> checkVisibilityBlocks()
                    codBlock != null -> checkVisibilitySubBlocks(
                        withContext(Dispatchers.IO) { blockDao.getWithSubBlocksOnly(codBlock) }
                    )
                    codSubBlock != null -> {
                        _isHeaderValid.value = true
                        _isGroupButtonAvailable.value = false
                    }
                }
                _firstCheck = false
            }

    }

    /**
     * Select and make the necessary changes to make and intelligent and correct behaviour.
     *
     * @param subject to select.
     */
    fun selectSubject(subject: Subject) {
        if (!lockSelectedItems && subject.cod.isNotEmpty() && codSubject == null && _selectedSubject.value != subject) {
            _selectedSubject.value = subject
            _selectedBlock = null
            _selectedSubBlock = null
            uiScope.launch {
                codSubSubject = withContext(Dispatchers.IO) {
                    subSubjectDao.getActiveFromSubject(subject.cod).cod
                }
                checkVisibilityBlocks()
            }
        }
    }

    /**
     * Check if the blocks have to be visible or only their types.
     * They also load the [blocksWithSubBlocks] list of the subject.
     *
     * @param assignment for update the list of blocks.
     *        If this is true this will throw an event of blocks list changed that call this method.
     *        And may produce an infinite loop of events.
     *        So make sure what do you need before call this event with the default value of this parameter.
     */
    private suspend fun checkVisibilityBlocks(assignment: Boolean = true) {
        var examFound = false
        var workFound = false
        var homeworkFound = false
        var unitBlockFound = false
        val unitsBlocks = ArrayList<Block>()
        unitsBlocks.add(Block("", "", ""))

        /**
         * Check the type of [block] and make this type as found.
         *
         * @param block
         */
        fun checkType(block: Block) {
            when (block.type) {
                TYPE_EXAM -> examFound = true
                TYPE_WORK -> workFound = true
                TYPE_HOMEWORK -> homeworkFound = true
                else -> {
                    unitsBlocks.add(block)
                    unitBlockFound = true
                }
            }
        }
        blocksWithSubBlocks = withContext(Dispatchers.IO) {
            blockDao.getAllWithSubBlocksList(codSubSubject!!)
        }
        for (block in blocksWithSubBlocks) {
            checkType(block.block)
        }
        if (!(_selectedType == TYPE_EXAM && examFound
                    || _selectedType == TYPE_WORK && workFound
                    || _selectedType == TYPE_HOMEWORK && homeworkFound)
        )
            autoSelect(examFound, workFound, homeworkFound)
        else
            selectType(_selectedType)
        checkGroupVisibility(examFound, workFound, homeworkFound)
        _isExamAvailable.value = examFound
        _isWorkAvailable.value = workFound
        _isHomeworkAvailable.value = homeworkFound
        _isUnitBlockAvailable.value = unitBlockFound
        if (unitBlockFound && assignment)
            this.unitsBlocks.value = unitsBlocks
    }

    /**
     * Check if the type radio group should be visible or not.
     *
     * @param examFound if this type was found.
     * @param workFound if this type was found.
     * @param homeworkFound if this type was found.
     */
    private fun checkGroupVisibility(
        examFound: Boolean,
        workFound: Boolean,
        homeworkFound: Boolean
    ) {
        _isGroupButtonAvailable.value = examFound || workFound || homeworkFound
    }

    /**
     * Perform the intelligent auto select of the type of the element.
     *
     * @param examFound if this type was found.
     * @param workFound if this type was found.
     * @param homeworkFound if this type was found.
     */
    private fun autoSelect(
        examFound: Boolean,
        workFound: Boolean,
        homeworkFound: Boolean
    ) {
        when {
            homeworkFound -> selectType(TYPE_HOMEWORK)
            workFound -> selectType(TYPE_WORK)
            examFound -> selectType(TYPE_EXAM)
            else -> {
                _isHeaderValid.value = false
                _validData.value = false
            }
        }
        _notifyUIUpdate.value = true
    }

    /**
     * Select a unit block and make the necessary changes to make and intelligent and correct behaviour.
     *
     * @param block
     */
    fun selectBlock(block: Block) {
        if (!lockSelectedItems) {
            _selectedBlock = block
            _selectedSubBlock = null
            if (block.cod.isNotEmpty()) {
                val blockWithSubBlocks = foundBlockWithSubBlock(null, selectedBlock)
                checkVisibilitySubBlocks(blockWithSubBlocks!!)
                _lockSelectedItems = false
            } else {
                _selectedBlock = null
                _isUnitSubBlockAvailable.value = false
                unitsSubBlocks.value = ArrayList()
                uiScope.launch {
                    checkVisibilityBlocks(false)
                }
            }
        }
    }

    /**
     * Search the block in the [blocksWithSubBlocks].
     *
     * @param selectedType
     * @param selectedBlock
     * @return a blockWithSubBlocks object found, null otherwise.
     */
    private fun foundBlockWithSubBlock(
        selectedType: Int?,
        selectedBlock: Block?
    ): BlockWithSubBlocks? {
        var blockWithSubBlocks: BlockWithSubBlocks? = null
        var i = 0
        var found = false
        while (!found && i < blocksWithSubBlocks.size) {
            if (selectedType != null && selectedType == blocksWithSubBlocks[i].block.type
                || selectedBlock != null && selectedBlock == blocksWithSubBlocks[i].block
            ) {
                blockWithSubBlocks = blocksWithSubBlocks[i]
                found = true
            }
            i++
        }
        return blockWithSubBlocks
    }

    /**
     * Select type and make the necessary changes to make and intelligent and correct behaviour.
     *
     * @param type to select.
     */
    fun selectType(type: Int) {
        if (!lockSelectedItems) {
            _selectedType = type
            if (selectedBlock == null) {
                val blockWithSubBlocks: BlockWithSubBlocks? = foundBlockWithSubBlock(type, null)
                if (blockWithSubBlocks != null)
                    checkVisibilitySubBlocks(blockWithSubBlocks)
            } else {
                _isHeaderValid.value = true
            }
        }
    }

    /**
     * Check if the subBlocks spinner and radio button group have to be visible or not.
     *
     * @param blockWithSubBlocks
     */
    private fun checkVisibilitySubBlocks(blockWithSubBlocks: BlockWithSubBlocks) {
        var examFound = false
        var workFound = false
        var homeworkFound = false
        var unitSubBlockFound = false
        val unitsSubBlocks = ArrayList<SubBlock>()
        unitsSubBlocks.add(SubBlock("", "", ""))
        fun checkType(subBlock: SubBlock) {
            when (subBlock.type) {
                TYPE_EXAM -> examFound = true
                TYPE_WORK -> workFound = true
                TYPE_HOMEWORK -> homeworkFound = true
                else -> {
                    unitsSubBlocks.add(subBlock)
                    unitSubBlockFound = true
                }
            }
        }
        for (subBlock in blockWithSubBlocks.subBlocks) {
            checkType(subBlock)
        }
        if (blockWithSubBlocks.block.type == null) {
            if (!(_selectedType == TYPE_EXAM && examFound
                        || _selectedType == TYPE_WORK && workFound
                        || _selectedType == TYPE_HOMEWORK && homeworkFound)
            )
                autoSelect(examFound, workFound, homeworkFound)
            else
                selectType(_selectedType)
            checkGroupVisibility(examFound, workFound, homeworkFound)
            _isExamAvailable.value = examFound
            _isWorkAvailable.value = workFound
            _isHomeworkAvailable.value = homeworkFound
            _isHeaderValid.value = true
        } else {
            _isUnitSubBlockAvailable.value = unitSubBlockFound
            if (unitSubBlockFound) {
                this.unitsSubBlocks.value = unitsSubBlocks
                _isUnitSubBlockAvailable.value = true
                _isHeaderValid.value = false
                _validData.value = false
            } else
                _isHeaderValid.value = true
        }
    }

    /**
     * Select subBlock and make the header valid because don't exist other possibility.
     *
     * @param subBlock to select.
     */
    fun selectSubBlock(subBlock: SubBlock) {
        if (!lockSelectedItems) {
            _selectedSubBlock = subBlock
            _isHeaderValid.value = subBlock.cod.isNotEmpty()
        }
    }

    /**
     * Evaluate if the data are valid.
     *
     * @param title
     * @param description
     */
    fun evaluateData(
        title: String,
        description: String
    ) {
        _validData.value =
            ((title.trim().isNotEmpty() || description.trim()
                .isNotEmpty())) && _isHeaderValid.value!!
                    && elementCondition(title, description)
    }

    /**
     * The requirements of data to make the information of the element valid.
     *
     * @param title of the element.
     * @param description of the element.
     * @return if this condition are true.
     */
    private fun elementCondition(title: String, description: String): Boolean {
        val selectedDate =
            if (_selectedDate != _calendarAdapters.value!!.first())
                _selectedDate
            else null
        val selectedTime =
            if (_selectedTime != _timeAdapters.value!!.first())
                _selectedTime
            else null
        return (element.value == null
                || title.trim() != element.value!!.name
                || description.trim() != element.value!!.description
                || element.value!!.deliveryDate != formatDeliveryDate(selectedDate, selectedTime))
    }

    /**
     * Finish notification of ui update for prevent unwanted navigation.
     *
     */
    fun doneNotifyUIUpdate() {
        _notifyUIUpdate.value = false
    }

    /**
     * Load spinner of date and time.
     *
     */
    fun populateSpinners() {
        if (_calendarAdapters.value == null) {
            _calendarAdapters.value = populateAndSelectDate()
        }
        if (_timeAdapters.value == null)
            _timeAdapters.value = populateAndSelectTime()
    }

    /**
     * Load date options and select a none if the [_selectedDate] are empty.
     *
     * @return a list of dates.
     */
    private fun populateAndSelectDate(): List<CalendarAdapter> {
        val now = Calendar.getInstance()
        val tomorrow = Calendar.getInstance()
        tomorrow.add(Calendar.DAY_OF_YEAR, 1)
        val nextWeek = Calendar.getInstance()
        nextWeek.add(Calendar.WEEK_OF_YEAR, 1)
        val list = ArrayList<CalendarAdapter>()

        list.add(CalendarAdapter(now, resources, resources.getString(R.string.date)))
        _selectedDate = if (element.value != null && element.value!!.deliveryDate != null) {
            list.add(CalendarAdapter(element.value!!.deliveryDate!!, resources))
            list[1]
        } else
            list[0]

        list.add(CalendarAdapter(now, resources, resources.getString(R.string.today)))
        list.add(CalendarAdapter(tomorrow, resources, resources.getString(R.string.tomorrow)))
        list.add(
            CalendarAdapter(
                nextWeek,
                resources,
                resources.getString(
                    R.string.nextDay, nextWeek.getDisplayName(
                        Calendar.DAY_OF_WEEK,
                        Calendar.LONG,
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                            resources.configuration.locales[0]
                        else
                            resources.configuration.locale
                    )
                )
            )
        )
        list.add(CalendarAdapter(now, resources, resources.getString(R.string.pickADate)))
        return list
    }

    /**
     * Load time options and select a none if the [_selectedTime] are empty.
     *
     * @return a list of times.
     */
    private fun populateAndSelectTime(): List<TimeAdapter> {
        val empty = Calendar.getInstance()
        val morning = Calendar.getInstance()
        morning.set(Calendar.HOUR_OF_DAY, 8)
        morning.set(Calendar.MINUTE, 0)
        val noon = Calendar.getInstance()
        noon.set(Calendar.HOUR_OF_DAY, 16)
        noon.set(Calendar.MINUTE, 20)
        val afternoon = Calendar.getInstance()
        afternoon.set(Calendar.HOUR_OF_DAY, 18)
        afternoon.set(Calendar.MINUTE, 0)
        val night = Calendar.getInstance()
        night.set(Calendar.HOUR_OF_DAY, 20)
        night.set(Calendar.MINUTE, 0)
        val list = ArrayList<TimeAdapter>()

        list.add(TimeAdapter(empty, resources.getString(R.string.time), true))
        _selectedTime = if (element.value != null && element.value!!.deliveryDate != null) {
            list.add(TimeAdapter(element.value!!.deliveryDate!!))
            list[1]
        } else
            list[0]
        list.add(TimeAdapter(morning, resources.getString(R.string.morning)))
        list.add(TimeAdapter(noon, resources.getString(R.string.noon)))
        list.add(TimeAdapter(afternoon, resources.getString(R.string.afternoon)))
        list.add(TimeAdapter(night, resources.getString(R.string.night)))
        list.add(TimeAdapter(empty, resources.getString(R.string.pickATime), true))
        return list
    }

    /**
     * Update the selected date and make the necessary ui changes.
     *
     * @param selectedDate to select.
     */
    fun updateSelectedDate(selectedDate: CalendarAdapter) {
        if (selectedDate != _selectedDate)
            if (selectedDate == calendarAdapters.value!!.last())
                startShowDateDialog()
            else {
                val oldSelected = _selectedDate
                _selectedDate = selectedDate
                if (oldSelected == calendarAdapters.value!!.last())
                    calendarAdapters.value!!.last().representation =
                        resources.getString(R.string.pickADate)
            }
    }

    /**
     * Update the personalized date selected by the user making the necessary ui changes.
     *
     * @param year
     * @param month
     * @param dayOfMonth
     */
    fun updatePersonalizedDate(year: Int, month: Int, dayOfMonth: Int) {
        val date = Calendar.getInstance()
        val swap = _calendarAdapters.value!!
        val newList = ArrayList<CalendarAdapter>()
        date.set(year, month, dayOfMonth)
        _selectedDate = CalendarAdapter(date, resources)
        for (i in 0 until swap.size - 1)
            newList.add(swap[i])
        newList.add(_selectedDate!!)
        _calendarAdapters.value = newList
    }

    /**
     * Update the selected time and make the necessary ui changes.
     *
     * @param selectedTime to select.
     */
    fun updateSelectedTime(selectedTime: TimeAdapter) {
        if (selectedTime != _selectedTime)
            if (selectedTime == timeAdapters.value!!.last())
                startShowTimeDialog()
            else {
                val oldSelected = _selectedTime
                _selectedTime = selectedTime
                if (oldSelected == timeAdapters.value!!.last()) {
                    timeAdapters.value!!.last().name =
                        resources.getString(R.string.pickATime)
                    timeAdapters.value!!.last().onlyString = true
                }
            }
    }

    /**
     * Update the personalized time selected by the user making the necessary ui changes.
     *
     * @param hourOfDay
     * @param minute
     */
    fun updatePersonalizedTime(hourOfDay: Int, minute: Int) {
        val time = Calendar.getInstance()
        val swap = _timeAdapters.value!!
        val newList = ArrayList<TimeAdapter>()
        time.set(Calendar.HOUR_OF_DAY, hourOfDay)
        time.set(Calendar.MINUTE, minute)
        _selectedTime = TimeAdapter(time)
        for (i in 0 until swap.size - 1)
            newList.add(swap[i])
        newList.add(_selectedTime!!)
        _timeAdapters.value = newList
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailElement() {
        _navigateToDetailElement.value = null
    }

    /**
     * Start to navigate to back stack.
     *
     */
    fun startNavigateToBackStack() {
        _navigateToBackStack.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToBackStack() {
        _navigateToBackStack.value = false
    }

    /**
     * Start show the confirm dialog.
     *
     */
    fun startShowExitDialog() {
        _showExitDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun showExitDialogDone() {
        _showExitDialog.value = false
    }

    /**
     * Start show the date dialog.
     *
     */
    private fun startShowDateDialog() {
        _showDateDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun showDateDialogDone() {
        _showDateDialog.value = false
    }

    /**
     * Start show the time dialog.
     *
     */
    private fun startShowTimeDialog() {
        _showTimeDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun showTimeDialogDone() {
        _showTimeDialog.value = false
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarMessageDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Finish first load for prevent unwanted loop behaviour.
     *
     */
    fun doneFirstLoad() {
        _firstLoad = false
    }

    /**
     * Notify to start restore the ui data content when the configuration change.
     *
     */
    fun startRestoreContent() {
        _lockSelectedItems = true
        _restoreSelectedItem.value = true
    }

    /**
     * Finish restore content for prevent unwanted behaviour.
     *
     */
    fun restoreContentDone() {
        _restoreSelectedItem.value = false
        _lockSelectedItems = false
    }

    /**
     * Save the new element with all the information.
     *
     * @param name of the element.
     * @param description of the element.
     */
    fun save(
        name: String,
        description: String
    ) {
        val selectedDate =
            if (_selectedDate != _calendarAdapters.value!!.first())
                _selectedDate
            else null
        val selectedTime =
            if (_selectedTime != _timeAdapters.value!!.first())
                _selectedTime
            else null
        if (element.value != null)
            update(name, selectedDate, selectedTime, description)
        else
            saveElement(name, selectedDate, selectedTime, description)
    }

    /**
     * Update element with all the information.
     *
     * @param name of the element.
     * @param selectedDate of the element.
     * @param selectedTime of the element.
     * @param description of the element.
     */
    private fun update(
        name: String,
        selectedDate: CalendarAdapter?,
        selectedTime: TimeAdapter?,
        description: String
    ) {
        val element = element.value!!
        uiScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    element.name = name
                    element.deliveredDate = formatDeliveryDate(selectedDate, selectedTime)
                    element.description = description
                    elementDao.update(element)
                }
                _navigateToDetailElement.value = element.cod
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }

    /**
     * Convert the date and time selected into a single calendar item.
     *
     * @param selectedDate
     * @param selectedTime
     * @return a calendar with date and time specified.
     */
    private fun formatDeliveryDate(
        selectedDate: CalendarAdapter?,
        selectedTime: TimeAdapter?
    ): Calendar? =
        when {
            selectedDate != null && selectedTime != null -> {
                val calendar = selectedDate.calendar
                calendar.set(Calendar.HOUR_OF_DAY, selectedTime.calendar.get(Calendar.HOUR_OF_DAY))
                calendar.set(Calendar.MINUTE, selectedTime.calendar.get(Calendar.MINUTE))
                calendar
            }
            selectedDate != null -> {
                val calendar = selectedDate.calendar
                calendar.set(Calendar.HOUR_OF_DAY, 23)
                calendar.set(Calendar.MINUTE, 59)
                calendar
            }
            selectedTime != null -> {
                val calendar = selectedTime.calendar
                if (calendar < Calendar.getInstance())
                    calendar.add(Calendar.DAY_OF_MONTH, 1)
                calendar
            }
            else -> null
        }

    /**
     * Save the element into database with blocks and subBlocks information.
     *
     * @param name of the element.
     * @param selectedDate of the element.
     * @param selectedTime of the element.
     * @param description of the element.
     */
    private fun saveElement(
        name: String,
        selectedDate: CalendarAdapter?,
        selectedTime: TimeAdapter?,
        description: String
    ) {
        var selectedType = _selectedType
        var codBlock: String? = null
        var codSubBlock: String? = null
        val selectedSubBlock = selectedSubBlock
        if (selectedBlock == null) {
            val blockWithSubBlocks: BlockWithSubBlocks? = foundBlockWithSubBlock(selectedType, null)
            if (blockWithSubBlocks != null) {
                codBlock = blockWithSubBlocks.block.cod
                if (selectedSubBlock != null)
                    codSubBlock = selectedSubBlock.cod
            }
        } else {
            val blockWithSubBlocks = foundBlockWithSubBlock(null, selectedBlock)!!
            codBlock = blockWithSubBlocks.block.cod
            codSubBlock = foundSubBlock(blockWithSubBlocks, selectedType)!!.cod
        }
        uiScope.launch {
            if (codBlock == null) {
                if (this@AddElementViewModel.codBlock != null) {
                    codBlock = this@AddElementViewModel.codBlock
                    val block = withContext(Dispatchers.IO) {
                        blockDao.getOnly(codBlock!!)
                    }!!
                    val subBlock =
                        if (selectedSubBlock != null) {
                            selectedType = block.type!!
                            selectedSubBlock
                        } else foundSubBlock(withContext(Dispatchers.IO) {
                            blockDao.getWithSubBlocksOnly(codBlock!!)
                        }, selectedType)
                    if (subBlock != null) {
                        codSubBlock = subBlock.cod
                    } else {
                        selectedType = block.type!!
                    }
                } else {
                    codSubBlock = this@AddElementViewModel.codSubBlock!!
                    val subBlock = withContext(Dispatchers.IO) {
                        subBlockDao.getOnly(codSubBlock!!)
                    }
                    codBlock = subBlock.codBlock
                    selectedType = if (subBlock.type == null) {
                        withContext(Dispatchers.IO) {
                            blockDao.getOnly(codBlock!!)
                        }!!.type!!
                    } else {
                        subBlock.type!!
                    }
                }

            }
            try {
                val codElement =
                    withContext(Dispatchers.IO) {
                        val element = Element(
                            elementDao.getNewElementCod(codBlock!!),
                            codBlock!!,
                            selectedType,
                            codSubBlock,
                            deliveryDate = formatDeliveryDate(selectedDate, selectedTime),
                            name = name,
                            description = description
                        )
                        elementDao.add(element)
                        element.cod
                    }
                _navigateToDetailElement.value = codElement
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }

    /**
     * Found the selected subBlock according to the [blocksWithSubBlocks] selected and [selectedType]
     *
     * @param blockWithSubBlocks
     * @param selectedType
     * @return the subBlock, null if not found.
     */
    private fun foundSubBlock(
        blockWithSubBlocks: BlockWithSubBlocks,
        selectedType: Int
    ): SubBlock? {
        var subBlock: SubBlock? = null
        var i = 0
        var found = false
        while (!found && i < blockWithSubBlocks.subBlocks.size) {
            if (selectedType == blockWithSubBlocks.subBlocks[i].type) {
                subBlock = blockWithSubBlocks.subBlocks[i]
                found = true
            }
            i++
        }
        return subBlock
    }
}