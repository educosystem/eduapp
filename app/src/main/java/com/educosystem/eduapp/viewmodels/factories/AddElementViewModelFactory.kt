/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.viewmodels.factories

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.educosystem.eduapp.data.dao.*
import com.educosystem.eduapp.viewmodels.AddElementViewModel

/**
 * Provide the database and context to the viewModel
 */
class AddElementViewModelFactory(
    private val subjectDao: SubjectDao,
    private val subSubjectDao: SubSubjectDao,
    private val blockDao: BlockDao,
    private val subBlockDao: SubBlockDao,
    private val elementDao: ElementDao,
    private val codSubject: String?,
    private val codSubSubject: String?,
    private val codBlock: String?,
    private val codSubBlock: String?,
    private val codElement: String?,
    private val application: Application
) : ViewModelProvider.Factory {

    /**
     * Creates a new instance of the given {@code Class}.
     * <p>
     *
     * @param modelClass a {@code Class} whose instance is requested
     * @param <T>        The type parameter for the ViewModel.
     * @return a newly created ViewModel
     */
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AddElementViewModel::class.java)) {
            return AddElementViewModel(
                subjectDao,
                subSubjectDao,
                blockDao,
                subBlockDao,
                elementDao,
                codSubject,
                codSubSubject,
                codBlock,
                codSubBlock,
                codElement,
                application
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}