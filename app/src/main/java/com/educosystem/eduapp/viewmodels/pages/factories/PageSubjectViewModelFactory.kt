/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.viewmodels.pages.factories

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.educosystem.eduapp.data.dao.SubSubjectDao
import com.educosystem.eduapp.data.dao.SubjectDao
import com.educosystem.eduapp.viewmodels.pages.PageSubjectViewModel

/**
 * Provide the database and context to the viewModel
 */
class PageSubjectViewModelFactory(
    private val subjectDao: SubjectDao,
    private val subSubjectDao: SubSubjectDao,
    private val codSchoolYear: String?,
    private val codEvaluation: String?,
    private val application: Application
) : ViewModelProvider.Factory {

    /**
     * Creates a new instance of the given {@code Class}.
     * <p>
     *
     * @param modelClass a {@code Class} whose instance is requested
     * @param <T>        The type parameter for the ViewModel.
     * @return a newly created ViewModel
     */
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PageSubjectViewModel::class.java)) {
            return PageSubjectViewModel(
                subjectDao,
                subSubjectDao,
                codSchoolYear,
                codEvaluation,
                application
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}