/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels

import android.app.Application
import androidx.core.content.ContextCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.dao.ElementDao
import com.educosystem.eduapp.data.dao.SubjectDao
import com.educosystem.eduapp.util.TYPE_EXAM
import com.educosystem.eduapp.util.TYPE_HOMEWORK
import com.educosystem.eduapp.util.TYPE_WORK
import kotlinx.coroutines.SupervisorJob
import java.util.*

class DetailElementViewModel(
    subjectDao: SubjectDao,
    elementDao: ElementDao,
    codElement: String,
    application: Application
) : AndroidViewModel(application) {
    private val resources = application.resources
    private val context = application.applicationContext
    private var viewModelJob = SupervisorJob()
    val subject = subjectDao.getFromElement(codElement)
    val element = elementDao.get(codElement)
    val delayed = Transformations.map(element) {
        if (it.deliveryDate != null
            && it.deliveryDate!! < Calendar.getInstance() && it.type != TYPE_EXAM
            || it.deliveryDate != null && it.deliveredDate != null
            && it.deliveredDate!! < it.deliveryDate!! && it.type != TYPE_EXAM
        )
            ContextCompat.getColor(
                context,
                R.color.delayedTextColour
            )
        else
            ContextCompat.getColor(
                context,
                R.color.primaryTextColor
            )
    }
    val title = Transformations.map(element) {
        when (it.type) {
            TYPE_EXAM -> resources.getString(R.string.examName)
            TYPE_WORK -> resources.getString(R.string.workName)
            TYPE_HOMEWORK -> resources.getString(R.string.homeworkName)
            else -> resources.getString(R.string.otherName)
        }
    }
    private val _navigateToEditFragment = MutableLiveData(false)
    val navigateToEditFragment: LiveData<Boolean>
        get() = _navigateToEditFragment
    private val _navigateToEditGrade = MutableLiveData(false)
    val navigateToEditGrade: LiveData<Boolean>
        get() = _navigateToEditGrade

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.AddElementFragment].
     *
     */
    fun startNavigateToEditFragment() {
        _navigateToEditFragment.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToEditFragment() {
        _navigateToEditFragment.value = false
    }

    /**
     * Start to navigate to editGradeFragment when they will be implemented..
     *
     */
    fun startNavigateToEditGrade() {
        _navigateToEditGrade.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToEditGrade() {
        _navigateToEditGrade.value = false
    }
}