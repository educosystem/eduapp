/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels.dialogs

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.dao.BlockDao
import com.educosystem.eduapp.data.dao.SubBlockDao
import com.educosystem.eduapp.data.entity.Block
import com.educosystem.eduapp.data.entity.SubBlock
import kotlinx.coroutines.*

class EditBlockViewModel(
    private val blockDao: BlockDao,
    private val subBlockDao: SubBlockDao,
    private val codSubSubject: String?,
    private val codBlock: String,
    private val codSubBlock: String?,
    application: Application
) : AndroidViewModel(application) {
    private val resources = application.resources
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private var cacheSum: Int? = null
    private var _firstSetting = true
    val firstSetting: Boolean
        get() = _firstSetting
    val block =
        if (codSubSubject != null) blockDao.get(codBlock)
        else MutableLiveData<Block?>(null)
    val subBlock =
        if (codSubBlock != null) subBlockDao.get(codSubBlock)
        else MutableLiveData<SubBlock>(null)
    private val _name = MutableLiveData<String?>(null)
    val name: LiveData<String?>
        get() = _name
    private val _percentage = MutableLiveData<Int?>(null)
    val percentage: LiveData<Int?>
        get() = _percentage
    private val _validData = MutableLiveData(false)
    val validData: LiveData<Boolean>
        get() = _validData
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _clearData = MutableLiveData(false)
    val clearData: LiveData<Boolean>
        get() = _clearData
    private val _showDialog = MutableLiveData(false)
    val showDialog: LiveData<Boolean>
        get() = _showDialog
    private val _navigateToBackStack = MutableLiveData(false)
    val navigateToBackStack: LiveData<Boolean>
        get() = _navigateToBackStack
    private val _showErrorPercentages = MutableLiveData(false)
    val showErrorPercentages: LiveData<Boolean>
        get() = _showErrorPercentages
    val messageTextView = MutableLiveData(resources.getString(R.string.percentageSuggestion))

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Evaluate if the data are different and if this are valid.
     *
     * @param title
     * @param percentage
     */
    fun evaluateData(
        title: String,
        percentage: String
    ) {
        val titleValid = title.trim().isNotEmpty() && when {
            block.value != null -> title.trim() != block.value!!.name
            else -> title.trim() != subBlock.value!!.name
        }
        val numberValid: Boolean
        if (percentage.isNotEmpty()) {
            val percentageInRange = Integer.valueOf(percentage) in 0..100
            numberValid =
                when {
                    block.value != null ->
                        if (block.value!!.percent == null
                            || Integer.valueOf(percentage) != block.value!!.percent
                        )
                            percentageInRange
                        else
                            false
                    else ->
                        if (subBlock.value!!.percent == null
                            || Integer.valueOf(percentage) != subBlock.value!!.percent
                        )
                            percentageInRange
                        else
                            false
                }
            calculatePercentage(percentage)
            _showErrorPercentages.value = !percentageInRange
        } else {
            numberValid = when {
                block.value != null -> block.value!!.percent != null
                else -> subBlock.value!!.percent != null
            }
            messageTextView.value = resources.getString(R.string.percentageSuggestion)
        }
        _validData.value = titleValid || numberValid
    }

    /**
     * Provide a helper to calculate the future total percentage.
     *
     * @param percentage value.
     */
    private fun calculatePercentage(percentage: String) {
        uiScope.launch {
            if (cacheSum == null)
                if (codSubSubject != null) {
                    val blocks = withContext(Dispatchers.IO) {
                        blockDao.getAllFromSubSubjectList(codSubSubject)
                    }
                    cacheSum = 0
                    for (block in blocks) {
                        if (block.percent != null && block.cod != codBlock)
                            cacheSum = cacheSum!! + block.percent!!
                    }
                } else {
                    val subBlocks = withContext(Dispatchers.IO) {
                        subBlockDao.getAllFromBlockList(codBlock)
                    }
                    cacheSum = 0
                    for (subBlock in subBlocks) {
                        if (subBlock.percent != null && subBlock.cod != codSubBlock)
                            cacheSum = cacheSum!! + subBlock.percent!!
                    }
                }
            if (cacheSum != null)
                messageTextView.value = resources.getString(
                    R.string.percentageAdd,
                    cacheSum!! + Integer.valueOf(percentage)
                )
        }
    }

    /**
     * Start to navigate to back stack.
     *
     */
    fun startNavigateToBackStack() {
        _clearData.value = true
        _navigateToBackStack.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToBackStack() {
        _navigateToBackStack.value = false
    }

    /**
     * Start show the confirm dialog.
     *
     */
    fun startShowDialog() {
        _showDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun showDialogDone() {
        _showDialog.value = false
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarMessageDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Finish notification of clear data for prevent unwanted behaviour.
     *
     */
    fun clearDataDone() {
        _clearData.value = false
    }

    /**
     * For first load. Load the name and percent of the block or subBlock.
     *
     */
    fun updateNameAndPercentage() {
        if (block.value != null) {
            _name.value = block.value!!.name
            _percentage.value = block.value!!.percent
        } else if (subBlock.value != null) {
            _name.value = subBlock.value!!.name
            _percentage.value = subBlock.value!!.percent
        }
    }

    /**
     * Finish first setting for prevent unwanted behaviour.
     *
     */
    fun doneFirstSetting() {
        _firstSetting = false
    }

    /**
     * Edit and save changes of block or subBlocks
     * depending of the parameters received in the constructor.
     *
     * @param newName
     * @param percentageString
     */
    fun edit(newName: String, percentageString: String) {
        uiScope.launch {
            val percentage =
                if (percentageString.trim().isNotEmpty())
                    Integer.valueOf(percentageString)
                else null
            if (codSubSubject != null)
                saveBlock(newName, percentage)
            else if (codSubBlock != null)
                saveSubBlock(newName, percentage)
        }
    }

    /**
     * Save block changes
     *
     * @param newName
     * @param percentage
     */
    private suspend fun saveBlock(newName: String, percentage: Int?) {
        val block = block.value!!
        block.name = newName.trim()
        block.percent = percentage
        try {
            withContext(Dispatchers.IO) {
                blockDao.update(block)
            }
            startNavigateToBackStack()
        } catch (exception: Exception) {
            _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
        }
    }

    /**
     * Save subBlock changes
     *
     * @param newName
     * @param percentage
     */
    private suspend fun saveSubBlock(newName: String, percentage: Int?) {
        val subBlock = subBlock.value!!
        subBlock.name = newName.trim()
        subBlock.percent = percentage
        try {
            withContext(Dispatchers.IO) {
                subBlockDao.update(subBlock)
            }
            startNavigateToBackStack()
        } catch (exception: Exception) {
            _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
        }
    }
}
