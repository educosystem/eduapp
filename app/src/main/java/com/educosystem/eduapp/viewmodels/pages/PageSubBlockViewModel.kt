/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels.pages

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.dao.SubBlockDao
import com.educosystem.eduapp.data.entity.SubBlock
import com.educosystem.eduapp.util.PageListener
import kotlinx.coroutines.*

class PageSubBlockViewModel(
    private val subBlockDao: SubBlockDao,
    codBlock: String,
    application: Application
) : AndroidViewModel(application) {
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val resources = application.resources
    val blocks = subBlockDao.getAllFromBlock(codBlock)
    private val _navigateToDetailSubBlock = MutableLiveData<String?>()
    val navigateToDetailSubBlock: LiveData<String?>
        get() = _navigateToDetailSubBlock
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _actionModeActivated = MutableLiveData<Boolean>()
    val actionModeActivated: LiveData<Boolean>
        get() = _actionModeActivated
    private val _selectAllItems = MutableLiveData<Boolean>()
    val selectAllItems: LiveData<Boolean>
        get() = _selectAllItems
    private val _showDialogDelete = MutableLiveData<Boolean>()
    val showDialogDelete: LiveData<Boolean>
        get() = _showDialogDelete
    var selectedItems = ArrayList<SubBlock>()
    val emptyTextViewVisibility = Transformations.map(blocks) {
        if (it.isEmpty()) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }
    private var _colour = 0
    val colour
        get() = _colour
    private val _changeTitleActionMode = MutableLiveData(false)
    val changeTitleActionMode: LiveData<Boolean>
        get() = _changeTitleActionMode
    val titleActionMode: Int
        get() = selectedItems.size
    private var _listener: PageListener? = null
    val listener: PageListener?
        get() = _listener

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.DetailSubBlockFragment].
     *
     * @param codSubBlock
     */
    fun startNavigateToDetailSubBlock(codSubBlock: String) {
        _navigateToDetailSubBlock.value = codSubBlock
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailSubBlock() {
        _navigateToDetailSubBlock.value = null
    }

    /**
     * Activate the action mode.
     *
     */
    fun activateActionMode() {
        _actionModeActivated.value = true
    }

    /**
     * Change the selected state of the [subBlock].
     *
     * @param subBlock to select or deselect.
     */
    fun toggleItemSelected(subBlock: SubBlock) {
        if (selectedItems.contains(subBlock)) {
            selectedItems.remove(subBlock)
            if (selectedItems.isEmpty()) {
                _actionModeActivated.value = false
            }
        } else {
            selectedItems.add(subBlock)
        }
        notifyChangeTitleActionMode()
    }

    /**
     * Change the selected state of all subBlocks to selected.
     *
     */
    fun selectAll() {
        selectedItems.clear()
        for (subBlock: SubBlock in blocks.value!!) {
            selectedItems.add(subBlock)
        }
        _selectAllItems.value = true
        notifyChangeTitleActionMode()
    }

    /**
     * Finish notification of selection for prevent unwanted behaviour.
     *
     */
    fun selectAllDone() {
        _selectAllItems.value = false
    }

    /**
     * Notify to change the title of the action mode when a new item is selected.
     *
     */
    fun notifyChangeTitleActionMode() {
        _changeTitleActionMode.value = true
    }

    /**
     * Finish notification of change the title for prevent unwanted behaviour.
     *
     */
    fun changeTitleActionModeDone() {
        _changeTitleActionMode.value = false
    }

    /**
     * Delete all selected subBlocks.
     *
     */
    fun delete() {
        val selectedItemsOld = ArrayList<SubBlock>(selectedItems)
        _actionModeActivated.value = false
        uiScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    subBlockDao.delete(*selectedItemsOld.toTypedArray())
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorDelete)
            }
        }
    }

    /**
     * Finish action mode and restore the default behaviour.
     *
     */
    fun actionModeDone() {
        selectedItems.clear()
        _actionModeActivated.value = false
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Start show the confirm delete.
     *
     */
    fun startDialogDelete() {
        _showDialogDelete.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun dialogDeleteDone() {
        _showDialogDelete.value = false
    }

    /**
     * Set the page listener if it's not set yet.
     *
     * @param newListener
     */
    fun setListener(newListener: PageListener) {
        if (_listener == null) {
            _listener = newListener
        }
    }

    /**
     * Set the status bar colour for restore colours when the action mode finish.
     *
     * @param statusBarColor
     */
    fun setColour(statusBarColor: Int) {
        _colour = statusBarColor
    }
}