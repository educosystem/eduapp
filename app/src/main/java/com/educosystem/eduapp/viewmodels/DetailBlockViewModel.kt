/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.educosystem.eduapp.data.dao.BlockDao
import com.educosystem.eduapp.data.dao.SubBlockDao
import com.educosystem.eduapp.data.dao.SubjectDao
import com.educosystem.eduapp.util.TYPE_EXAM
import com.educosystem.eduapp.util.TYPE_HOMEWORK
import com.educosystem.eduapp.util.TYPE_WORK
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class DetailBlockViewModel(
    subjectDao: SubjectDao,
    blockDao: BlockDao,
    subBlockDao: SubBlockDao,
    val active: Boolean,
    codSubject: String,
    private val codBlock: String,
    application: Application
) : AndroidViewModel(application) {
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    val subject = subjectDao.get(codSubject)
    val block = blockDao.get(codBlock)
    val subBlocks = subBlockDao.getAllFromBlock(codBlock)
    private var _canExecuteFunction = false
    private val _navigateToAddSubBlock = MutableLiveData<String?>()
    val navigateToAddSubBlock: LiveData<String?>
        get() = _navigateToAddSubBlock
    private val _navigateToDetailSubBlock = MutableLiveData<String?>()
    val navigateToDetailSubBlock: LiveData<String?>
        get() = _navigateToDetailSubBlock
    private val _navigateToDetailElement = MutableLiveData<String?>(null)
    val navigateToDetailElement: LiveData<String?>
        get() = _navigateToDetailElement
    private val _navigateToAddElement = MutableLiveData<String?>(null)
    val navigateToAddElement: LiveData<String?>
        get() = _navigateToAddElement
    private val _showEditDialog = MutableLiveData(false)
    val showEditDialog: LiveData<Boolean>
        get() = _showEditDialog
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    val toolbarTitle = Transformations.map(block) {
        it.name
    }
    private var _viewPagerLoad = true
    val viewPageLoad: Boolean
        get() = _viewPagerLoad
    private var _function: (() -> Unit)? = null
    val function
        get() = _function
    private var _oldPosition = 0
    val oldPosition: Int
        get() = _oldPosition
    private val _hideButton = MutableLiveData<Boolean>()
    val hideButton: LiveData<Boolean>
        get() = _hideButton
    private val _isAddSubBlockAvailable = MutableLiveData(true)
    private val isAddSubBlockAvailable: LiveData<Boolean>
        get() = _isAddSubBlockAvailable

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Make checks to verify if this the fragment can add items.
     */
    fun evaluateData() {
        if (block.value != null && subBlocks.value != null) {
            var available = true
            if (block.value!!.type == null) {
                var examFound = false
                var workFound = false
                var homeworkFound = false
                val listSubBlocks = subBlocks.value!!
                var i = 0
                while (i < listSubBlocks.size && (!examFound || !workFound || !homeworkFound)) {
                    when (listSubBlocks[i].type) {
                        TYPE_EXAM -> examFound = true
                        TYPE_WORK -> workFound = true
                        TYPE_HOMEWORK -> homeworkFound = true
                    }
                    i++
                }
                if (examFound && workFound && homeworkFound)
                    available = false
            }
            _isAddSubBlockAvailable.value = available
            hideFabButton(true)
            hideFabButton(false)
        }
    }

    /**
     * Calculate the action of the fab button depending of the actual page.
     *
     */
    fun onAdd() {
        when (oldPosition) {
            0 -> startNavigateToAddElement()
            3 -> if (block.value!!.type == TYPE_HOMEWORK) startNavigateToAddSubBlock()
            4 -> startNavigateToAddSubBlock()
        }
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.AddBlockFragment].
     *
     */
    private fun startNavigateToAddSubBlock() {
        _navigateToAddSubBlock.value = codBlock
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddSubBlock() {
        _navigateToAddSubBlock.value = null
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.DetailSubBlockFragment].
     *
     * @param codBlock
     */
    fun startNavigateToDetailSubBlock(codBlock: String) {
        uiScope.launch {
            _navigateToDetailSubBlock.value = codBlock
        }
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailSubBlock() {
        _navigateToDetailSubBlock.value = null
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.AddElementFragment].
     *
     */
    private fun startNavigateToAddElement() {
        _navigateToAddElement.value = codBlock
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddElement() {
        _navigateToAddElement.value = null
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.DetailElementFragment].
     *
     * @param codElement
     */
    fun startNavigateToDetailElement(codElement: String) {
        _navigateToDetailElement.value = codElement
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailElement() {
        _navigateToDetailElement.value = null
    }

    /**
     * Start show the edit dialog.
     *
     */
    fun startShowEditDialog() {
        _showEditDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun doneShowEditDialog() {
        _showEditDialog.value = false
    }

    /**
     * Start show message on snackBar and check the if the undo action should be shown.
     *
     * @param message
     */
    fun startSnackBarMessage(message: String) {
        if (_canExecuteFunction) {
            _canExecuteFunction = false
        } else {
            _function = null
        }
        _showSnackBarMessage.value = message
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Finish to load dynamic viewPager to prevent unwanted behaviour until a configuration change.
     *
     */
    fun doneLoadViewPager() {
        _viewPagerLoad = false
    }

    /**
     * Update the actual page position.
     *
     * @param position of the viewPager.
     */
    fun setLastPosition(position: Int) {
        _oldPosition = position
        hideFabButton(!active)
    }

    /**
     * Update the undo function to execute when the snackBar message is shown.
     *
     * @param function
     */
    fun updateFunction(function: (() -> Unit)?) {
        _function = function
        _canExecuteFunction = true
    }

    /**
     * Set to null [_function] to prevent repeated actions.
     *
     */
    fun doneUseFunction() {
        _function = null
    }

    /**
     * Set the hide state of the fab button making necessary checks.
     *
     * @param hide status of fab button.
     */
    fun hideFabButton(hide: Boolean) {
        if (active && (addSubBlockCondition() || addElementCondition()) || hide)
            _hideButton.value = hide
    }

    /**
     * Condition for show the fab button in the subBlock page.
     *
     */
    private fun addSubBlockCondition() =
        isAddSubBlockAvailable.value != null && isAddSubBlockAvailable.value!!
                && (oldPosition == 4 || oldPosition == 3 && block.value != null && block.value!!.type == TYPE_HOMEWORK)

    /**
     * Condition for show the fab button in the element page.
     *
     */
    private fun addElementCondition() =
        oldPosition == 0 && block.value != null
                && (
                block.value!!.type != null
                        || block.value!!.type == null && subBlocks.value != null && subBlocks.value!!.isNotEmpty()
                )
}