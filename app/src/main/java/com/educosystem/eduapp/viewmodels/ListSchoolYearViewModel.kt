/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/**
 * ViewModel for ListActiveSchoolYearFragment
 */
class ListSchoolYearViewModel(
    application: Application
) : AndroidViewModel(application) {
    private val _navigateToAddSchoolYear = MutableLiveData<Boolean>()
    val navigateToAddSchoolYear: LiveData<Boolean>
        get() = _navigateToAddSchoolYear
    private val _navigateToDetailSchoolYear = MutableLiveData<String?>()
    val navigateToDetailSchoolYear: LiveData<String?>
        get() = _navigateToDetailSchoolYear
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private var _oldPosition = 0
    val oldPosition: Int
        get() = _oldPosition
    private val _hideButton = MutableLiveData<Boolean>()
    val hideButton: LiveData<Boolean>
        get() = _hideButton

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.AddSchoolYearFragment].
     *
     */
    fun onAddSchoolYear() {
        _navigateToAddSchoolYear.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddSchoolYear() {
        _navigateToAddSchoolYear.value = false
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.DetailSchoolYearFragment].
     *
     * @param codSchoolYear
     */
    fun startNavigateToDetailSchoolYear(codSchoolYear: String) {
        _navigateToDetailSchoolYear.value = codSchoolYear
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailSchoolYear() {
        _navigateToDetailSchoolYear.value = null
    }

    /**
     * Start show message on snackBar.
     *
     * @param message
     */
    fun startSnackBarMessage(message: String) {
        _showSnackBarMessage.value = message
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Update the actual page position.
     *
     * @param position of the viewPager.
     */
    fun setLastPosition(position: Int) {
        _oldPosition = position
    }

    /**
     * Set the hide state of the fab button making necessary checks.
     *
     * @param hide status of fab button.
     */
    fun hideFabButton(hide: Boolean) {
        if (hide || _oldPosition == 0)
            _hideButton.value = hide
    }
}