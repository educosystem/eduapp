/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.db.EduAppDatabase
import com.educosystem.eduapp.data.entity.BlockWithSubBlocks
import com.educosystem.eduapp.data.entity.Evaluation
import com.educosystem.eduapp.data.entity.SubBlock
import com.educosystem.eduapp.data.entity.SubSubject
import kotlinx.coroutines.*

class
CopyToViewModel(
    private val dataSource: EduAppDatabase,
    private val codSubject: String,
    private val codSubSubject: String,
    application: Application
) : AndroidViewModel(application) {
    private val resources = application.resources
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    val evaluations =
        dataSource.evaluationDao.getAllFromSubjectWithoutSubSubject(codSubject, codSubSubject)
    val subject = dataSource.subjectDao.get(codSubject)
    val structure = dataSource.blockDao.getAllWithSubBlocks(codSubSubject)
    private val _validData = MutableLiveData(false)
    val validData: LiveData<Boolean>
        get() = _validData
    private val _restoreSelectedItems = MutableLiveData(false)
    val restoreSelectedItems: LiveData<Boolean>
        get() = _restoreSelectedItems
    private val _navigateToDetailSubSubject = MutableLiveData<String?>()
    val navigateToDetailSubSubject: LiveData<String?>
        get() = _navigateToDetailSubSubject
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _showToastMessage = MutableLiveData<String?>()
    val showToastMessage: LiveData<String?>
        get() = _showToastMessage
    private val _disableCloneMode = MutableLiveData(false)
    val disableCloneMode: LiveData<Boolean>
        get() = _disableCloneMode
    private val _showDialog = MutableLiveData(false)
    val showDialog: LiveData<Boolean>
        get() = _showDialog
    private val _navigateToBackStack = MutableLiveData(false)
    val navigateToBackStack: LiveData<Boolean>
        get() = _navigateToBackStack
    var selectedItems = ArrayList<Evaluation>()
    private var _numberBlockCod: Int = 0
    private var _numberSubBlockCod: Int = 0

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Evaluate if the data are valid.
     *
     */
    fun evaluateData() {
        _validData.value = selectedItems.isNotEmpty()
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailSubject() {
        _navigateToDetailSubSubject.value = null
    }

    /**
     * Start to navigate to back stack.
     *
     */
    fun startNavigateToBackStack() {
        _navigateToBackStack.value = true
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToBackStack() {
        _navigateToBackStack.value = false
    }

    /**
     * Start show the confirm dialog.
     *
     */
    fun startShowDialog() {
        _showDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun showDialogDone() {
        _showDialog.value = false
    }

    /**
     * Finish clone mode
     *
     */
    fun startDisableCloneMode() {
        _disableCloneMode.value = true
    }

    /**
     * Finish notification of finish clone mode for prevent unwanted behaviour.
     *
     */
    fun doneDisableCloneMode() {
        _disableCloneMode.value = false
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarMessageDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showToastMessageDone() {
        _showToastMessage.value = null
    }

    /**
     * Notify to start restore the ui data content when the configuration change.
     *
     */
    fun startRestoreContent() {
        _restoreSelectedItems.value = true
    }

    /**
     * Finish restore content for prevent unwanted behaviour.
     *
     */
    fun restoreContentDone() {
        _restoreSelectedItems.value = false
    }

    /**
     * Change the selected state of the [evaluation].
     *
     * @param evaluation to select or deselect.
     */
    fun toggleItemSelected(evaluation: Evaluation) {
        if (selectedItems.contains(evaluation)) {
            selectedItems.remove(evaluation)
        } else {
            selectedItems.add(evaluation)
        }
        evaluateData()
    }

    /**
     * Save the previewStructure to persistence.
     *
     * @param cloneMode if recreate with the new structure.
     */
    fun startCopy(cloneMode: Boolean) {
        val selectedStructure = structure.value!!
        val selectedEvaluations = ArrayList(selectedItems)
        uiScope.launch {
            val subSubjects = ArrayList<SubSubject>()
            withContext(Dispatchers.IO) {
                for (evaluation in selectedEvaluations)
                    subSubjects.add(dataSource.subSubjectDao.get(evaluation.cod, codSubject))
            }
            if (cloneMode)
                cloneEvaluations(selectedStructure, subSubjects)
            else {
                val resultStructure = ArrayList<BlockWithSubBlocks>()
                _numberBlockCod =
                    withContext(Dispatchers.IO) { dataSource.blockDao.getNewNumberBlockCod() }
                for (subSubject in subSubjects) {
                    resultStructure.addAll(processStructure(selectedStructure, subSubject.cod))
                }
                copyStructureTo(resultStructure)
            }
        }
    }

    /**
     * Delete the old structure of the subjects and create the new one.
     *
     * @param structure to create.
     * @param subSubjects
     */
    private suspend fun cloneEvaluations(
        structure: List<BlockWithSubBlocks>,
        subSubjects: ArrayList<SubSubject>
    ) {
        try {
            withContext(Dispatchers.IO) {
                dataSource.blockDao.delete(*subSubjects.toTypedArray())
            }
            for (subSubject in subSubjects) {
                val swapArray = ArrayList<BlockWithSubBlocks>(structure)
                createStructureAndSave(swapArray, subSubject.cod)
            }
            _navigateToDetailSubSubject.value = codSubSubject
        } catch (_: Exception) {
            _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
        }
    }

    /**
     * Create the new structure and save it.
     *
     * @param blocksWithSubBlocks to create.
     * @param codSubSubject for update codes.
     */
    private suspend fun createStructureAndSave(
        blocksWithSubBlocks: List<BlockWithSubBlocks>,
        codSubSubject: String
    ) {
        withContext(Dispatchers.IO) {
            updateCodes(blocksWithSubBlocks, codSubSubject)
            dataSource.blockDao.addWithSubBlocks(*blocksWithSubBlocks.toTypedArray())
        }
    }

    /**
     * Update codes of the list of blocksWithSubBlocks structure.
     *
     * @param blocksWithSubBlocks to update.
     * @param codSubSubject of blocks.
     */
    private fun updateCodes(
        blocksWithSubBlocks: List<BlockWithSubBlocks>,
        codSubSubject: String
    ) {
        var numberCod = dataSource.blockDao.getNewNumberBlockCod()
        for (blockWithSubBlock in blocksWithSubBlocks) {
            val updated = updateCode(blockWithSubBlock, numberCod, codSubSubject)
            blockWithSubBlock.block = updated.block
            blockWithSubBlock.subBlocks = updated.subBlocks
            numberCod++
        }
    }

    /**
     * Update codes of the block structure.
     *
     * @param blockWithSubBlock to update.
     * @param numberCod to parse into a valid cod.
     * @param codSubSubject of block.
     * @return an updated blockWithSubBlocks.
     */
    private fun updateCode(
        blockWithSubBlock: BlockWithSubBlocks,
        numberCod: Int,
        codSubSubject: String
    ): BlockWithSubBlocks {
        blockWithSubBlock.block.cod = dataSource.blockDao.parseIntToCod(numberCod)
        blockWithSubBlock.block.codSubSubject = codSubSubject
        blockWithSubBlock.block.id = 0L
        if (blockWithSubBlock.subBlocks.isNotEmpty()) {
            blockWithSubBlock.subBlocks =
                updateSubBlocksCod(blockWithSubBlock.subBlocks, blockWithSubBlock.block.cod)
        }
        return blockWithSubBlock
    }

    /**
     * Update codes of the list of subBlocks structure.
     *
     * @param subBlocks to update.
     * @param codBlock of the subBlocks.
     * @return list of subBlocks updated.
     */
    private fun updateSubBlocksCod(subBlocks: List<SubBlock>, codBlock: String): List<SubBlock> {
        var numberSubBlockCod =
            dataSource.subBlockDao.getNewNumberSubBlockCod(codBlock)
        val updatedSubBlock = ArrayList<SubBlock>()
        for (subBlock in subBlocks) {
            updatedSubBlock.add(updateSubBlockCod(subBlock, numberSubBlockCod, codBlock))
            numberSubBlockCod++
        }
        return updatedSubBlock
    }

    /**
     * Update the code of single subBlock.
     *
     * @param subBlock to update.
     * @param numberSubBlockCod to parse into a valid cod.
     * @param codBlock of subBlock.
     * @return an updated subBlocks.
     */
    private fun updateSubBlockCod(
        subBlock: SubBlock,
        numberSubBlockCod: Int,
        codBlock: String
    ): SubBlock {
        subBlock.cod = dataSource.subBlockDao.parseIntToCod(
            numberSubBlockCod,
            codBlock
        )
        subBlock.codBlock = codBlock
        subBlock.id = 0L
        return subBlock
    }

    /**
     * Calculate the final structure.
     *
     * @param _source of blockWithSubBlocks for search the selected subSubject.
     * @param _codSubSubject of blocks.
     * @return the result structure.
     */
    private suspend fun processStructure(
        _source: List<BlockWithSubBlocks>,
        _codSubSubject: String
    ): List<BlockWithSubBlocks> {
        val source = ArrayList<BlockWithSubBlocks>()
        for (block in _source) {
            source.add(block.copyImpl())
        }
        val resultStructure = ArrayList<BlockWithSubBlocks>()
        val target = ArrayList<BlockWithSubBlocks>(
            withContext(Dispatchers.IO) {
                dataSource.blockDao.getAllWithSubBlocksList(_codSubSubject)
            }
        )
        withContext(Dispatchers.IO) {
            for (blockWithSubBlocks in source) {
                resultStructure.add(
                    processBlockWithSubBlocks(
                        blockWithSubBlocks,
                        target,
                        _codSubSubject
                    )
                )
            }
        }
        if (target.isNotEmpty())
            resultStructure.addAll(target)

        return resultStructure.sortedBy { it.block.cod }
    }

    /**
     * Calculate the result blockWithSubBlocks structure.
     *
     * @param source starter structure of the block.
     * @param target structure to reach.
     * @param codSubSubject of block.
     * @return the result blockWithSubBlock structure.
     */
    private fun processBlockWithSubBlocks(
        source: BlockWithSubBlocks,
        target: ArrayList<BlockWithSubBlocks>,
        codSubSubject: String
    ): BlockWithSubBlocks {
        lateinit var blockWithSubBlocks: BlockWithSubBlocks
        var found = false
        var i = 0
        while (i < target.size && !found) {
            if ((source.block.type == null && target[i].block.name.equals(source.block.name, true))
                || (target[i].block.type != null && target[i].block.type == source.block.type)
            ) {
                target[i].block.let {
                    it.percent = source.block.percent
                    it.name = source.block.name
                }
                if (source.subBlocks.isNotEmpty()) {
                    _numberSubBlockCod =
                        dataSource.subBlockDao.getNewNumberSubBlockCod(target[i].block.cod)
                    target[i].subBlocks =
                        processSubBlocks(source.subBlocks, target[i].subBlocks, target[i].block.cod)
                }
                blockWithSubBlocks = target[i]
                target.removeAt(i)
                found = true
            }
            i++
        }
        if (!found) {
            blockWithSubBlocks = updateCode(source, _numberBlockCod, codSubSubject)
            _numberBlockCod++
            if (blockWithSubBlocks.subBlocks.isNotEmpty())
                blockWithSubBlocks.subBlocks =
                    updateSubBlocksCod(blockWithSubBlocks.subBlocks, blockWithSubBlocks.block.cod)
        }
        return blockWithSubBlocks
    }

    /**
     * Calculate the result subBlocks structure.
     *
     * @param source starter structure of the subBlocks.
     * @param _target structure to reach.
     * @param codBlock for update cod if the result add new subBlocks.
     * @return the result subBlocks structure.
     */
    private fun processSubBlocks(
        source: List<SubBlock>,
        _target: List<SubBlock>,
        codBlock: String
    ): List<SubBlock> {
        val target = ArrayList<SubBlock>()
        for (subBlock in _target)
            target.add(subBlock.copy())
        val result = ArrayList<SubBlock>()
        for (subBlock in source) {
            var found = false
            var i = 0
            while (i < target.size && !found) {
                if ((subBlock.type == null && target[i].name.equals(subBlock.name, true))
                    || (target[i].type != null && target[i].type == subBlock.type)
                ) {
                    target[i].let {
                        it.percent = subBlock.percent
                        it.name = subBlock.name
                    }
                    found = true
                    result.add(target[i])
                    target.removeAt(i)
                }
                i++
            }
            if (!found) {
                result.add(updateSubBlockCod(subBlock, _numberSubBlockCod, codBlock))
                _numberSubBlockCod++
            }
        }
        if (target.isNotEmpty()) {
            result.addAll(target)
        }
        return result.sortedBy { it.cod }
    }

    /**
     * Save and update the preview structure into persistence.
     *
     * @param structure to save and update.
     */
    private suspend fun copyStructureTo(
        structure: List<BlockWithSubBlocks>
    ) {
        try {
            withContext(Dispatchers.IO) {
                dataSource.blockDao.addNewsAndUpdateOlder(structure)
            }
            _navigateToDetailSubSubject.value = codSubSubject
        } catch (_: Exception) {
            _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
        }
    }
}

