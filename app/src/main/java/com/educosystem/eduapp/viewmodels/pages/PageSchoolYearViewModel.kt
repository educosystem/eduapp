/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels.pages

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.dao.SchoolYearDao
import com.educosystem.eduapp.data.entity.SchoolYear
import com.educosystem.eduapp.util.PageListener
import kotlinx.coroutines.*

class PageSchoolYearViewModel(
    val database: SchoolYearDao,
    val active: Boolean,
    application: Application
) : AndroidViewModel(application) {
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val resources = application.resources
    val schoolYears = database.getActive(active)
    private val _navigateToDetailSchoolYear = MutableLiveData<String?>()
    val navigateToDetailSchoolYear: LiveData<String?>
        get() = _navigateToDetailSchoolYear
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private val _actionModeActivated = MutableLiveData<Boolean>()
    val actionModeActivated: LiveData<Boolean>
        get() = _actionModeActivated
    private val _selectAllItems = MutableLiveData<Boolean>()
    val selectAllItems: LiveData<Boolean>
        get() = _selectAllItems
    private val _showDialogUpdate = MutableLiveData<Boolean>()
    val showDialogUpdate: LiveData<Boolean>
        get() = _showDialogUpdate
    private val _showDialogDelete = MutableLiveData<Boolean>()
    val showDialogDelete: LiveData<Boolean>
        get() = _showDialogDelete
    var selectedItems = ArrayList<SchoolYear>()
    val emptyTextViewVisibility = Transformations.map(schoolYears) {
        if (it.isEmpty()) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }
    private val _changeTitleActionMode = MutableLiveData(false)
    val changeTitleActionMode: LiveData<Boolean>
        get() = _changeTitleActionMode
    val titleActionMode: Int
        get() = selectedItems.size
    private var _listener: PageListener? = null
    val listener: PageListener?
        get() = _listener

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun messageTextView() =
        if (active)
            resources.getString(R.string.emptyActiveSchoolYear)
        else
            resources.getString(R.string.emptyFinishedSchoolYear)

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.DetailSchoolYearFragment].
     *
     * @param codSchoolYear
     */
    fun startNavigateToDetailSchoolYear(codSchoolYear: String) {
        _navigateToDetailSchoolYear.value = codSchoolYear
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailSchoolYear() {
        _navigateToDetailSchoolYear.value = null
    }

    /**
     * Activate the action mode.
     *
     */
    fun activateActionMode() {
        _actionModeActivated.value = true
    }

    /**
     * Change the selected state of the [schoolYear].
     *
     * @param schoolYear to select or deselect.
     */
    fun toggleItemSelected(schoolYear: SchoolYear) {
        if (selectedItems.contains(schoolYear)) {
            selectedItems.remove(schoolYear)
            if (selectedItems.isEmpty()) {
                _actionModeActivated.value = false
            }
        } else {
            selectedItems.add(schoolYear)
        }
        notifyChangeTitleActionMode()
    }

    /**
     * Change the selected state of all schoolYears to selected.
     *
     */
    fun selectAll() {
        selectedItems.clear()
        for (schoolYear: SchoolYear in schoolYears.value!!) {
            selectedItems.add(schoolYear)
        }
        _selectAllItems.value = true
        notifyChangeTitleActionMode()
    }

    /**
     * Finish notification of selection for prevent unwanted behaviour.
     *
     */
    fun selectAllDone() {
        _selectAllItems.value = false
    }

    /**
     * Notify to change the title of the action mode when a new item is selected.
     *
     */
    fun notifyChangeTitleActionMode() {
        _changeTitleActionMode.value = true
    }

    /**
     * Finish notification of change the title for prevent unwanted behaviour.
     *
     */
    fun changeTitleActionModeDone() {
        _changeTitleActionMode.value = false
    }

    /**
     * Finish action mode and restore the default behaviour.
     *
     */
    fun actionModeDone() {
        selectedItems.clear()
        _actionModeActivated.value = false
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Start show the confirm update.
     *
     */
    fun startDialogUpdate() {
        _showDialogUpdate.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun dialogUpdateDone() {
        _showDialogUpdate.value = false
    }

    /**
     * Start show the confirm delete.
     *
     */
    fun startDialogDelete() {
        _showDialogDelete.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun dialogDeleteDone() {
        _showDialogDelete.value = false
    }

    /**
     * Update the state school years.
     *
     */
    fun update() {
        if (active)
            markAsFinished()
        else
            markAsIncomplete()
    }

    /**
     * Mark the selected school years as finished.
     *
     */
    private fun markAsFinished() {
        val selectedItemsOld = ArrayList<SchoolYear>(selectedItems)
        _actionModeActivated.value = false

        uiScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    database.finishSchoolYear(*selectedItemsOld.toTypedArray())
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }

    /**
     * Mark the selected school years to active.
     *
     */
    private fun markAsIncomplete() {
        val selectedItemsOld = ArrayList<SchoolYear>(selectedItems)
        _actionModeActivated.value = false
        uiScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    database.markAsIncompleteSchoolYear(*selectedItemsOld.toTypedArray())
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }

    /**
     * Delete the selected school years.
     *
     */
    fun delete() {
        val selectedItemsOld = ArrayList<SchoolYear>(selectedItems)
        _actionModeActivated.value = false
        uiScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    database.delete(*selectedItemsOld.toTypedArray())
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorDelete)
            }
        }
    }


    /**
     * Set the page listener if it's not set yet.
     *
     * @param newListener
     */
    fun setListener(newListener: PageListener) {
        if (_listener == null) {
            _listener = newListener
        }
    }
}