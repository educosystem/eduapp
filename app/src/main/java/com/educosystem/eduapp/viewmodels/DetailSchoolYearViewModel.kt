/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.dao.SchoolYearDao
import kotlinx.coroutines.*

/**
 * ViewModel for ListActiveSchoolYearFragment
 */
class DetailSchoolYearViewModel(
    private val dataSource: SchoolYearDao,
    private val codSchoolYear: String,
    application: Application
) : AndroidViewModel(application) {
    private var viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val resources = application.resources
    private val schoolYear = dataSource.get(codSchoolYear)
    val active = Transformations.map(schoolYear) {
        it.active
    }
    val toolbarTitle = Transformations.map(schoolYear) {
        it.name
    }
    private val _navigateToAddSubject = MutableLiveData<String?>()
    val navigateToAddSubject: LiveData<String?>
        get() = _navigateToAddSubject
    private val _navigateToDetailSubject = MutableLiveData<String?>()
    val navigateToDetailSubject: LiveData<String?>
        get() = _navigateToDetailSubject
    private val _navigateToAddEvaluation = MutableLiveData<String?>()
    val navigateToAddEvaluation: LiveData<String?>
        get() = _navigateToAddEvaluation
    private val _navigateToDetailEvaluation = MutableLiveData<String?>()
    val navigateToDetailEvaluation: LiveData<String?>
        get() = _navigateToDetailEvaluation
    private val _showEditDialog = MutableLiveData(false)
    val showEditDialog: LiveData<Boolean>
        get() = _showEditDialog
    private val _showSnackBarMessage = MutableLiveData<String?>()
    val showSnackBarMessage: LiveData<String?>
        get() = _showSnackBarMessage
    private var _oldPosition = 0
    val oldPosition: Int
        get() = _oldPosition
    private val _hideButton = MutableLiveData<Boolean>()
    val hideButton: LiveData<Boolean>
        get() = _hideButton
    private val _showDialogUpdate = MutableLiveData<Boolean>()
    val showDialogUpdate: LiveData<Boolean>
        get() = _showDialogUpdate

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * Calculate the action of the fab button depending of the actual page.
     *
     */
    fun onAdd() {
        when (oldPosition) {
            0 -> startNavigateToAddSubject()
            1 -> startNavigateToAddEvaluation()
        }
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.AddSubjectFragment].
     *
     */
    private fun startNavigateToAddSubject() {
        _navigateToAddSubject.value = codSchoolYear
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddSubject() {
        _navigateToAddSubject.value = null
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.DetailSubjectFragment].
     *
     * @param codSubject
     */
    fun startNavigateToDetailSubject(codSubject: String) {
        _navigateToDetailSubject.value = codSubject
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailSubject() {
        _navigateToDetailSubject.value = null
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.AddEvaluationFragment].
     *
     */
    private fun startNavigateToAddEvaluation() {
        _navigateToAddEvaluation.value = codSchoolYear
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToAddEvaluation() {
        _navigateToAddEvaluation.value = null
    }

    /**
     * Start to navigate to [com.educosystem.eduapp.ui.DetailEvaluationFragment].
     *
     * @param codEvaluation
     */
    fun startNavigateToDetailEvaluation(codEvaluation: String) {
        _navigateToDetailEvaluation.value = codEvaluation
    }

    /**
     * Finish notification of navigation for prevent unwanted navigation.
     *
     */
    fun doneNavigatingToDetailEvaluation() {
        _navigateToDetailEvaluation.value = null
    }

    /**
     * Start show the edit dialog.
     *
     */
    fun startShowEditDialog() {
        _showEditDialog.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun doneShowEditDialog() {
        _showEditDialog.value = false
    }

    /**
     * Start show message on snackBar.
     *
     * @param message
     */
    fun startSnackBarMessage(message: String) {
        _showSnackBarMessage.value = message
    }

    /**
     * Finish notification of showing snackBar for prevent unwanted messages.
     *
     */
    fun showSnackBarDone() {
        _showSnackBarMessage.value = null
    }

    /**
     * Update the actual page position.
     *
     * @param position of the viewPager.
     */
    fun setLastPosition(position: Int) {
        _oldPosition = position
    }

    /**
     * Set the hide state of the fab button making necessary checks.
     *
     * @param hide status of fab button.
     */
    fun hideFabButton(hide: Boolean) {
        if (active.value!! || hide)
            _hideButton.value = hide
    }

    /**
     * Start show the update dialog.
     *
     */
    fun startDialogUpdate() {
        _showDialogUpdate.value = true
    }

    /**
     * Finish notification of showing dialog for prevent unwanted behaviour.
     *
     */
    fun dialogUpdateDone() {
        _showDialogUpdate.value = false
    }

    /**
     * Update the state of this schoolYear.
     *
     */
    fun update() {
        if (active.value!!)
            markAsFinished()
        else
            markAsIncomplete()
    }

    /**
     * Mark this schoolYear as finished.
     *
     */
    private fun markAsFinished() {
        uiScope.launch {
            val schoolYear = schoolYear.value!!
            try {
                withContext(Dispatchers.IO) {
                    dataSource.finishSchoolYear(schoolYear)
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }

    /**
     * Mark this schoolYear as active.
     *
     */
    private fun markAsIncomplete() {
        uiScope.launch {
            val schoolYear = schoolYear.value!!
            try {
                withContext(Dispatchers.IO) {
                    dataSource.markAsIncompleteSchoolYear(schoolYear)
                }
            } catch (exception: Exception) {
                _showSnackBarMessage.value = resources.getString(R.string.errorUpdate)
            }
        }
    }
}