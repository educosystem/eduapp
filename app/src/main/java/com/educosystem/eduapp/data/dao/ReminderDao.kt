/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.educosystem.eduapp.data.entity.Reminder
import com.educosystem.eduapp.data.entity.ReminderElement
import com.educosystem.eduapp.data.entity.ReminderWithElements

@Dao
interface ReminderDao {
    /**
     * @param date of the reminder
     * @return an observable list of reminders with elements for the [date]
     */
    @Transaction
    @Query("SELECT * FROM REMINDERS WHERE date=:date")
    fun get(date: Long): LiveData<ReminderWithElements?>

    /**
     * @param reminder to add
     */
    @Insert
    fun add(vararg reminder: Reminder)

    /**
     * @param reminder to add
     */
    @Transaction
    @Insert
    fun add(vararg reminder: ReminderElement)

    /**
     * @param reminder to update
     */
    @Update
    fun update(vararg reminder: Reminder)

    /**
     * @param reminder to update
     */
    @Transaction
    @Update
    fun update(vararg reminder: ReminderElement)

    /**
     * @param reminder to delete
     */
    @Delete
    fun delete(vararg reminder: Reminder)

    /**
     * @param reminder to delete
     */
    @Transaction
    @Delete
    fun delete(vararg reminder: ReminderElement)
}