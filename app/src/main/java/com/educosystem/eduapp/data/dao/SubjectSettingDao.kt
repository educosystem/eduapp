/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.educosystem.eduapp.data.entity.SubjectSetting

@Dao
interface SubjectSettingDao {
    /**
     * @param cod of the subject.
     * @return an observable list of subjectSettings.
     */
    @Query("SELECT * FROM SUBJECTS_SETTINGS WHERE cod_subject=:cod")
    fun get(cod: String): LiveData<List<SubjectSetting>>

    /**
     * @param subjectSetting to add.
     */
    @Insert
    fun add(vararg subjectSetting: SubjectSetting)

    /**
     * @param subjectSetting to update.
     */
    @Update
    fun update(vararg subjectSetting: SubjectSetting)

    /**
     * @param subjectSetting to delete.
     */
    @Delete
    fun delete(vararg subjectSetting: SubjectSetting)
}