/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.educosystem.eduapp.data.entity.Block
import com.educosystem.eduapp.data.entity.BlockWithSubBlocks
import com.educosystem.eduapp.data.entity.SubBlock
import com.educosystem.eduapp.data.entity.SubSubject

@Dao
abstract class BlockDao {
    /**
     * @param cod of the block.
     * @return an observable block.
     * @see getOnly for get only the value.
     */
    @Query("SELECT * FROM BLOCKS WHERE cod=:cod")
    abstract fun get(cod: String): LiveData<Block>

    /**
     * @param cod of the block.
     * @return a nullable block.
     * @see get for get an observable.
     */
    @Query("SELECT * FROM BLOCKS WHERE cod=:cod")
    abstract fun getOnly(cod: String): Block?

    /**
     * @param cod of the subBlock.
     * @return an nullable subBlock.
     */
    @Query("SELECT * FROM SUB_BLOCKS WHERE cod=:cod")
    protected abstract fun getSubBlock(cod: String): SubBlock?

    /**
     * @param codSubBlock
     * @return an observable block.
     */
    @Query("SELECT * FROM BLOCKS WHERE cod=(SELECT cod_block FROM SUB_BLOCKS WHERE cod=:codSubBlock)")
    abstract fun getFromSubBlock(codSubBlock: String): LiveData<Block>

    /**
     * @param subSubject cod.
     * @return an observable list of blocks.
     * @see getAllFromSubSubjectList for get only the value.
     */
    @Query("SELECT * FROM BLOCKS WHERE cod_sub_subject=:subSubject ORDER BY name")
    abstract fun getAllFromSubSubject(subSubject: String): LiveData<List<Block>>

    /**
     * @param subSubject cod.
     * @return a list of blocks.
     * @see getAllFromSubSubject for get an observable.
     */
    @Query("SELECT * FROM BLOCKS WHERE cod_sub_subject=:subSubject")
    abstract fun getAllFromSubSubjectList(subSubject: String): List<Block>

    /**
     * @param cod of block.
     * @return a block with subBlocks.
     */
    @Transaction
    @Query("SELECT * FROM BLOCKS WHERE cod=:cod")
    abstract fun getWithSubBlocksOnly(cod: String): BlockWithSubBlocks

    /**
     * @param subSubject cod.
     * @return an observable list of block with subBlocks.
     * @see getAllWithSubBlocksList for get only the value.
     */
    @Transaction
    @Query("SELECT * FROM BLOCKS WHERE cod_sub_subject=:subSubject ORDER BY name")
    abstract fun getAllWithSubBlocks(subSubject: String): LiveData<List<BlockWithSubBlocks>>

    /**
     * @param subSubject cod.
     * @return a list of block with subBlocks.
     * @see getAllWithSubBlocks for get an observable.
     */
    @Transaction
    @Query("SELECT * FROM BLOCKS WHERE cod_sub_subject=:subSubject")
    abstract fun getAllWithSubBlocksList(subSubject: String): List<BlockWithSubBlocks>

    /**
     * @return max cod of the blocks.
     */
    @Query("SELECT MAX(cod) FROM BLOCKS")
    abstract fun getMaxCod(): String?

    /**
     * @param block to add.
     */
    @Insert
    abstract fun add(vararg block: Block)

    /**
     * @param subBlocks to add.
     */
    @Insert
    protected abstract fun add(vararg subBlocks: SubBlock)

    /**
     * @param block to update.
     */
    @Update
    abstract fun update(vararg block: Block)

    /**
     * @param subBlocks to update.
     */
    @Update
    protected abstract fun update(vararg subBlocks: SubBlock)

    /**
     * @param block to delete.
     */
    @Delete
    abstract fun delete(vararg block: Block)

    /**
     * Add a block with subBlocks.
     *
     * @param blocksWithSubBlocks to add.
     */
    @Transaction
    open fun addWithSubBlocks(vararg blocksWithSubBlocks: BlockWithSubBlocks) {
        for (blockWithSubBlocks: BlockWithSubBlocks in blocksWithSubBlocks) {
            add(blockWithSubBlocks.block)
            if (blockWithSubBlocks.subBlocks.isNotEmpty())
                add(*blockWithSubBlocks.subBlocks.toTypedArray())
        }
    }

    /**
     * @return new number cod of blocks.
     */
    open fun getNewNumberBlockCod(): Int {
        val blockCod = getMaxCod()
        return if (blockCod != null) {
            Integer.parseInt(blockCod) + 1
        } else {
            1
        }
    }

    /**
     * @return new valid cod of block.
     */
    open fun getNewBlockCod(): String = parseIntToCod(getNewNumberBlockCod())

    /**
     * @param number to parse into a valid cod.
     * @return a valid cod parsed from [number].
     */
    open fun parseIntToCod(number: Int): String =
        when (number) {
            in 1..9 -> "0000$number"
            in 10..99 -> "000$number"
            in 100..999 -> "00$number"
            in 1000..9999 -> "0$number"
            else -> "$number"
        }


    /**
     * @param subSubjects to delete.
     */
    @Transaction
    open fun delete(vararg subSubjects: SubSubject) {
        for (subSubject in subSubjects) {
            delete(*getAllFromSubSubjectList(subSubject.cod).toTypedArray())
        }
    }

    /**
     * Add new structure of blocks with sub blocks and update the older structure
     * depending if cod exist or no.
     *
     * @param blocksWithSubBlocks to add and update.
     */
    @Transaction
    open fun addNewsAndUpdateOlder(blocksWithSubBlocks: List<BlockWithSubBlocks>) {
        for (blockWithSubBlocks in blocksWithSubBlocks) {
            if (getOnly(blockWithSubBlocks.block.cod) == null) {
                addWithSubBlocks(blockWithSubBlocks)
            } else {
                update(blockWithSubBlocks.block)
                for (subBock in blockWithSubBlocks.subBlocks) {
                    if (getSubBlock(subBock.cod) == null)
                        add(subBock)
                    else
                        update(subBock)
                }
            }
        }
    }

}