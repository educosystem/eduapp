/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NotNull

@Entity(tableName = "CREDENTIALS", indices = [Index("cod", unique = true)])
data class Credential(
    @NotNull var cod: String,
    @NotNull var username: String,
    @NotNull var password: String,
    @NotNull var domain: String,
    @PrimaryKey(autoGenerate = true) var id: Long = 0L,
    var codSynchronizedUser: String? = null,
    var firstName: String? = null,
    var surname1: String? = null,
    var surname2: String? = null,
    var urlProfilePhoto: String? = null
)