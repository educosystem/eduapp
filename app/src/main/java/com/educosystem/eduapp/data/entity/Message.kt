/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.entity

import androidx.annotation.NonNull
import androidx.room.*
import java.util.*

@Entity(
    tableName = "MESSAGES",
    indices = [
        Index("cod_sender_user"),
        Index("cod_receiver_user")
    ],
    foreignKeys = [
        ForeignKey(
            entity = UserData::class,
            parentColumns = ["cod"],
            childColumns = ["cod_sender_user"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = UserData::class,
            parentColumns = ["cod"],
            childColumns = ["cod_receiver_user"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Message(
    @ColumnInfo(name = "cod_sender_user") var codSenderUser: String,
    @ColumnInfo(name = "cod_receiver_user") var codReceiverUser: String,
    @NonNull @ColumnInfo(name = "shipping_date") var shippingDate: Calendar,
    @NonNull var text: String,
    @NonNull var synced: Boolean = false,
    @NonNull var read: Boolean = true,
    @PrimaryKey(autoGenerate = true) var id: Long = 0L
)