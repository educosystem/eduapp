/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.entity

import androidx.annotation.NonNull
import androidx.room.*
import java.util.*

@Entity(
    tableName = "FILES_ELEMENTS",
    indices = [
        Index("cod", unique = true),
        Index("cod_element")
    ],
    foreignKeys = [
        ForeignKey(
            entity = Element::class,
            parentColumns = ["cod"],
            childColumns = ["cod_element"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class FileElement(
    @NonNull var cod: String,
    @NonNull @ColumnInfo(name = "cod_element") var codElement: String,
    @NonNull @ColumnInfo(name = "cod_synchronized_file") var codSynchronizedFile: String,
    @NonNull var type: String,
    @NonNull var name: String,
    @NonNull var route: String,
    @NonNull @ColumnInfo(name = "delivered_date") var deliveredDate: Calendar,
    @ColumnInfo(name = "corrected_route") var correctedRoute: String? = null,
    @PrimaryKey(autoGenerate = true) var id: Long = 0L
)