/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.educosystem.eduapp.data.entity.FileElement

@Dao
interface FileElementDao {

    /**
     * @param cod of the fileElement.
     * @return an observable fileElement.
     */
    @Query("SELECT * FROM FILES_ELEMENTS WHERE cod=:cod")
    fun get(cod: String): LiveData<FileElement>

    /**
     * @param element to search.
     * @return an observable list of fileElements from elements.
     */
    @Query("SELECT * FROM FILES_ELEMENTS WHERE cod_element=:element")
    fun getAllFromElement(element: String): LiveData<List<FileElement>>

    /**
     * @param fileElement to add.
     */
    @Insert
    fun add(vararg fileElement: FileElement)

    /**
     * @param fileElement to update.
     */
    @Update
    fun update(vararg fileElement: FileElement)

    /**
     * @param fileElement to delete.
     */
    @Delete
    fun delete(vararg fileElement: FileElement)
}