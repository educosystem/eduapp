/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.entity

import androidx.annotation.NonNull
import androidx.room.*
import java.util.*

@Entity(
    tableName = "EVALUATIONS",
    indices = [Index("cod", unique = true), Index("cod_school_year")],
    foreignKeys = [ForeignKey(
        entity = SchoolYear::class, parentColumns = ["cod"],
        childColumns = ["cod_school_year"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    )]
)
data class Evaluation(
    @NonNull var cod: String,
    @NonNull @ColumnInfo(name = "cod_school_year") var codSchoolYear: String,
    @NonNull var name: String,
    @NonNull var position: Int,
    @PrimaryKey(autoGenerate = true) var id: Long = 0L,
    @ColumnInfo(name = "cod_synchronized_evaluation") var codSynchronizedEvaluation: String? = null,
    @ColumnInfo(name = "start_date") var startDate: Calendar? = null,
    @ColumnInfo(name = "finish_date") var finishDate: Calendar? = null,
    @NonNull @ColumnInfo(name = "extra_ordinary") var extraOrdinary: Boolean = false
) {
    override fun toString(): String {
        return name
    }
}