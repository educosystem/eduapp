/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.entity

import androidx.annotation.NonNull
import androidx.room.*
import java.util.*

@Entity(
    tableName = "SCHOOL_YEARS",
    indices = [Index("cod", unique = true), Index("cod_credential")],
    foreignKeys = [ForeignKey(
        entity = Credential::class,
        parentColumns = ["cod"],
        childColumns = ["cod_credential"],
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.SET_NULL
    )]
)
data class SchoolYear(
    @NonNull var cod: String,
    @NonNull var name: String,
    @NonNull @ColumnInfo(name = "start_date") var startDate: Calendar = Calendar.getInstance(),
    @PrimaryKey(autoGenerate = true) var id: Long = 0L,
    @ColumnInfo(name = "finish_date") var finishDate: Calendar? = null,
    @ColumnInfo(name = "cod_credential") var codCredential: String? = null,
    @NonNull var active: Boolean = true
)