/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.educosystem.eduapp.data.entity.Credential

@Dao
interface CredentialDao {
    /**
     * @return an observable list of credentials
     */
    @Query("SELECT * FROM CREDENTIALS")
    fun getAll(): LiveData<List<Credential>>

    /**
     * @return max code of credentials
     */
    @Query("SELECT MAX(cod) FROM CREDENTIALS")
    fun getMaxCod(): String?

    /**
     * @param cod of credential
     * @return an observable credential
     */
    @Query("SELECT * FROM CREDENTIALS WHERE cod = :cod")
    fun get(cod: String): LiveData<Credential>

    /**
     * @param credential to add
     */
    @Insert
    fun add(credential: Credential)

    /**
     * @param credential to update
     */
    @Update
    fun update(credential: Credential)

    /**
     * @param credential to delete
     */
    @Delete
    fun delete(credential: Credential)
}