/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.entity

import androidx.annotation.NonNull
import androidx.room.*

@Entity(
    tableName = "SUB_BLOCKS",
    indices = [
        Index("cod", unique = true),
        Index("cod_block"),
        Index("cod_repeat_sub_block")
    ],
    foreignKeys = [
        ForeignKey(
            entity = Block::class,
            parentColumns = ["cod"],
            childColumns = ["cod_block"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = SubBlock::class,
            parentColumns = ["cod"],
            childColumns = ["cod_repeat_sub_block"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.SET_NULL
        )
    ]
)
data class SubBlock(
    @NonNull var cod: String,
    @NonNull @ColumnInfo(name = "cod_block") var codBlock: String,
    @NonNull var name: String,
    @PrimaryKey(autoGenerate = true) var id: Long = 0L,
    var percent: Int? = null,
    var type: Int? = null,
    var grade: Double? = null,
    @ColumnInfo(name = "cod_synchronized_sub_block") var codSynchronizedSubBlock: String? = null,
    @ColumnInfo(name = "cod_repeat_sub_block") var codRepeatSubBlock: String? = null,
    @NonNull @ColumnInfo(name = "mode_repeat") var modeRepeat: Boolean = false,
    @ColumnInfo(name = "grade_comment") var gradeComment: String? = null
) {
    override fun toString(): String {
        return name
    }
}