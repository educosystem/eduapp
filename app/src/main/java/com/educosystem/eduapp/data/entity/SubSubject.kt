/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.entity

import androidx.annotation.NonNull
import androidx.room.*

@Entity(
    tableName = "SUB_SUBJECTS",
    indices = [
        Index("cod", unique = true),
        Index("cod_subject"),
        Index("cod_evaluation"),
        Index("cod_subject", "cod_evaluation", unique = true)
    ],
    foreignKeys = [
        ForeignKey(
            entity = Subject::class,
            parentColumns = ["cod"],
            childColumns = ["cod_subject"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Evaluation::class,
            parentColumns = ["cod"],
            childColumns = ["cod_evaluation"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class SubSubject(
    @NonNull var cod: String,
    @NonNull @ColumnInfo(name = "cod_subject") var codSubject: String,
    @NonNull @ColumnInfo(name = "cod_evaluation") var codEvaluation: String,
    @ColumnInfo(name = "final_grade") var finalGrade: Double? = null,
    @ColumnInfo(name = "grade_comment") var gradeComment: String? = null,
    @PrimaryKey(autoGenerate = true) var id: Long = 0L,
    @NonNull var active: Boolean = false
)