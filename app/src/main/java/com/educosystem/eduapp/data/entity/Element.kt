/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.entity

import androidx.annotation.NonNull
import androidx.room.*
import java.util.*

@Entity(
    tableName = "ELEMENTS",
    indices = [
        Index("cod", unique = true),
        Index("cod_block"),
        Index("cod_sub_block")
    ],
    foreignKeys = [
        ForeignKey(
            entity = Block::class,
            parentColumns = ["cod"],
            childColumns = ["cod_block"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = SubBlock::class,
            parentColumns = ["cod"],
            childColumns = ["cod_sub_block"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Element(
    @NonNull var cod: String,
    @NonNull @ColumnInfo(name = "cod_block") var codBlock: String,
    @NonNull var type: Int,
    @ColumnInfo(name = "cod_sub_block") var codSubBlock: String? = null,
    @NonNull @ColumnInfo(name = "insert_date") var insertDate: Calendar = Calendar.getInstance(),
    var name: String? = null,
    var position: Int? = null,
    @NonNull var multiplier: Int = 1,
    @PrimaryKey(autoGenerate = true) var id: Long = 0L,
    @ColumnInfo(name = "delivery_date") var deliveryDate: Calendar? = null,
    @ColumnInfo(name = "delivered_date") var deliveredDate: Calendar? = null,
    var grade: Double? = null,
    @ColumnInfo(name = "cod_synchronized_element") var codSynchronizedElement: String? = null,
    var completed: Boolean? = null,
    @ColumnInfo(name = "grade_comment") var gradeComment: String? = null,
    var url: String? = null,
    var description: String? = null,
    var fileName: String? = null,
    var fileType: String? = null,
    var fileRoute: String? = null,
    @NonNull var realized: Boolean = false
)