/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.educosystem.eduapp.data.entity.Message

@Dao
interface MessageDao {

    /**
     * @param cod of the user.
     * @return an observable list of messages from the user [cod].
     */
    @Query("SELECT * FROM MESSAGES WHERE cod_receiver_user=:cod or cod_sender_user=:cod ORDER BY shipping_date ASC")
    fun get(cod: String): LiveData<List<Message>>

    /**
     * @param message to add.
     */
    @Insert
    fun add(message: Message)

    /**
     * @param message to update.
     */
    @Update
    fun update(message: Message)

    /**
     * @param message to delete.
     */
    @Delete
    fun delete(vararg message: Message)
}