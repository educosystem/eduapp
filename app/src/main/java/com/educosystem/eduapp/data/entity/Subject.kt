/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.entity

import androidx.annotation.NonNull
import androidx.room.*

@Entity(
    tableName = "SUBJECTS",
    indices = [
        Index("cod", unique = true),
        Index("cod_school_year"),
        Index("cod_previous_subject"),
        Index("cod_repeat_subject"),
        Index("cod_credential")
    ],
    foreignKeys = [
        ForeignKey(
            entity = SchoolYear::class,
            parentColumns = ["cod"],
            childColumns = ["cod_school_year"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Subject::class,
            parentColumns = ["cod"],
            childColumns = ["cod_previous_subject"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.SET_NULL
        ),
        ForeignKey(
            entity = Subject::class,
            parentColumns = ["cod"],
            childColumns = ["cod_repeat_subject"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.SET_NULL
        ),
        ForeignKey(
            entity = Credential::class,
            parentColumns = ["cod"],
            childColumns = ["cod_credential"],
            onDelete = ForeignKey.SET_NULL,
            onUpdate = ForeignKey.CASCADE
        )
    ]
)
data class Subject(
    @NonNull var cod: String,
    @NonNull @ColumnInfo(name = "cod_school_year") var codSchoolYear: String,
    @NonNull var name: String,
    @NonNull @ColumnInfo(name = "continuous_assessment") var continuousAssessment: Boolean = true,
    @PrimaryKey(autoGenerate = true) var id: Long = 0L,
    @NonNull var sync: Boolean = false,
    @ColumnInfo(name = "final_grade") var finalGrade: Double? = null,
    @ColumnInfo(name = "grade_comment") var gradeComment: String? = null,
    @ColumnInfo(name = "cod_previous_subject") var codPreviousSubject: String? = null,
    @NonNull var colour: Int = 0,
    @ColumnInfo(name = "cod_repeat_subject") var codRepeatSubject: String? = null,
    @NonNull @ColumnInfo(name = "repeat_mode") var repeatMode: Boolean = false,
    @ColumnInfo(name = "cod_credential") var codCredential: String? = null,
    @NonNull var active: Boolean = false
) {
    override fun toString(): String {
        return name
    }
}