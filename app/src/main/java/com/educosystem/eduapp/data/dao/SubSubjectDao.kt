/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.educosystem.eduapp.data.entity.Evaluation
import com.educosystem.eduapp.data.entity.SubSubject
import com.educosystem.eduapp.data.entity.Subject
import com.educosystem.eduapp.util.TYPE_EXAM
import com.educosystem.eduapp.util.TYPE_HOMEWORK
import com.educosystem.eduapp.util.TYPE_WORK
import java.util.*
import kotlin.collections.ArrayList

@Dao
abstract class SubSubjectDao {
    /**
     * @param cod of the subSubject.
     * @return an observable subSubject.
     * @see getOnly for get only the value.
     */
    @Query("SELECT * FROM SUB_SUBJECTS WHERE cod=:cod")
    abstract fun get(cod: String): LiveData<SubSubject>

    /**
     * @param cod of the subSubject.
     * @return a subSubject.
     * @see get for get an observable.
     */
    @Query("SELECT * FROM SUB_SUBJECTS WHERE cod=:cod")
    abstract fun getOnly(cod: String): SubSubject

    /**
     * @param subject for search.
     * @return the active subSubject of this [subject].
     */
    @Query("SELECT * FROM SUB_SUBJECTS WHERE cod_subject=:subject AND active = 1")
    abstract fun getActiveFromSubject(subject: String): SubSubject

    /**
     * @param codSubject for get other subSubjects.
     * @param codSubSubject for evaluate if this subSubject is valid.
     * @return an observable value of if exists other subSubjects of [codSubject] and
     * if this [codSubSubject] have a valid structure.
     */
    @Query(
        """SELECT COUNT(*)>1 FROM SUB_SUBJECTS 
            WHERE cod_subject = :codSubject
            AND :codSubSubject IN (SELECT DISTINCT cod_sub_subject FROM BLOCKS)"""
    )
    abstract fun existOtherSubSubjectAndSubSubjectIsValid(
        codSubject: String,
        codSubSubject: String
    ): LiveData<Boolean>

    /**
     * @param codSubSubject for evaluate.
     * @param types of the valid blocks or subBlocks.
     * @return an observable value of if the subSubject can add elements.
     */
    @Query(
        """SELECT COUNT(*)>0 FROM BLOCKS 
            WHERE cod_sub_subject = :codSubSubject
            AND (type IS NOT NULL AND type IN (:types)
            OR cod IN (SELECT cod_block FROM SUB_BLOCKS WHERE type IS NOT NULL AND type IN (:types)))"""
    )
    abstract fun subSubjectCanAddElements(
        codSubSubject: String,
        types: List<Int> = listOf(TYPE_EXAM, TYPE_WORK, TYPE_HOMEWORK)
    ): LiveData<Boolean>

    /**
     * @param codSubject for get other subSubjects.
     * @param codSubSubject to exclude.
     * @return an observable value of if exists other subSubjects that have a valid structure.
     */
    @Query(
        """SELECT COUNT(*)!=0 FROM SUB_SUBJECTS
        WHERE cod_subject = :codSubject
        AND 0 < (SELECT COUNT(*) FROM SUB_SUBJECTS 
        WHERE cod_subject=:codSubject 
        AND cod != :codSubSubject
        AND cod IN (SELECT DISTINCT cod_sub_subject FROM BLOCKS)
        )
    """
    )
    abstract fun existOtherSubSubjectWithValidStructure(
        codSubject: String,
        codSubSubject: String
    ): LiveData<Boolean>

    /**
     * @param evaluation cod of the subSubject.
     * @param subject cod of the subSubject.
     * @return a Subject of this [subject] and [evaluation].
     */
    @Query("SELECT * FROM SUB_SUBJECTS WHERE cod_subject=:subject AND cod_evaluation=:evaluation")
    abstract fun get(evaluation: String, subject: String): SubSubject

    /**
     * @param subject to search.
     * @return an observable value of subSubject list of this [subject].
     */
    @Query("SELECT * FROM SUB_SUBJECTS WHERE cod_subject=:subject")
    abstract fun getAllFromSubject(subject: String): LiveData<List<SubSubject>>

    /**
     * @param evaluation to search.
     * @return an observable value of subSubject list of this [evaluation].
     */
    @Query("SELECT * FROM SUB_SUBJECTS WHERE cod_evaluation=:evaluation")
    abstract fun getAllFromEvaluation(evaluation: String): LiveData<List<SubSubject>>

    /**
     * @param cod of subject.
     * @return max code that start with [cod].
     */
    @Query("SELECT MAX(cod) FROM SUB_SUBJECTS WHERE cod like :cod||'%'")
    abstract fun getMaxCod(cod: String): String?

    /**
     * @param codSubject to search.
     * @param position of evaluation.
     * @return a list of subSubject cod from this [codSubject] and
     * only until the specified [position] of evaluation.
     */
    @Query(
        """SELECT s.cod FROM SUB_SUBJECTS as s INNER JOIN EVALUATIONS as e
         ON s.cod_evaluation=e.cod
        WHERE s.cod_subject=:codSubject 
        AND e.position < :position
        AND extra_ordinary = 0
        ORDER BY e.position DESC"""
    )
    abstract fun getCodesFromSubjectOnlyUtilPositionOfEvaluation(
        codSubject: String,
        position: Int
    ): List<String>

    /**
     * @param codSubject to search.
     * @return a list of extraordinary subSubject cod from this [codSubject].
     */
    @Query(
        """SELECT s.cod FROM SUB_SUBJECTS as s INNER JOIN EVALUATIONS as e
         ON s.cod_evaluation=e.cod
        WHERE s.cod_subject=:codSubject 
        AND e.extra_ordinary = 1
        ORDER BY e.position DESC"""
    )
    abstract fun getCodesFromSubjectOnlyOfExtraordinaryEvaluations(
        codSubject: String
    ): List<String>

    /**
     * @param codSubject to search.
     * @return a list of evaluations from this [codSubject].
     */
    @Query(
        """SELECT * FROM EVALUATIONS 
            WHERE cod IN (SELECT cod_evaluation FROM SUB_SUBJECTS WHERE cod_subject=:codSubject)
            ORDER BY position"""
    )
    protected abstract fun getEvaluationsFromSubject(codSubject: String): List<Evaluation>

    /**
     * Update subSubject state to inactive of the specified subject.
     *
     * @param codSubject to update subSubject state.
     */
    @Query(
        """UPDATE SUB_SUBJECTS SET active=0 WHERE cod_subject=:codSubject"""
    )
    protected abstract fun resetActive(codSubject: String)

    /**
     * @param subSubject to add.
     */
    @Insert
    abstract fun add(vararg subSubject: SubSubject)

    /**
     * @param subSubject to update.
     */
    @Update
    abstract fun update(vararg subSubject: SubSubject)

    /**
     * @param evaluations to update.
     */
    @Update
    protected abstract fun update(vararg evaluations: Evaluation)

    /**
     * @param subSubject to delete.
     */
    @Delete
    abstract fun delete(vararg subSubject: SubSubject)

    /**
     * Delete the subSubject of the specified evaluation and subjects.
     *
     * @param evaluation cod.
     * @param subjects
     */
    open fun delete(evaluation: String, vararg subjects: Subject) {
        val subSubjects = ArrayList<SubSubject>()
        for (subject in subjects) {
            subSubjects.add(get(evaluation, subject.cod))
        }
        delete(*subSubjects.toTypedArray())
    }

    /**
     * Delete the subSubject of the specified subject and evaluations.
     *
     * @param subject cod.
     * @param evaluations
     */
    open fun delete(subject: String, vararg evaluations: Evaluation) {
        val subSubjects = ArrayList<SubSubject>()
        for (evaluation in evaluations) {
            subSubjects.add(get(evaluation.cod, subject))
        }
        delete(*subSubjects.toTypedArray())
    }

    /**
     * @param codSubject for generate the new cod.
     * @return new number cod of subSubject.
     */
    open fun getNewNumberSubSubjectCod(codSubject: String): Int {
        val subjectCod = getMaxCod(codSubject)
        return if (subjectCod != null) {
            Integer.parseInt(subjectCod.substring(8)) + 1
        } else {
            1
        }
    }

    /**
     * @param codSubject for generate the new cod.
     * @return new valid cod of subSubject.
     */
    open fun getNewSubSubjectCod(codSubject: String): String =
        parseIntToCod(getNewNumberSubSubjectCod(codSubject), codSubject)

    /**
     * @param number to parse into a valid cod.
     * @param codSubject for parse into a valid cod.
     * @return a valid cod parsed from [number] and [codSubject].
     */
    open fun parseIntToCod(number: Int, codSubject: String): String =
        when (number) {
            in 1..9 -> "${codSubject}0$number"
            else -> "$codSubject$number"
        }

    /**
     * Mark as complete this subSubject and active next.
     *
     * @param subSubject to mark as complete.
     */
    @Transaction
    open fun finishAndActiveNext(subSubject: SubSubject) {
        val evaluations = getEvaluationsFromSubject(subSubject.codSubject)
        var evaluationToActive: Evaluation? = null
        var found = false
        var i = 0
        while (i < evaluations.size && !found) {
            if (evaluations[i].cod == subSubject.codEvaluation) {
                if (i + 1 < evaluations.size)
                    evaluationToActive = evaluations[i + 1]
                found = true
            }
            i++
        }
        subSubject.active = false
        update(subSubject)
        evaluationToActive?.let {
            if (it.startDate == null) {
                it.startDate = Calendar.getInstance()
                update(it)
            }
            active(get(it.cod, subSubject.codSubject))
        }
    }

    /**
     * Mark as active this subSubject
     *
     * @param subSubject to active
     */
    @Transaction
    open fun active(subSubject: SubSubject) {
        resetActive(subSubject.codSubject)
        subSubject.active = true
        update(subSubject)
    }
}