/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.educosystem.eduapp.data.entity.Requirement

@Dao
interface RequirementDao {

    /**
     * @param cod of the requirement.
     * @return an observable requirement.
     */
    @Query("SELECT * FROM REQUIREMENTS WHERE cod=:cod")
    fun get(cod: String): LiveData<Requirement>

    /**
     * @param element cod.
     * @return an observable list of requirement from the [element].
     */
    @Query("SELECT * FROM REQUIREMENTS WHERE cod_element=:element")
    fun getAllFromElement(element: String): LiveData<List<Requirement>>

    /**
     * @param element cod.
     * @param completed if the requirement is completed.
     * @return an observable list of requirement from the [element] and [completed] state.
     */
    @Query("SELECT * FROM REQUIREMENTS WHERE cod_element=:element AND completed=:completed")
    fun getAllFromElement(element: String, completed: Boolean): LiveData<List<Requirement>>

    /**
     * @param requirement to add.
     */
    @Insert
    fun add(vararg requirement: Requirement)

    /**
     * @param requirement to update.
     */
    @Update
    fun update(vararg requirement: Requirement)

    /**
     * @param requirement to delete.
     */
    @Delete
    fun delete(vararg requirement: Requirement)
}