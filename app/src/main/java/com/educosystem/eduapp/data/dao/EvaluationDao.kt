/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.educosystem.eduapp.data.entity.Evaluation
import java.util.*
import kotlin.collections.ArrayList

@Dao
abstract class EvaluationDao {
    /**
     * @param schoolYear cod.
     * @return an observable list of evaluations.
     */
    @Query("SELECT * FROM EVALUATIONS WHERE cod_school_year=:schoolYear ORDER BY position ASC")
    abstract fun getAllFromSchoolYear(schoolYear: String): LiveData<List<Evaluation>>

    /**
     * @param subject cod.
     * @return an observable list of evaluations.
     */
    @Query(
        """SELECT * FROM EVALUATIONS 
        WHERE cod IN (SELECT cod_evaluation FROM SUB_SUBJECTS WHERE cod_subject=:subject) 
        ORDER BY position"""
    )
    abstract fun getAllFromSubject(subject: String): LiveData<List<Evaluation>>

    /**
     * @param subject cod.
     * @return an observable active evaluation.
     */
    @Query(
        """SELECT * FROM EVALUATIONS 
        WHERE cod IN (SELECT cod_evaluation FROM SUB_SUBJECTS 
        WHERE cod_subject=:subject
        AND active = 1
        ) 
        ORDER BY position"""
    )
    abstract fun getActiveEvaluationFromSubject(
        subject: String
    ): LiveData<Evaluation?>

    /**
     * @param codEvaluation to search active subSubjects.
     * @return if the evaluation with [codEvaluation] have active subSubjects.
     */
    @Query(
        """SELECT COUNT(*)!=0 FROM SUB_SUBJECTS
            WHERE cod_evaluation = :codEvaluation
            AND active = 1"""
    )
    abstract fun haveActiveSubSubject(codEvaluation: String): Boolean

    /**
     * @param subject to search.
     * @param subSubject to exclude.
     * @return an observable list of evaluations from [subject] without the [subSubject] excluded.
     */
    @Query(
        """SELECT * FROM EVALUATIONS 
        WHERE cod IN (SELECT cod_evaluation FROM SUB_SUBJECTS 
        WHERE cod_subject=:subject
        AND cod != :subSubject
        ) 
        ORDER BY position"""
    )
    abstract fun getAllFromSubjectWithoutSubSubject(
        subject: String,
        subSubject: String
    ): LiveData<List<Evaluation>>

    /**
     * @param codSubject to search.
     * @param codSubSubject to exclude.
     * @return an observable list of evaluations without [codSubSubject] excluded
     * which subSubject of [codSubject] have a valid structure.
     */
    @Query(
        """SELECT * FROM EVALUATIONS
        WHERE cod IN (SELECT cod_evaluation FROM SUB_SUBJECTS 
        WHERE cod_subject=:codSubject 
        AND cod != :codSubSubject
        AND cod IN (SELECT DISTINCT cod_sub_subject FROM BLOCKS)
        )
    """
    )
    abstract fun getOtherSubSubjectWithValidStructure(
        codSubject: String,
        codSubSubject: String
    ): LiveData<List<Evaluation>>

    /**
     * @param subject to search.
     * @return an observable list of evaluations that don't have any subSubject of this [subject].
     */
    @Query(
        """SELECT * FROM EVALUATIONS 
        WHERE finish_date IS NULL
        AND cod_school_year= (SELECT cod_school_year FROM SUBJECTS WHERE cod=:subject)
        AND cod NOT IN (SELECT cod_evaluation FROM SUB_SUBJECTS WHERE cod_subject=:subject)
        ORDER BY position"""
    )
    abstract fun getAllPendingEvaluationsFromSubject(
        subject: String
    ): LiveData<List<Evaluation>>

    /**
     * @param schoolYear to search.
     * @return how many evaluations have this [schoolYear].
     */
    @Query("SELECT COUNT(*) FROM EVALUATIONS WHERE cod_school_year=:schoolYear")
    abstract fun countAllFromSchoolYear(schoolYear: String): LiveData<Int>

    /**
     * @param schoolYear to search.
     * @return if this [schoolYear] is active.
     * @see isSchoolYearActive for get an observable.
     */
    @Query("SELECT active FROM SCHOOL_YEARS WHERE cod=:schoolYear")
    abstract fun isSchoolYearActiveOnly(schoolYear: String): Boolean

    /**
     * @param schoolYear to search.
     * @return an observable value of if this [schoolYear] is active.
     * @see isSchoolYearActiveOnly for get only the value.
     */
    @Query("SELECT active FROM SCHOOL_YEARS WHERE cod=:schoolYear")
    abstract fun isSchoolYearActive(schoolYear: String): LiveData<Boolean>

    /**
     * @param schoolYear to search.
     * @return a list of evaluations of this [schoolYear].
     */
    @Query("SELECT * FROM EVALUATIONS WHERE cod_school_year=:schoolYear ORDER BY position ASC")
    protected abstract fun getListFromSchoolYear(schoolYear: String): List<Evaluation>

    /**
     * @param schoolYear to search.
     * @param position start.
     * @return a list of previous evaluations of [position] from this [schoolYear].
     */
    @Query("SELECT * FROM EVALUATIONS WHERE cod_school_year=:schoolYear AND position < :position")
    protected abstract fun getPreviousEvaluations(
        schoolYear: String,
        position: Int
    ): List<Evaluation>

    /**
     * @param schoolYear to search.
     * @param position start.
     * @return a list of next evaluations of [position] from this [schoolYear].
     */
    @Query("SELECT * FROM EVALUATIONS WHERE cod_school_year=:schoolYear AND position > :position")
    protected abstract fun getNextEvaluations(
        schoolYear: String,
        position: Int
    ): List<Evaluation>

    /**
     * @param cod of school year.
     * @return max code that start with [cod].
     */
    @Query("SELECT MAX(cod) FROM EVALUATIONS WHERE cod like :cod||'%'")
    abstract fun getMaxCod(cod: String): String?

    /**
     * @param cod
     * @return an observable evaluation of this [cod].
     * @see getOnly for get only the evaluation value.
     */
    @Query("SELECT * FROM EVALUATIONS WHERE cod=:cod")
    abstract fun get(cod: String): LiveData<Evaluation>

    /**
     * @param cod
     * @return an evaluation of this [cod].
     * @see get for get an observable evaluation value.
     */
    @Query("SELECT * FROM EVALUATIONS WHERE cod=:cod")
    abstract fun getOnly(cod: String): Evaluation

    /**
     * @param codSchoolYear to search.
     * @param position to retrieve.
     * @return a nullable evaluation from this [codSchoolYear] of this [position].
     */
    @Query("SELECT * FROM EVALUATIONS WHERE cod_school_year=:codSchoolYear AND position=:position")
    abstract fun getFromPositionAndSchoolYear(codSchoolYear: String, position: Int): Evaluation?

    /**
     * Update the state of subSubjects when an evaluation finish.
     *
     * @param codEvaluations for finish subjects.
     */
    @Query("UPDATE SUB_SUBJECTS SET active=0 WHERE cod_evaluation IN (:codEvaluations)")
    protected abstract fun finishSubjects(vararg codEvaluations: String)

    /**
     * Update the state of school year if this [codSchoolYear] don't have more active evaluation.
     *
     * @param codSchoolYear to update.
     * @param finishDate default now.
     */
    @Query("UPDATE SCHOOL_YEARS SET active=0, finish_date=:finishDate WHERE cod =:codSchoolYear")
    protected abstract fun finishSchoolYear(
        codSchoolYear: String,
        finishDate: Calendar = Calendar.getInstance()
    )

    /**
     * Update the state of school year if this [codSchoolYear] have now an active evaluation.
     *
     * @param codSchoolYear to active.
     */
    @Query("UPDATE SCHOOL_YEARS SET active=1, finish_date=null WHERE cod =:codSchoolYear")
    protected abstract fun activeSchoolYear(
        codSchoolYear: String
    )

    /**
     * @param schoolYear to search.
     * @return a list of cod of subject that don't have any subSubject active.
     */
    @Query(
        """SELECT cod FROM SUBJECTS
        WHERE cod_school_year = :schoolYear
        AND cod NOT IN (SELECT DISTINCT cod_subject FROM SUB_SUBJECTS WHERE active = 1)"""
    )
    protected abstract fun getSubjectsInactive(schoolYear: String): List<String>

    /**
     * Active subSubject of this [evaluation] and only of this [subjects] list.
     *
     * @param evaluation to search.
     * @param subjects to active.
     */
    @Query(
        """UPDATE SUB_SUBJECTS SET active=1 
        WHERE cod_evaluation=:evaluation 
        AND cod_subject IN (:subjects)
    """
    )
    protected abstract fun activeSubSubject(evaluation: String, vararg subjects: String)

    /**
     * @param evaluation to add
     */
    @Insert
    abstract fun add(vararg evaluation: Evaluation)

    /**
     * @param evaluation to update
     */
    @Update
    abstract fun update(vararg evaluation: Evaluation)

    /**
     * @param evaluation to delete
     */
    @Delete
    protected abstract fun delete(vararg evaluation: Evaluation)

    /**
     * @param codSchoolYear for generate the new cod.
     * @return new number cod of evaluation.
     */
    open fun getNewNumberEvaluationCod(codSchoolYear: String): Int {
        val evaluationCod = getMaxCod(codSchoolYear)
        return if (evaluationCod != null) {
            Integer.parseInt(evaluationCod.substring(6)) + 1
        } else {
            1
        }
    }

    /**
     * @param codSchoolYear for generate the new cod.
     * @return new valid cod of evaluation.
     */
    open fun getNewEvaluationCod(codSchoolYear: String): String =
        parseIntToCod(getNewNumberEvaluationCod(codSchoolYear), codSchoolYear)

    /**
     * @param number to parse into a valid cod.
     * @param codSchoolYear for parse into a valid cod.
     * @return a valid cod parsed from [number] and [codSchoolYear].
     */
    open fun parseIntToCod(number: Int, codSchoolYear: String): String =
        when (number) {
            in 1..9 -> "${codSchoolYear}0$number"
            else -> "$codSchoolYear$number"
        }

    /**
     * Update the position of two evaluations.
     *
     * @param codSchoolYear of the evaluation.
     * @param fromPosition start.
     * @param toPosition end.
     */
    @Deprecated("In consideration for suppress in order a good logic business")
    open fun move(codSchoolYear: String, fromPosition: Int, toPosition: Int) {
        val ev1 = getFromPositionAndSchoolYear(codSchoolYear, fromPosition)
        val ev2 = getFromPositionAndSchoolYear(codSchoolYear, toPosition)
        ev1!!.position = toPosition
        ev2!!.position = fromPosition
        update(ev1, ev2)
    }

    /**
     * Delete and reorder the evaluations
     *
     * @param evaluation to delete
     */
    @Transaction
    open fun deleteAndReorder(vararg evaluation: Evaluation) {
        delete(*evaluation)
        reorder(evaluation[0].codSchoolYear)
    }

    /**
     * Reorder the evaluations of this [codSchoolYear]
     *
     * @param codSchoolYear for reorder evaluations
     */
    protected open fun reorder(codSchoolYear: String) {
        var evaluations = getListFromSchoolYear(codSchoolYear)
        evaluations = evaluations.sortedByDescending { it.id }
        evaluations = evaluations.sortedBy { it.position }
        for (i in 1..evaluations.size) {
            evaluations[i - 1].position = i
        }
        update(*evaluations.toTypedArray())
    }

    /**
     * Add a new evaluation and reorder evaluations
     *
     * @param evaluation to add
     */
    @Transaction
    open fun addAndReorder(evaluation: Evaluation) {
        add(evaluation)
        reorder(evaluation.codSchoolYear)
    }

    /**
     * Mark a evaluation as finished and make the necessary changes and checks
     *
     * @param evaluation to mark as complete
     */
    @Transaction
    open fun markAsFinished(evaluation: Evaluation) {
        val nextEvaluation =
            getFromPositionAndSchoolYear(evaluation.codSchoolYear, evaluation.position + 1)
        val previousEvaluations =
            getPreviousEvaluations(evaluation.codSchoolYear, evaluation.position)
        val codEvaluations = ArrayList<String>()

        evaluation.finishDate = Calendar.getInstance()
        codEvaluations.add(evaluation.cod)
        for (prevEvaluation in previousEvaluations) {
            if (prevEvaluation.startDate == null)
                prevEvaluation.startDate = Calendar.getInstance()
            if (prevEvaluation.finishDate == null) {
                prevEvaluation.finishDate = Calendar.getInstance()
            }
            codEvaluations.add(prevEvaluation.cod)
        }
        finishSubjects(*codEvaluations.toTypedArray())
        update(evaluation, *previousEvaluations.toTypedArray())
        if (nextEvaluation != null) {
            if (nextEvaluation.startDate == null) {
                nextEvaluation.startDate = Calendar.getInstance()
                update(nextEvaluation)
                activeNextSubSubject(nextEvaluation)
            }
        } else {
            finishSchoolYear(evaluation.codSchoolYear)
        }
    }

    /**
     * Active subSubject of this [nextEvaluation]
     *
     * @param nextEvaluation to search subSubjects and mark as active
     */
    protected open fun activeNextSubSubject(nextEvaluation: Evaluation) {
        val list = getSubjectsInactive(nextEvaluation.codSchoolYear)
        activeSubSubject(nextEvaluation.cod, *list.toTypedArray())
    }

    /**
     * Mark a evaluation as active and make the necessary changes and checks
     *
     * @param evaluation to mark as active
     */
    @Transaction
    open fun markAsIncomplete(evaluation: Evaluation) {
        val nextEvaluations = getNextEvaluations(evaluation.codSchoolYear, evaluation.position)
        evaluation.finishDate = null
        for (nextEvaluation in nextEvaluations)
            nextEvaluation.finishDate = null
        update(evaluation, *nextEvaluations.toTypedArray())
        activeSchoolYear(evaluation.codSchoolYear)
    }
}