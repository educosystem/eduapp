/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.dao

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.*
import com.educosystem.eduapp.data.entity.Element
import com.educosystem.eduapp.data.entity.ElementWithAllInformation
import com.educosystem.eduapp.util.ListElementsTypes
import com.educosystem.eduapp.util.TYPE_EXAM
import com.educosystem.eduapp.util.TYPE_WORK
import java.util.*

@Dao
abstract class ElementDao {
    /**
     * @param cod
     * @return an observable element.
     */
    @Query("SELECT * FROM ELEMENTS WHERE cod=:cod")
    abstract fun get(cod: String): LiveData<Element>

    /**
     * @param cod
     * @return an observable element with all information.
     */
    @Transaction
    @Query("SELECT * FROM ELEMENTS WHERE cod=:cod")
    abstract fun getWithAllInformation(cod: String): LiveData<ElementWithAllInformation>

    /**
     * @param cod to start.
     * @return max cod starting with [cod].
     */
    @Query("SELECT MAX(cod) FROM ELEMENTS WHERE cod like :cod||'%'")
    abstract fun getMaxCod(cod: String): String?

    // All active lists
    /**
     * @param date default now.
     * @return an observable list of active elements from this [date].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            realized=0 AND (delivery_date>:date OR delivery_date IS NULL) 
            AND cod_block IN (
        SELECT b.cod FROM BLOCKS as b 
        LEFT JOIN SUB_SUBJECTS as ssb ON b.cod_sub_subject=ssb.cod 
        LEFT JOIN SUBJECTS as sb ON ssb.cod_subject=sb.cod 
        LEFT JOIN SCHOOL_YEARS as sy ON sb.cod_school_year=sy.cod 
        WHERE ssb.active=1 AND sy.active=1)
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActive(
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    /**
     * @param subject cod for search elements.
     * @param date default now.
     * @return an observable list of active elements from this [date] and [subject].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            realized=0 AND (delivery_date>:date OR delivery_date IS NULL)
            AND cod_block IN (
        SELECT cod FROM BLOCKS WHERE cod_sub_subject IN (
        SELECT cod FROM SUB_SUBJECTS WHERE cod_subject=:subject))
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveFromSubject(
        subject: String,
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    /**
     * @param subSubject cod for search elements.
     * @param date default now.
     * @return an observable list of active elements from this [date] and [subSubject].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            realized=0 AND (delivery_date>:date OR delivery_date IS NULL)
            AND cod_block IN (
        SELECT cod FROM BLOCKS WHERE cod_sub_subject = :subSubject)
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveFromSubSubject(
        subSubject: String,
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    /**
     * @param block cod for search elements.
     * @param date default now.
     * @return an observable list of active elements from this [date] and [block].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE cod_block=:block 
        AND (delivery_date>:date OR delivery_date IS NULL)
        AND realized=0 ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveFromBlock(
        block: String,
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    /**
     * @param subBlock cod for search elements.
     * @param date default now.
     * @return an observable list of active elements from this [date] and [subBlock].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE cod_sub_block=:subBlock 
        AND (delivery_date>:date OR delivery_date IS NULL)
        AND realized=0 ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveFromSubBlock(
        subBlock: String,
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    // All active delayed lists
    /**
     * @param date default now.
     * @return an observable list of active and delayed elements from this [date].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE realized=0 
            AND delivery_date IS NOT NULL AND delivery_date<:date 
            AND cod_block IN (
        SELECT b.cod FROM BLOCKS as b 
        LEFT JOIN SUB_SUBJECTS as ssb ON b.cod_sub_subject=ssb.cod 
        LEFT JOIN SUBJECTS as sb ON ssb.cod_subject=sb.cod 
        LEFT JOIN SCHOOL_YEARS as sy ON sb.cod_school_year=sy.cod 
        WHERE ssb.active=1 AND sy.active=1)
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveDelayed(
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    /**
     * @param subject cod for search elements.
     * @param date default now.
     * @return an observable list of active and delayed elements from this [date] and [subject].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE realized=0 
            AND delivery_date IS NOT NULL AND delivery_date<:date 
            AND cod_block IN (
        SELECT cod FROM BLOCKS WHERE cod_sub_subject IN (
        SELECT cod FROM SUB_SUBJECTS WHERE cod_subject=:subject))
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveDelayedFromSubject(
        subject: String,
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    /**
     * @param subSubject cod for search elements.
     * @param date default now.
     * @return an observable list of active and delayed elements from this [date] and [subSubject].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE realized=0 
            AND delivery_date IS NOT NULL AND delivery_date<:date 
            AND cod_block IN (
        SELECT cod FROM BLOCKS WHERE cod_sub_subject=:subSubject)
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveDelayedFromSubSubject(
        subSubject: String,
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    /**
     * @param block cod for search elements.
     * @param date default now.
     * @return an observable list of active and delayed elements from this [date] and [block].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE realized=0 
            AND delivery_date IS NOT NULL AND delivery_date<:date 
            AND cod_block =:block
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveDelayedFromBlock(
        block: String,
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    /**
     * @param subBlock cod for search elements.
     * @param date default now.
     * @return an observable list of active and delayed elements from this [date] and [subBlock].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE realized=0 
            AND delivery_date IS NOT NULL AND delivery_date<:date 
            AND cod_sub_block =:subBlock
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllActiveDelayedFromSubBlock(
        subBlock: String,
        date: Calendar = Calendar.getInstance()
    ): LiveData<List<Element>>

    // All finished lists

    /**
     * @param subject cod for search elements.
     * @return an observable list of finished elements from [subject].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            realized=1 AND cod_block IN (
        SELECT cod FROM BLOCKS WHERE cod_sub_subject IN (
        SELECT cod FROM SUB_SUBJECTS WHERE cod_subject=:subject))
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllCompletedFromSubject(
        subject: String
    ): LiveData<List<Element>>

    /**
     * @param subSubject cod for search elements.
     * @return an observable list of finished elements from [subSubject].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            realized=1 AND cod_block IN (
        SELECT cod FROM BLOCKS WHERE cod_sub_subject=:subSubject)
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllCompletedFromSubSubject(
        subSubject: String
    ): LiveData<List<Element>>

    /**
     * @param block cod for search elements.
     * @return an observable list of finished elements from [block].
     */
    @Query("SELECT * FROM ELEMENTS WHERE cod_block=:block AND realized=1 ORDER BY delivery_date DESC")
    abstract fun getAllCompletedFromBlock(block: String): LiveData<List<Element>>

    /**
     * @param subBlock cod for search elements
     * @return an observable list of finished elements from [subBlock].
     */
    @Query("SELECT * FROM ELEMENTS WHERE cod_sub_block=:subBlock AND realized=1 ORDER BY delivery_date DESC")
    abstract fun getAllCompletedFromSubBlock(subBlock: String): LiveData<List<Element>>

    // All without grade lists
    /**
     * @param type of the elements to search.
     * @return an observable list of elements without grade of this [type].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            grade IS NULL AND type in (:type)
            AND realized = 1
            AND cod_block IN (
        SELECT b.cod FROM BLOCKS as b 
        LEFT JOIN SUB_SUBJECTS as ssb ON b.cod_sub_subject=ssb.cod 
        LEFT JOIN SUBJECTS as sb ON ssb.cod_subject=sb.cod 
        LEFT JOIN SCHOOL_YEARS as sy ON sb.cod_school_year=sy.cod 
        WHERE sy.active=1)
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllWithoutGrade(vararg type: Int): LiveData<List<Element>>

    /**
     * @param subject cod for search elements.
     * @param type of the elements to search.
     * @return an observable list of elements without grade from [subject] and this [type].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            grade IS NULL AND type in (:type)
            AND realized = 1
            AND cod_block IN (
        SELECT cod FROM BLOCKS WHERE cod_sub_subject IN (
        SELECT cod FROM SUB_SUBJECTS WHERE cod_subject=:subject))
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllWithoutGradeFromSubject(
        subject: String,
        vararg type: Int
    ): LiveData<List<Element>>

    /**
     * @param subSubject cod for search elements.
     * @param type of the elements to search.
     * @return an observable list of elements without grade from [subSubject] and this [type].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            grade IS NULL AND type in (:type)
            AND realized = 1
            AND cod_block IN (
        SELECT cod FROM BLOCKS WHERE cod_sub_subject =:subSubject)
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllWithoutGradeFromSubSubject(
        subSubject: String,
        vararg type: Int
    ): LiveData<List<Element>>

    /**
     * @param block cod for search elements.
     * @param type of the elements to search.
     * @return an observable list of elements without grade from [block] and this [type].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            grade IS NULL AND type in (:type)
            AND realized = 1
            AND cod_block=:block
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllWithoutGradeFromBlock(
        block: String,
        vararg type: Int
    ): LiveData<List<Element>>

    /**
     * @param subBlock cod for search elements.
     * @param type of the elements to search.
     * @return an observable list of elements without grade from [subBlock] and this [type].
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE 
            grade IS NULL AND type in (:type)
            AND realized = 1
            AND cod_sub_block=:subBlock
        ORDER BY delivery_date DESC"""
    )
    abstract fun getAllWithoutGradeFromSubBlock(
        subBlock: String,
        vararg type: Int
    ): LiveData<List<Element>>

    // Average methods.
    /**
     * @param block of the elements to calculate the average.
     * @return average of this [block].
     */
    @Query("SELECT AVG(grade*multiplier) FROM ELEMENTS WHERE cod_block=:block")
    abstract fun getAverage(block: String): Double?

    /**
     * @param subBlock of the elements to calculate the average.
     * @return average of this [subBlock].
     */
    @Query("SELECT AVG(grade*multiplier) FROM ELEMENTS WHERE cod_sub_block=:subBlock")
    abstract fun getAverageSubBlock(subBlock: String): Double?

    /**
     * @param block of the elements to calculate the how much are completed and calculate
     * the average.
     * @return average of this [block].
     */
    @Query(
        """SELECT (
        SELECT COUNT(*) FROM ELEMENTS WHERE cod_block=:block 
        AND realized=1 AND (delivery_date IS NULL OR delivery_date>delivered_date))
        *10/COUNT(*) FROM ELEMENTS WHERE cod_block=:block"""
    )
    abstract fun getAverageTask(block: String): Double?

    /**
     * @param subBlock of the elements to calculate the how much are completed and calculate
     * the average.
     * @return average of this [subBlock].
     */
    @Query(
        """SELECT (
        SELECT COUNT(*) FROM ELEMENTS WHERE cod_sub_block=:subBlock 
        AND realized=1 AND (delivery_date IS NULL OR delivery_date>delivered_date))
        *10/COUNT(*) FROM ELEMENTS WHERE cod_sub_block=:subBlock"""
    )
    abstract fun getAverageTaskSubBlock(subBlock: String): Double?

    // Service methods

    /**
     * @param date default now
     * @param typeList types of the elements to search, default only [TYPE_EXAM]
     * @return the elements that are active and delayed of this [typeList] from this [date]
     */
    @Query(
        """SELECT * FROM ELEMENTS WHERE realized=0 
            AND delivery_date IS NOT NULL AND delivery_date<:date 
            AND type IN (:typeList)
            AND cod_block IN (
        SELECT b.cod FROM BLOCKS as b 
        LEFT JOIN SUB_SUBJECTS as ssb ON b.cod_sub_subject=ssb.cod 
        LEFT JOIN SUBJECTS as sb ON ssb.cod_subject=sb.cod 
        LEFT JOIN SCHOOL_YEARS as sy ON sb.cod_school_year=sy.cod 
        WHERE ssb.active=1 AND sy.active=1)
        ORDER BY delivery_date DESC"""
    )
    protected abstract fun getExamsActiveDelayed(
        date: Calendar = Calendar.getInstance(),
        typeList: List<Int> = listOf(TYPE_EXAM)
    ): List<Element>

    // Generic Methods

    /**
     * @param element to add
     */
    @Insert
    abstract fun add(vararg element: Element)

    /**
     * @param element to update
     */
    @Update
    abstract fun update(vararg element: Element)

    /**
     * @param element to delete
     */
    @Delete
    abstract fun delete(vararg element: Element)

    /**
     * Generic method to obtain an observable list of elements depending of the [typeList] and
     * the optional values provided.
     *
     * @param typeList of the elements list.
     * @param codSubject to search.
     * @param codSubSubject to search.
     * @param codBlock to search.
     * @param codSubBlock to search.
     * @return an observable list of elements depending of the [typeList] and
     * the optional values provided.
     */
    open fun getAll(
        typeList: ListElementsTypes,
        codSubject: String?,
        codSubSubject: String?,
        codBlock: String?,
        codSubBlock: String?
    ): LiveData<List<Element>> {
        return when (typeList) {
            ListElementsTypes.ACTIVE -> when {
                codSubject != null -> getAllActiveFromSubject(codSubject)
                codSubSubject != null -> getAllActiveFromSubSubject(codSubSubject)
                codBlock != null -> getAllActiveFromBlock(codBlock)
                codSubBlock != null -> getAllActiveFromSubBlock(codSubBlock)
                else -> getAllActive()
            }
            ListElementsTypes.PENDING -> when {
                codSubject != null -> getAllActiveDelayedFromSubject(codSubject)
                codSubSubject != null -> getAllActiveDelayedFromSubSubject(codSubSubject)
                codBlock != null -> getAllActiveDelayedFromBlock(codBlock)
                codSubBlock != null -> getAllActiveDelayedFromSubBlock(codSubBlock)
                else -> getAllActiveDelayed()
            }
            ListElementsTypes.PENDING_GRADES -> when {
                codSubject != null -> getAllWithoutGradeFromSubject(
                    codSubject,
                    TYPE_EXAM,
                    TYPE_WORK
                )
                codSubSubject != null -> getAllWithoutGradeFromSubSubject(
                    codSubSubject, TYPE_EXAM,
                    TYPE_WORK
                )
                codBlock != null -> getAllWithoutGradeFromBlock(codBlock, TYPE_EXAM, TYPE_WORK)
                codSubBlock != null -> getAllWithoutGradeFromSubBlock(
                    codSubBlock, TYPE_EXAM,
                    TYPE_WORK
                )
                else -> getAllWithoutGrade(TYPE_EXAM, TYPE_WORK)
            }
            ListElementsTypes.COMPLETED -> when {
                codSubject != null -> getAllCompletedFromSubject(codSubject)
                codSubSubject != null -> getAllCompletedFromSubSubject(codSubSubject)
                codBlock != null -> getAllCompletedFromBlock(codBlock)
                codSubBlock != null -> getAllCompletedFromSubBlock(codSubBlock)
                else -> MutableLiveData()
            }
        }
    }

    /**
     * @param codBlock for generate the new cod.
     * @return new number cod of element.
     */
    open fun getNewNumberElementCod(codBlock: String): Int {
        val elementCod = getMaxCod(codBlock)
        return if (elementCod != null) {
            Integer.parseInt(elementCod.substring(5)) + 1
        } else {
            1
        }
    }

    /**
     * @param codBlock for generate the new cod.
     * @return new valid cod of element.
     */
    open fun getNewElementCod(codBlock: String): String =
        parseIntToCod(getNewNumberElementCod(codBlock), codBlock)

    /**
     * @param number to parse into a valid cod.
     * @param codBlock for parse into a valid cod.
     * @return a valid cod parsed from [number] and [codBlock].
     */
    open fun parseIntToCod(number: Int, codBlock: String): String =
        when (number) {
            in 1..9 -> "${codBlock}00$number"
            in 10..99 -> "${codBlock}0$number"
            else -> "$codBlock$number"
        }

    /**
     * Service that update the delivered date of elements of type [TYPE_EXAM]
     * and make that as complete before they will transform into a delayed item.
     */
    @Transaction
    open fun updateService() {
        val elements = getExamsActiveDelayed()
        for (element in elements) {
            element.realized = true
            element.deliveryDate = element.deliveredDate
        }
        if (elements.isNotEmpty())
            update(*elements.toTypedArray())
    }
}