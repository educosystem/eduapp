/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.educosystem.eduapp.data.entity.SubBlock

@Dao
abstract class SubBlockDao {
    /**
     * @param cod of the subBlock.
     * @return an observable subBlock.
     * @see getOnly for get only the value.
     */
    @Query("SELECT * FROM SUB_BLOCKS WHERE cod=:cod")
    abstract fun get(cod: String): LiveData<SubBlock>

    /**
     * @param cod of the block.
     * @return a nullable block.
     * @see get for get an observable.
     */
    @Query("SELECT * FROM SUB_BLOCKS WHERE cod=:cod")
    abstract fun getOnly(cod: String): SubBlock

    /**
     * @param block cod.
     * @return an observable list of subBlocks.
     */
    @Query("SELECT * FROM SUB_BLOCKS WHERE cod_block=:block ORDER BY name")
    abstract fun getAllFromBlock(block: String): LiveData<List<SubBlock>>

    /**
     * @param block cod.
     * @return a list of subBlocks.
     */
    @Query("SELECT * FROM SUB_BLOCKS WHERE cod_block=:block ORDER BY name")
    abstract fun getAllFromBlockList(block: String): List<SubBlock>

    /**
     * @param cod of block.
     * @return max code that start with [cod].
     */
    @Query("SELECT MAX(cod) FROM SUB_BLOCKS WHERE cod like :cod||'%'")
    abstract fun getMaxCod(cod: String): String?

    /**
     * @param subBlock to add.
     */
    @Insert
    abstract fun add(vararg subBlock: SubBlock)

    /**
     * @param subBlock to update.
     */
    @Update
    abstract fun update(vararg subBlock: SubBlock)

    /**
     * @param subBlock to delete.
     */
    @Delete
    abstract fun delete(vararg subBlock: SubBlock)

    /**
     * @param codBlock for generate the new cod.
     * @return new number cod of subBlock.
     */
    open fun getNewNumberSubBlockCod(codBlock: String): Int {
        val subjectCod = getMaxCod(codBlock)
        return if (subjectCod != null) {
            Integer.parseInt(subjectCod.substring(5)) + 1
        } else {
            1
        }
    }

    /**
     * @param codBlock for generate the new cod.
     * @return new valid cod of subBlock.
     */
    open fun getNewSubBlockCod(codBlock: String): String =
        parseIntToCod(getNewNumberSubBlockCod(codBlock), codBlock)


    /**
     * @param number to parse into a valid cod.
     * @param codBlock for parse into a valid cod.
     * @return a valid cod parsed from [number] and [codBlock].
     */
    open fun parseIntToCod(number: Int, codBlock: String): String =
        when (number) {
            in 1..9 -> "${codBlock}00$number"
            in 10..99 -> "${codBlock}0$number"
            else -> "$codBlock$number"
        }
}