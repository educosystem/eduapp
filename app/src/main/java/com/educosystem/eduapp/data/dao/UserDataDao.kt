/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.educosystem.eduapp.data.entity.UserData
import com.educosystem.eduapp.data.entity.UserWithMessages

@Dao
interface UserDataDao {
    /**
     * @param cod of the user.
     * @return an observable userData value.
     */
    @Query("SELECT * FROM USERS_DATA WHERE cod=:cod")
    fun get(cod: String): LiveData<UserData>

    /**
     * @return max cod of users.
     */
    @Query("SELECT MAX(cod) FROM USERS_DATA")
    fun getMaxCod(): String?

    /**
     * @param cod of the user.
     * @return a user with messages from [cod].
     */
    @Transaction
    @Query("SELECT * FROM USERS_DATA WHERE cod=:cod")
    fun getUserWithMessages(cod: String): UserWithMessages

    /**
     * @param codCredential
     * @return a nullable user with messages from this [codCredential].
     */
    @Transaction
    @Query("SELECT * FROM USERS_DATA WHERE cod_credential=:codCredential")
    fun getUsersWithMessages(codCredential: String): List<UserWithMessages>?

    /**
     * @param userData to add
     */
    @Insert
    fun add(userData: UserData)

    /**
     * @param userData to update
     */
    @Update
    fun update(userData: UserData)
}