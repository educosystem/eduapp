/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.educosystem.eduapp.data.entity.Subject
import com.educosystem.eduapp.data.entity.SubjectWithSettings
import com.educosystem.eduapp.data.entity.SubjectWithSubSubject
import com.educosystem.eduapp.util.TYPE_EXAM
import com.educosystem.eduapp.util.TYPE_HOMEWORK
import com.educosystem.eduapp.util.TYPE_WORK

@Dao
abstract class SubjectDao {
    /**
     * @param schoolYear cod.
     * @return an observable list of subjects.
     */
    @Query("SELECT * FROM SUBJECTS WHERE cod_school_year=:schoolYear ORDER BY name")
    abstract fun getAllFromSchoolYear(schoolYear: String): LiveData<List<Subject>>

    /**
     * @param evaluation cod.
     * @return an observable list of subjects.
     */
    @Query(
        """SELECT * FROM SUBJECTS 
        WHERE cod IN (SELECT cod_subject FROM SUB_SUBJECTS WHERE cod_evaluation=:evaluation) 
        ORDER BY name"""
    )
    abstract fun getAllFromEvaluation(evaluation: String): LiveData<List<Subject>>

    /**
     * @param evaluation cod.
     * @return an observable list of active subjects.
     */
    @Query(
        """SELECT * FROM SUBJECTS 
        WHERE cod IN (SELECT cod_subject FROM SUB_SUBJECTS 
        WHERE cod_evaluation=:evaluation
        AND active = 1
        ) 
        ORDER BY name"""
    )
    abstract fun getAllActiveItemsFromEvaluation(evaluation: String): LiveData<List<Subject>>

    /**
     * @param evaluation cod.
     * @return an observable list of subjects that don't have any subSubject in this [evaluation].
     */
    @Query(
        """SELECT * FROM SUBJECTS 
        WHERE cod_school_year= (SELECT cod_school_year FROM EVALUATIONS WHERE cod=:evaluation)
        AND cod NOT IN (SELECT cod_subject FROM SUB_SUBJECTS WHERE cod_evaluation=:evaluation)
        ORDER BY name"""
    )
    abstract fun getAllPendingSubjectsFromEvaluation(
        evaluation: String
    ): LiveData<List<Subject>>

    /**
     * @param types of the valid blocks or subBlocks.
     * @return an observable list of subjects that can add elements because
     * they have an active subSubject with a valid structure.
     */
    @Query(
        """SELECT * FROM SUBJECTS
             WHERE cod IN (SELECT DISTINCT s.cod_subject FROM SUB_SUBJECTS as s INNER JOIN BLOCKS as b
             ON s.cod = b.cod_sub_subject
             WHERE s.active = 1
             AND (type IS NOT NULL AND type IN (:types)
             OR b.cod IN (SELECT cod_block FROM SUB_BLOCKS WHERE type IS NOT NULL AND type IN (:types))))"""
    )
    abstract fun getAllValidForAddElements(
        types: List<Int> = listOf(TYPE_EXAM, TYPE_WORK, TYPE_HOMEWORK)
    ): LiveData<List<Subject>>

    /**
     * @param types of the valid blocks or subBlocks.
     * @return exist any subject that can add elements.
     */
    @Query(
        """SELECT COUNT(*)>0 FROM SUBJECTS
             WHERE cod IN (SELECT DISTINCT s.cod_subject FROM SUB_SUBJECTS as s INNER JOIN BLOCKS as b
             ON s.cod = b.cod_sub_subject
             WHERE s.active = 1
             AND (type IS NOT NULL AND type IN (:types)
             OR b.cod IN (SELECT cod_block FROM SUB_BLOCKS WHERE type IS NOT NULL AND type IN (:types))))"""
    )
    abstract fun getAnySubjectCanAddElements(
        types: List<Int> = listOf(TYPE_EXAM, TYPE_WORK, TYPE_HOMEWORK)
    ): LiveData<Boolean>

    /**
     * @param cod of subject.
     * @return an observable subject.
     * @see getOnly for get only the value.
     */
    @Query("SELECT * FROM SUBJECTS WHERE cod=:cod")
    abstract fun get(cod: String): LiveData<Subject>

    /**
     * @param cod of subject.
     * @return a nullable subject.
     * @see get for get an observable.
     */
    @Query("SELECT * FROM SUBJECTS WHERE cod=:cod")
    abstract fun getOnly(cod: String): Subject?

    /**
     * @param codElement
     * @return the subject that correspond for this [codElement].
     */
    @Query(
        """ SELECT * FROM SUBJECTS
            WHERE cod = (SELECT cod_subject FROM SUB_SUBJECTS as s INNER JOIN BLOCKS as b
            ON s.cod = b.cod_sub_subject
            INNER JOIN ELEMENTS as e
            ON b.cod = e.cod_block
            WHERE e.cod=:codElement)"""
    )
    abstract fun getFromElement(codElement: String): LiveData<Subject>

    /**
     * @param codElement
     * @return the type colour of subject that correspond for this [codElement].
     */
    @Query(
        """ SELECT colour FROM SUBJECTS
            WHERE cod = (SELECT cod_subject FROM SUB_SUBJECTS as s INNER JOIN BLOCKS as b
            ON s.cod = b.cod_sub_subject
            INNER JOIN ELEMENTS as e
            ON b.cod = e.cod_block
            WHERE e.cod=:codElement)"""
    )
    abstract fun getTypeColourFromElement(codElement: String): Int

    /**
     * @param codSubject
     * @param types of the valid blocks or subBlocks.
     * @return a list of codes of subSubjects that can add elements.
     */
    @Query(
        """SELECT cod_sub_subject FROM BLOCKS 
            WHERE cod_sub_subject IN (SELECT cod FROM SUB_SUBJECTS 
            WHERE cod_subject=:codSubject 
            AND active = 1 )
            AND (type IS NOT NULL AND type IN (:types)
            OR cod IN (SELECT cod_block FROM SUB_BLOCKS WHERE type IS NOT NULL AND type IN (:types)))
            LIMIT 1"""
    )
    abstract fun subjectCanAddElements(
        codSubject: String,
        types: List<Int> = listOf(TYPE_EXAM, TYPE_WORK, TYPE_HOMEWORK)
    ): LiveData<String?>

    /**
     * @param cod of subject.
     * @return an observable subject with settings.
     */
    @Transaction
    @Query("SELECT * FROM SUBJECTS WHERE cod=:cod")
    abstract fun getWithSettings(cod: String): LiveData<SubjectWithSettings>

    /**
     * @param cod of subject.
     * @return an observable subject with subSubjects.
     */
    @Transaction
    @Query("SELECT * FROM SUBJECTS WHERE cod=:cod")
    abstract fun getWithSubSubject(cod: String): LiveData<SubjectWithSubSubject>

    /**
     * @param cod of school year.
     * @return max code that start with [cod].
     */
    @Query("SELECT MAX(cod) FROM SUBJECTS WHERE cod like :cod||'%'")
    abstract fun getMaxCod(cod: String): String?

    /**
     * @param subject to add.
     */
    @Insert
    abstract fun add(vararg subject: Subject)

    /**
     * @param subject to update.
     */
    @Update
    abstract fun update(vararg subject: Subject)

    /**
     * @param subject to delete.
     */
    @Delete
    abstract fun delete(vararg subject: Subject)

    /**
     * @param codSchoolYear for generate the new cod.
     * @return new number cod of subject.
     */
    open fun getNewNumberSubjectCod(codSchoolYear: String): Int {
        val subjectCod = getMaxCod(codSchoolYear)
        return if (subjectCod != null) {
            Integer.parseInt(subjectCod.substring(6)) + 1
        } else {
            1
        }
    }

    /**
     * @param codSchoolYear for generate the new cod.
     * @return new valid cod of subject.
     */
    open fun getNewSubjectCod(codSchoolYear: String): String {
        return when (val numberCodSubject = getNewNumberSubjectCod(codSchoolYear)) {
            in 1..9 -> "${codSchoolYear}0$numberCodSubject"
            else -> "$codSchoolYear$numberCodSubject"
        }
    }
}