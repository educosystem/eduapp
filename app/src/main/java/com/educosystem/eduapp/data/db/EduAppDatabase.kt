/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.educosystem.eduapp.data.dao.*
import com.educosystem.eduapp.data.entity.*

@Database(
    entities = [
        Block::class,
        Credential::class,
        Element::class,
        Evaluation::class,
        FileElement::class,
        Message::class,
        Reminder::class,
        ReminderElement::class,
        Requirement::class,
        SchoolYear::class,
        SubBlock::class,
        Subject::class,
        SubjectSetting::class,
        SubSubject::class,
        UserData::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(DateConverter::class)
abstract class EduAppDatabase : RoomDatabase() {
    abstract val blockDao: BlockDao
    abstract val credentialDao: CredentialDao
    abstract val elementDao: ElementDao
    abstract val evaluationDao: EvaluationDao
    abstract val fileElementDao: FileElementDao
    abstract val messageDao: MessageDao
    abstract val reminderDao: ReminderDao
    abstract val requirementDao: RequirementDao
    abstract val schoolYearDao: SchoolYearDao
    abstract val subBlockDao: SubBlockDao
    abstract val subjectDao: SubjectDao
    abstract val subjectSettingDao: SubjectSettingDao
    abstract val subSubjectDao: SubSubjectDao
    abstract val userDataDao: UserDataDao

    companion object {

        @Volatile
        private var INSTANCE: EduAppDatabase? = null

        /**
         * @param context
         * @return an instance of EduAppDatabase with all DAOs.
         */
        fun getInstance(context: Context): EduAppDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        EduAppDatabase::class.java,
                        "edu_app_database"
                    )
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}