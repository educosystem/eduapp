/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.entity

import androidx.annotation.NonNull
import androidx.room.*

@Entity(
    tableName = "BLOCKS",
    indices = [
        Index("cod", unique = true),
        Index("cod_sub_subject"),
        Index("cod_repeat_block")
    ],
    foreignKeys = [
        ForeignKey(
            entity = SubSubject::class,
            parentColumns = ["cod"],
            childColumns = ["cod_sub_subject"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Block::class,
            parentColumns = ["cod"],
            childColumns = ["cod_repeat_block"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.SET_NULL
        )
    ]
)
data class Block(
    @NonNull var cod: String,
    @NonNull @ColumnInfo(name = "cod_sub_subject") var codSubSubject: String,
    @NonNull var name: String,
    @PrimaryKey(autoGenerate = true) var id: Long = 0L,
    var percent: Int? = null,
    var type: Int? = null,
    var grade: Double? = null,
    @ColumnInfo(name = "cod_synchronized_block") var codSynchronizedBlock: String? = null,
    @ColumnInfo(name = "cod_repeat_block") var codRepeatBlock: String? = null,
    @NonNull @ColumnInfo(name = "mode_repeat") var modeRepeat: Boolean = false,
    @ColumnInfo(name = "grade_comment") var gradeComment: String? = null
) {
    override fun toString(): String {
        return name
    }
}