/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.educosystem.eduapp.data.entity.Evaluation
import com.educosystem.eduapp.data.entity.SchoolYear
import com.educosystem.eduapp.data.entity.SchoolYearWithEvaluations
import com.educosystem.eduapp.data.entity.SchoolYearWithSubjects
import java.util.*
import kotlin.collections.ArrayList

@Dao
abstract class SchoolYearDao {
    /**
     * @param cod of the schoolYear.
     * @return an observable schoolYear.
     */
    @Query("SELECT * FROM SCHOOL_YEARS WHERE cod=:cod")
    abstract fun get(cod: String): LiveData<SchoolYear>

    /**
     * @param cod of the schoolYear.
     * @return an observable schoolYear with their subjects.
     */
    @Transaction
    @Query("SELECT * FROM SCHOOL_YEARS WHERE cod=:cod")
    abstract fun getWithSubjects(cod: String): LiveData<SchoolYearWithSubjects>

    /**
     * @param cod of the schoolYear.
     * @return an observable schoolYear with their evaluations.
     */
    @Transaction
    @Query("SELECT * FROM SCHOOL_YEARS WHERE cod=:cod")
    abstract fun getWithEvaluations(cod: String): LiveData<SchoolYearWithEvaluations>

    /**
     * @return an observable list of schoolYears.
     */
    @Query("SELECT * FROM SCHOOL_YEARS")
    abstract fun getAll(): LiveData<List<SchoolYear>>

    /**
     * @param active if schoolYear is active.
     * @return an observable list of schoolYears.
     */
    @Query("SELECT * FROM SCHOOL_YEARS WHERE active=:active")
    abstract fun getActive(active: Boolean = true): LiveData<List<SchoolYear>>

    /**
     * @param active if schoolYear is active.
     * @return an observable list of schoolYears with their subjects.
     */
    @Transaction
    @Query("SELECT * FROM SCHOOL_YEARS WHERE active=:active")
    abstract fun getActiveWithSubjects(active: Boolean = true): LiveData<List<SchoolYearWithSubjects>>

    /**
     * @param year three last digits for make a valid cod.
     * @return max cod with start with [year].
     */
    @Query("SELECT MAX(cod) FROM SCHOOL_YEARS WHERE cod like :year||'%'")
    abstract fun getMaxCod(year: String): String?

    /**
     * @param schoolYear to add.
     */
    @Insert
    abstract fun add(schoolYear: SchoolYear)

    /**
     * @param evaluation to add.
     */
    @Insert
    abstract fun addEvaluations(vararg evaluation: Evaluation)

    /**
     * @param schoolYear to update.
     */
    @Update
    abstract fun update(vararg schoolYear: SchoolYear)

    /**
     * @param evaluation to update.
     */
    @Update
    protected abstract fun updateEvaluations(vararg evaluation: Evaluation)

    /**
     * @param schoolYear to delete.
     */
    @Delete
    abstract fun delete(vararg schoolYear: SchoolYear)

    /**
     * Finish the subjects of this evaluations.
     *
     * @param codEvaluations of this school year.
     */
    @Query("UPDATE SUB_SUBJECTS SET active=0 WHERE cod_evaluation IN (:codEvaluations)")
    protected abstract fun finishSubjects(vararg codEvaluations: String)

    /**
     * @param codSchoolYear to search evaluations.
     * @return a list of evaluations of this [codSchoolYear].
     */
    @Query("SELECT * FROM EVALUATIONS WHERE cod_school_year IN (:codSchoolYear)")
    protected abstract fun obtainEvaluationsCodeFromSchoolYear(vararg codSchoolYear: String): List<Evaluation>

    /**
     * Add a schoolYear with their evaluations.
     *
     * @param schoolYear to add.
     * @param evaluations list of evaluations.
     */
    @Transaction
    open fun addWithEvaluations(schoolYear: SchoolYear, evaluations: List<Evaluation>) {
        add(schoolYear)
        addEvaluations(*evaluations.toTypedArray())
    }

    /**
     * Mark this school year as completed and
     * make the necessary changes to evaluations and subjects.
     *
     * @param updatedSchoolYear to finish.
     */
    @Transaction
    open fun finishSchoolYear(vararg updatedSchoolYear: SchoolYear) {
        val codSchoolYears = ArrayList<String>()
        val codEvaluations = ArrayList<String>()
        for (schoolYear: SchoolYear in updatedSchoolYear) {
            schoolYear.active = false
            schoolYear.finishDate = Calendar.getInstance()
            codSchoolYears.add(schoolYear.cod)
        }
        val evaluations = obtainEvaluationsCodeFromSchoolYear(*codSchoolYears.toTypedArray())
        if (evaluations.isNotEmpty()) {
            for (evaluation in evaluations) {
                if (evaluation.startDate == null) {
                    evaluation.startDate = Calendar.getInstance()
                }
                if (evaluation.finishDate == null) {
                    evaluation.finishDate = Calendar.getInstance()
                }
                codEvaluations.add(evaluation.cod)
            }
            finishSubjects(*codEvaluations.toTypedArray())
            updateEvaluations(*evaluations.toTypedArray())
        }
        update(*updatedSchoolYear)
    }

    /**
     * Mark this school year as active.
     *
     * @param updatedSchoolYear to activate.
     */
    @Transaction
    open fun markAsIncompleteSchoolYear(vararg updatedSchoolYear: SchoolYear) {
        val codSchoolYears = ArrayList<String>()
        for (schoolYear: SchoolYear in updatedSchoolYear) {
            schoolYear.finishDate = null
            schoolYear.active = true
            codSchoolYears.add(schoolYear.cod)
        }
        update(*updatedSchoolYear)
    }

    /**
     * @return new valid cod for schoolYear.
     */
    open fun getNewSchoolYearCod(): String {
        val calendar = Calendar.getInstance()
        val cod = getMaxCod(calendar.get(Calendar.YEAR).toString().substring(1))
        var newCod = calendar.get(Calendar.YEAR).toString().substring(1)
        newCod += if (cod != null) {
            when (val number = Integer.parseInt(cod.substring(3)) + 1) {
                in 1..9 -> "00$number"
                in 10..99 -> "0$number"
                else -> "$number"
            }
        } else {
            "001"
        }
        return newCod
    }
}