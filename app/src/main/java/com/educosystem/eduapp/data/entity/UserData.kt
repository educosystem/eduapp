/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.data.entity

import androidx.annotation.NonNull
import androidx.room.*

@Entity(
    tableName = "USERS_DATA",
    indices = [
        Index("cod", unique = true),
        Index("cod_credential")
    ],
    foreignKeys = [
        ForeignKey(
            entity = Credential::class,
            parentColumns = ["cod"],
            childColumns = ["cod_credential"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class UserData(
    @NonNull var cod: String,
    @NonNull @ColumnInfo(name = "cod_credential") var codCredential: String,
    @NonNull var username: String,
    @NonNull @ColumnInfo(name = "first_name") var firstName: String,
    @NonNull var surname1: String? = null,
    var surname2: String,
    @ColumnInfo(name = "url_profile_photo") var urlProfilePhoto: String? = null,
    @PrimaryKey(autoGenerate = true) var id: Long = 0L
)