/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *  
 */



package com.educosystem.eduapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.educosystem.eduapp.data.entity.BlockWithSubBlocks
import com.educosystem.eduapp.databinding.ListItemBlockForPreviewBinding

/**
 * Adapter for blocks with subBlocks list items
 *
 */
class BlockWithSubBlockAdapter : ListAdapter<BlockWithSubBlocks,
        BlockWithSubBlockAdapter.ViewHolder>(BlockWithSubBlockDiffCallback()) {

    /**
     * Called by RecyclerView to display the data at the specified position.
     *
     * @param holder The ViewHolder which should be updated to represent the contents of the
     *        item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    /**
     * Called when RecyclerView needs a new [ViewHolder] of the given type to represent
     * an item.
     *
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     *               an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    /**
     * Describe an item view, metadata and behaviour about its place within the RecyclerView.
     *
     * @property binding the view which the item will be loaded.
     */
    class ViewHolder private constructor(
        private val binding: ListItemBlockForPreviewBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        /**
         * Attach data to the view, listen for events and define their behaviour.
         *
         * @param item the item will be rendered.
         */
        fun bind(item: BlockWithSubBlocks) {
            binding.block = item.block
            if (item.subBlocks.isNotEmpty()) {
                val adapter = SubBlockAdapter()
                binding.listItemBlockForPreviewRecyclerView.adapter = adapter
                adapter.submitList(item.subBlocks)
                binding.listItemBlockForPreviewRecyclerView.visibility = View.VISIBLE
            } else
                binding.listItemBlockForPreviewRecyclerView.visibility = View.GONE

            binding.executePendingBindings()
        }

        companion object {
            /**
             * Static function for load and create appropriately the [ViewHolder].
             *
             * @param parent The ViewGroup into which the new View will be added after it is bound to
             *               an adapter position.
             * @return a new [ViewHolder].
             */
            fun from(
                parent: ViewGroup
            ): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemBlockForPreviewBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

/**
 * Diff class to compare the difference between two items
 *
 */
class BlockWithSubBlockDiffCallback : DiffUtil.ItemCallback<BlockWithSubBlocks>() {

    /**
     * @param oldItem
     * @param newItem
     * @return if the items are the same
     */
    override fun areItemsTheSame(
        oldItem: BlockWithSubBlocks,
        newItem: BlockWithSubBlocks
    ): Boolean =
        oldItem.block.id == newItem.block.id

    /**
     * @param oldItem
     * @param newItem
     * @return if the content of the items are the same
     */
    override fun areContentsTheSame(
        oldItem: BlockWithSubBlocks,
        newItem: BlockWithSubBlocks
    ): Boolean =
        oldItem == newItem

}