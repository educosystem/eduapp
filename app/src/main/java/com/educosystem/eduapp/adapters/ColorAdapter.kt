/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.adapters

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.educosystem.eduapp.R
import com.educosystem.eduapp.databinding.ListColorItemBinding
import com.educosystem.eduapp.util.Colour
import com.educosystem.eduapp.util.HolderListener
import com.educosystem.eduapp.util.ListenerOfSelectMode

/**
 * Adapter for colour list items.
 *
 * @property clickListener for handle the click actions of items.
 * @property viewLifecycleOwner for observe values.
 */
class ColorAdapter(
    private val clickListener: HolderListener<Colour>,
    private val viewLifecycleOwner: LifecycleOwner
) : ListAdapter<Colour, ColorAdapter.ViewHolder>(ColorDiffCallback()),
    ListenerOfSelectMode<Colour> {
    override val selectedItems = ArrayList<Colour>()
    override val selectChanged = MutableLiveData(false)

    /**
     * Change the selected items.
     * In this case this will be always one item selected.
     *
     * @param items
     */
    override fun changeSelected(items: ArrayList<Colour>) {
        selectedItems.clear()
        selectedItems.addAll(items)
        selectChanged.value = true
    }

    /**
     * Called by RecyclerView to display the data at the specified position.
     *
     * @param holder The ViewHolder which should be updated to represent the contents of the
     *        item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, clickListener, viewLifecycleOwner)
    }

    /**
     * Called when RecyclerView needs a new [ViewHolder] of the given type to represent
     * an item.
     *
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     *               an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent, this)
    }

    /**
     * Describe an item view, metadata and behaviour about its place within the RecyclerView.
     *
     * @property binding the view which the item will be loaded.
     * @property listenerOfSelectMode for selected items state.
     * @property context used for obtain the selected colour.
     */
    class ViewHolder private constructor(
        private val binding: ListColorItemBinding,
        private val listenerOfSelectMode: ListenerOfSelectMode<Colour>,
        private val context: Context
    ) : RecyclerView.ViewHolder(binding.root) {

        /**
         * Attach data to the view, listen for events and define their behaviour.
         *
         * @param item the item will be rendered.
         * @param clickListener for single click and long click actions.
         * @param viewLifecycleOwner for observe events.
         */
        fun bind(
            item: Colour,
            clickListener: HolderListener<Colour>,
            viewLifecycleOwner: LifecycleOwner
        ) {
            val shape = obtainDrawable(item)
            binding.listColorItemContainer.setOnClickListener {
                clickListener.onLongClick(item)
            }
            binding.listColorItemImage.setImageDrawable(shape)
            binding.listColorItemImage.contentDescription = item.description
            binding.listColorItemImageSelected.contentDescription =
                context.getString(R.string.colorSelected, item.description)
            listenerOfSelectMode.selectChanged.observe(viewLifecycleOwner, Observer {
                if (it && listenerOfSelectMode.selectedItems.contains(item)) {
                    binding.listColorItemImageSelected.visibility = View.VISIBLE
                } else {
                    binding.listColorItemImageSelected.visibility = View.GONE
                }
            })
            binding.listColorItemImageSelected.visibility =
                if (listenerOfSelectMode.selectedItems.contains(item))
                    View.VISIBLE
                else
                    View.GONE
            binding.executePendingBindings()
        }

        /**
         * Create the drawable for the colour item programmatically
         *
         * @param colour of the item
         * @return circular drawable filled with the colour of the item
         */
        private fun obtainDrawable(colour: Colour): GradientDrawable {
            val shape = GradientDrawable()
            shape.shape = GradientDrawable.OVAL
            shape.cornerRadii = floatArrayOf(0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f)
            shape.setColor(colour.color)
            shape.setStroke(2, ContextCompat.getColor(context, R.color.gray))
            return shape
        }

        companion object {
            /**
             * Static function for load and create appropriately the [ViewHolder].
             *
             * @param parent The ViewGroup into which the new View will be added after it is bound to
             *               an adapter position.
             * @param listenerOfSelectMode for observe selected items changes.
             * @return a new [ViewHolder].
             */
            fun from(
                parent: ViewGroup,
                listenerOfSelectMode: ListenerOfSelectMode<Colour>
            ): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding =
                    ListColorItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding, listenerOfSelectMode, parent.context)
            }
        }
    }
}

/**
 * Diff class to compare the difference between two items
 *
 */
class ColorDiffCallback : DiffUtil.ItemCallback<Colour>() {
    /**
     * @param oldItem
     * @param newItem
     * @return if the items are the same
     */
    override fun areItemsTheSame(oldItem: Colour, newItem: Colour): Boolean =
        oldItem.color == newItem.color

    /**
     * @param oldItem
     * @param newItem
     * @return if the content of the items are the same
     */
    override fun areContentsTheSame(oldItem: Colour, newItem: Colour): Boolean =
        oldItem == newItem

}
