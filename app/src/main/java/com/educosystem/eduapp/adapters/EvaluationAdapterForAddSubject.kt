/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.educosystem.eduapp.data.entity.Evaluation
import com.educosystem.eduapp.databinding.ListItemEvaluationForAddSubjectBinding
import com.educosystem.eduapp.util.HolderListener
import com.educosystem.eduapp.util.ListenerOfSelectMode

/**
 * Adapter for list evaluation items in the [com.educosystem.eduapp.ui.AddSubjectFragment].
 *
 * @property clickListener for handle the click actions of items.
 * @property viewLifecycleOwner for observe values.
 */
class EvaluationAdapterForAddSubject(
    private val clickListener: HolderListener<Evaluation>,
    private val viewLifecycleOwner: LifecycleOwner
) : ListAdapter<Evaluation, EvaluationAdapterForAddSubject.ViewHolder>(EvaluationDiffCallback()),
    ListenerOfSelectMode<Evaluation> {
    override val selectedItems = ArrayList<Evaluation>()
    override val selectChanged = MutableLiveData(false)

    /**
     * Change the selected items.
     * In this case this will be always one item selected.
     *
     * @param items
     */
    override fun changeSelected(items: ArrayList<Evaluation>) {
        selectChanged.value = true
        selectedItems.clear()
        selectedItems.addAll(items)
    }

    /**
     * Called by RecyclerView to display the data at the specified position.
     *
     * @param holder The ViewHolder which should be updated to represent the contents of the
     *        item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, clickListener, viewLifecycleOwner)
    }

    /**
     * Called when RecyclerView needs a new [ViewHolder] of the given type to represent
     * an item.
     *
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     *               an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent, this)
    }

    /**
     * Describe an item view, metadata and behaviour about its place within the RecyclerView.
     *
     * @property binding the view which the item will be loaded.
     * @property listenerOfSelectMode for selected items state.
     */
    class ViewHolder private constructor(
        private val binding: ListItemEvaluationForAddSubjectBinding,
        private val listenerOfSelectMode: ListenerOfSelectMode<Evaluation>
    ) : RecyclerView.ViewHolder(binding.root) {

        /**
         * Attach data to the view, listen for events and define their behaviour.
         *
         * @param item the item will be rendered.
         * @param clickListener for single click and long click actions.
         * @param viewLifecycleOwner for observe events.
         */
        fun bind(
            item: Evaluation,
            clickListener: HolderListener<Evaluation>,
            viewLifecycleOwner: LifecycleOwner
        ) {
            var lock = true
            binding.evaluation = item
            binding.listItemEvaluationForAddSubjectExtraOrdinaryText.visibility =
                if (item.extraOrdinary)
                    View.VISIBLE
                else
                    View.GONE
            binding.listItemEvaluationForAddSubjectTitle.setOnCheckedChangeListener { _: CompoundButton?, _: Boolean ->
                if (!lock) {
                    clickListener.onLongClick(item)
                    if (listenerOfSelectMode.selectedItems.contains(item)) {
                        listenerOfSelectMode.selectedItems.remove(item)
                    } else {
                        listenerOfSelectMode.selectedItems.add(item)
                    }
                }
            }
            listenerOfSelectMode.selectChanged.observe(viewLifecycleOwner, Observer {
                if (it && listenerOfSelectMode.selectedItems.contains(item)) {
                    binding.listItemEvaluationForAddSubjectTitle.isChecked = true
                }
            })
            binding.listItemEvaluationForAddSubjectTitle.isChecked =
                listenerOfSelectMode.selectedItems.contains(item)
            binding.executePendingBindings()
            lock = false
        }

        companion object {
            /**
             * Static function for load and create appropriately the [ViewHolder].
             *
             * @param parent The ViewGroup into which the new View will be added after it is bound to
             *               an adapter position.
             * @param listenerOfSelectMode for observe selected items changes.
             * @return a new [ViewHolder].
             */
            fun from(
                parent: ViewGroup,
                listenerOfSelectMode: ListenerOfSelectMode<Evaluation>
            ): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding =
                    ListItemEvaluationForAddSubjectBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding, listenerOfSelectMode)
            }
        }
    }
}