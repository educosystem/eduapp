/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *  
 */



package com.educosystem.eduapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.entity.Block
import com.educosystem.eduapp.databinding.ListItemBlockBinding
import com.educosystem.eduapp.util.HolderListener
import com.educosystem.eduapp.util.ListenersOfActionMode
import java.util.*

/**
 * Adapter for block list items.
 *
 * @property clickListener for handle the click actions of items.
 * @property viewLifecycleOwner for observe values.
 */
class BlockAdapter(
    private val clickListener: HolderListener<Block>,
    private val viewLifecycleOwner: LifecycleOwner
) : ListAdapter<Block, BlockAdapter.ViewHolder>(BlockDiffCallback()),
    ListenersOfActionMode<Block> {
    override val selectAll = MutableLiveData<Boolean>()
    override val actionModeEnabled = MutableLiveData<Boolean>()
    override var selectedItems = ArrayList<Block>()

    /**
     * Called by RecyclerView to display the data at the specified position.
     *
     * @param holder The ViewHolder which should be updated to represent the contents of the
     *        item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, clickListener, viewLifecycleOwner)
    }

    /**
     * Called when RecyclerView needs a new [ViewHolder] of the given type to represent
     * an item.
     *
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     *               an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent, this)
    }

    /**
     * Change the selected state for all elements of the list.
     *
     * @param value if all elements should be selected.
     */
    override fun changeSelectedAllTo(value: Boolean) {
        if (value) {
            selectedItems.clear()
            selectedItems.addAll(currentList)
        }
        selectAll.value = value
    }

    /**
     * Activate other behaviours that should be doing in this mode
     * and clear the [selectedItems] list for prevent older item selections
     * will be selected again.
     *
     */
    override fun activateActionMode() {
        selectedItems.clear()
        actionModeEnabled.value = true
    }

    /**
     * Activate other behaviours that should be doing in this mode,
     * clear the [selectedItems] list and restore the list.
     *
     * This method is used for restoring states when a configuration device change.
     *
     * @param items selected.
     */
    override fun activateActionMode(items: ArrayList<Block>) {
        selectedItems.clear()
        selectedItems.addAll(items)
        actionModeEnabled.value = true
    }

    /**
     * Called for restore default behaviours when this mode end.
     *
     */
    override fun actionModeDone() {
        actionModeEnabled.value = false
        selectAll.value = false
    }

    /**
     * Describe an item view, metadata and behaviour about its place within the RecyclerView.
     *
     * @property binding the view which the item will be loaded.
     * @property context used for obtain the selected colour.
     * @property listenersOfActionMode for changes modes and selected items state.
     */
    class ViewHolder private constructor(
        private val binding: ListItemBlockBinding,
        private val context: Context,
        private val listenersOfActionMode: ListenersOfActionMode<Block>
    ) : RecyclerView.ViewHolder(binding.root) {
        private var actionModeEnabled = false

        /**
         * Attach data to the view, listen for events and define their behaviour.
         *
         * @param item the item will be rendered.
         * @param clickListener for single click and long click actions.
         * @param lifecycleOwner for observe events.
         */
        fun bind(
            item: Block,
            clickListener: HolderListener<Block>,
            lifecycleOwner: LifecycleOwner
        ) {
            binding.block = item

            binding.listItemBlockItemContainer.setOnClickListener {
                if (actionModeEnabled) {
                    longClick(clickListener, item)
                } else {
                    clickListener.onClick(item.cod)
                }
            }
            binding.listItemBlockItemContainer.setOnLongClickListener {
                longClick(clickListener, item)
                true
            }
            binding.executePendingBindings()
            listenersOfActionMode.actionModeEnabled.observe(lifecycleOwner, Observer {
                actionModeEnabled = if (it) {
                    if (listenersOfActionMode.selectedItems.contains(item)) {
                        select()
                    }
                    true
                } else {
                    deselect()
                    false
                }
            })
            listenersOfActionMode.selectAll.observe(lifecycleOwner, Observer {
                if (it) {
                    select()
                }
            })
            if (listenersOfActionMode.actionModeEnabled.value == true && listenersOfActionMode.selectedItems.contains(
                    item
                )
            ) {
                select()
            } else {
                deselect()
            }
        }

        /**
         * The action when the user perform a long click to the item.
         *
         * @param clickListener for notify to parent to handle the click action.
         * @param item of the view clicked.
         */
        private fun longClick(
            clickListener: HolderListener<Block>,
            item: Block
        ) {
            clickListener.onLongClick(item)
            if (binding.listItemBlockItemContainer.isSelected) {
                deselect()
                listenersOfActionMode.selectedItems.remove(item)
            } else if (listenersOfActionMode.actionModeEnabled.value == true) {
                select()
                listenersOfActionMode.selectedItems.add(item)
            }
        }

        /**
         * Change state of the elements and its view into a selected state.
         *
         */
        private fun select() {
            binding.listItemBlockItemContainer.isSelected = true
            binding.listItemBlockTitle.isSelected = true
            if (binding.listItemBlockImageType.visibility == View.VISIBLE) {
                binding.listItemBlockImageType.setColorFilter(
                    ContextCompat.getColor(
                        context,
                        R.color.secondaryColor
                    )
                )
            } else
                binding.listItemBlockImageSelected.visibility = View.VISIBLE
        }

        /**
         * Change state of the elements and its view into a unselected state.
         *
         */
        private fun deselect() {
            binding.listItemBlockItemContainer.isSelected = false
            binding.listItemBlockTitle.isSelected = false
            if (binding.listItemBlockImageType.visibility == View.VISIBLE) {
                binding.listItemBlockImageType.clearColorFilter()
            } else
                binding.listItemBlockImageSelected.visibility = View.GONE
        }

        companion object {
            /**
             * Static function for load and create appropriately the [ViewHolder].
             *
             * @param parent The ViewGroup into which the new View will be added after it is bound to
             *               an adapter position.
             * @param listenersOfActionMode for observe events of the adapter.
             * @return a new [ViewHolder].
             */
            fun from(
                parent: ViewGroup,
                listenersOfActionMode: ListenersOfActionMode<Block>
            ): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemBlockBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding, parent.context, listenersOfActionMode)
            }
        }
    }
}

/**
 * Diff class to compare the difference between two items
 *
 */
class BlockDiffCallback : DiffUtil.ItemCallback<Block>() {

    /**
     * @param oldItem
     * @param newItem
     * @return if the items are the same
     */
    override fun areItemsTheSame(oldItem: Block, newItem: Block): Boolean =
        oldItem.id == newItem.id


    /**
     * @param oldItem
     * @param newItem
     * @return if the content of the items are the same
     */
    override fun areContentsTheSame(oldItem: Block, newItem: Block): Boolean =
        oldItem == newItem

}