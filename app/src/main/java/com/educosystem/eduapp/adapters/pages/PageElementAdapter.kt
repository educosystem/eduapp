/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.adapters.pages

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.educosystem.eduapp.ui.pages.PageElementFragment
import com.educosystem.eduapp.util.ListElementsTypes
import com.educosystem.eduapp.util.PageElementConstants
import com.educosystem.eduapp.util.PageListener

/**
 * A viewpager adapter for [com.educosystem.eduapp.ui.MainFragment],[com.educosystem.eduapp.ui.PendingElementFragment],
 * [com.educosystem.eduapp.ui.PendingGradesElementFragment].
 *
 * @property elementListener listener for the [PageElementFragment].
 * @property typeList define what elements will be loaded, one of the [ListElementsTypes] class.
 * @constructor
 *
 * @param fragment which have the ViewPager element.
 */
class PageElementAdapter(
    fragment: Fragment,
    private val elementListener: PageListener,
    private val typeList: ListElementsTypes

) : FragmentStateAdapter(fragment), PageElementConstants {

    /**
     * @return the total fragments that will be loaded.
     */
    override fun getItemCount(): Int = 1

    /**
     * Create the fragments depending of the number of page.
     *
     * @param position of the page.
     * @return a new fragment.
     */
    override fun createFragment(position: Int): Fragment {
        val fragment: Fragment = when (position) {
            0 -> PageElementFragment(
                elementListener
            )
            else -> Fragment()
        }
        fragment.arguments = Bundle().apply {
            putSerializable(ARG_TYPE_LIST, typeList)
        }
        return fragment
    }
}
