/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.adapters.pages

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.educosystem.eduapp.ui.pages.PageEvaluationFragment
import com.educosystem.eduapp.ui.pages.PageSubjectFragment
import com.educosystem.eduapp.util.PageDetailSchoolYearConstants
import com.educosystem.eduapp.util.PageListener

/**
 * A viewpager adapter for [com.educosystem.eduapp.ui.DetailSchoolYearFragment].
 *
 * @property codSchoolYear from which its details are loaded.
 * @property evaluationListener listener for the [PageEvaluationFragment].
 * @property subjectListener listener for the [PageSubjectFragment].
 * @constructor
 *
 * @param fragment which have the ViewPager element.
 */
class PageDetailSchoolYearAdapter(
    fragment: Fragment,
    private val codSchoolYear: String,
    private val evaluationListener: PageListener,
    private val subjectListener: PageListener

) : FragmentStateAdapter(fragment), PageDetailSchoolYearConstants {

    /**
     * @return the total fragments that will be loaded.
     */
    override fun getItemCount(): Int = 2

    /**
     * Create the fragments depending of the number of page.
     *
     * @param position of the page.
     * @return a new fragment.
     */
    override fun createFragment(position: Int): Fragment {
        val fragment: Fragment = when (position) {
            0 -> PageSubjectFragment(
                subjectListener
            )
            else -> PageEvaluationFragment(
                evaluationListener
            )
        }
        fragment.arguments = Bundle().apply {
            putString(ARG_OBJECT, codSchoolYear)
        }
        return fragment
    }
}
