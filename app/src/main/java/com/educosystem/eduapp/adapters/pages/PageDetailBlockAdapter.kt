/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.adapters.pages

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.educosystem.eduapp.ui.pages.PageElementFragment
import com.educosystem.eduapp.ui.pages.PageSubBlockFragment
import com.educosystem.eduapp.util.ListElementsTypes
import com.educosystem.eduapp.util.PageDetailBlockConstants
import com.educosystem.eduapp.util.PageElementConstants
import com.educosystem.eduapp.util.PageListener

/**
 * A viewpager adapter for [com.educosystem.eduapp.ui.DetailBlockFragment].
 *
 * @property codBlock from which its details are loaded.
 * @property typedBlock if the block have one type defined.
 * @property homeworkType if the type block is [com.educosystem.eduapp.util.TYPE_HOMEWORK].
 * @property elementListener listener for the [PageElementFragment].
 * @property subBlockListener listener for the [PageSubBlockFragment].
 * @constructor
 *
 * @param fragment which have the ViewPager element.
 */
class PageDetailBlockAdapter(
    fragment: Fragment,
    private val codBlock: String,
    private val typedBlock: Boolean,
    private val homeworkType: Boolean,
    private val elementListener: PageListener,
    private val subBlockListener: PageListener

) : FragmentStateAdapter(fragment), PageDetailBlockConstants, PageElementConstants {

    /**
     * @return the total fragments that will be loaded.
     */
    override fun getItemCount(): Int = if (homeworkType) 4 else 5

    /**
     * Create the fragments depending of the number of page.
     *
     * @param position of the page.
     * @return a new fragment.
     */
    override fun createFragment(position: Int): Fragment {
        val fragment: Fragment = when (position) {
            0, 1, 2 -> PageElementFragment(
                elementListener
            )
            3 -> if (homeworkType) PageSubBlockFragment(
                subBlockListener
            ) else PageElementFragment(
                elementListener
            )
            4 -> PageSubBlockFragment(
                subBlockListener
            )
            else -> Fragment()
        }
        when (position) {
            0 -> fragment.arguments = Bundle().apply {
                putString(ARG_BLOCK, codBlock)
                putBoolean(ARG_TYPED_BLOCK, typedBlock)
                putSerializable(ARG_TYPE_LIST, ListElementsTypes.ACTIVE)
            }
            1 -> fragment.arguments = Bundle().apply {
                putString(ARG_BLOCK, codBlock)
                putBoolean(ARG_TYPED_BLOCK, typedBlock)
                putSerializable(ARG_TYPE_LIST, ListElementsTypes.PENDING)
            }
            2 -> if (homeworkType) loadCompletedList(fragment)
            else fragment.arguments = Bundle().apply {
                putString(ARG_BLOCK, codBlock)
                putBoolean(ARG_TYPED_BLOCK, typedBlock)
                putSerializable(ARG_TYPE_LIST, ListElementsTypes.PENDING_GRADES)
            }
            3 -> if (homeworkType) fragment.arguments = Bundle().apply {
                putString(ARG_OBJECT, codBlock)
            }
            else loadCompletedList(fragment)
            4 -> fragment.arguments = Bundle().apply {
                putString(ARG_OBJECT, codBlock)
            }
        }
        return fragment
    }

    /**
     * Load the arguments for the completed list type of the fragment [PageElementFragment].
     * @param fragment: the fragment to which the arguments will be added.
     */
    private fun loadCompletedList(fragment: Fragment) {
        fragment.arguments = Bundle().apply {
            putString(ARG_SUBJECT, codBlock)
            putBoolean(ARG_TYPED_BLOCK, typedBlock)
            putSerializable(ARG_TYPE_LIST, ListElementsTypes.COMPLETED)
        }
    }
}
