/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *  
 */



package com.educosystem.eduapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.educosystem.eduapp.R
import com.educosystem.eduapp.data.dao.SubjectDao
import com.educosystem.eduapp.data.entity.Element
import com.educosystem.eduapp.databinding.ListItemElementBinding
import com.educosystem.eduapp.util.*
import kotlinx.coroutines.*
import java.util.*

/**
 * Adapter for elements list items.
 *
 * @property clickListener for handle the click actions of items.
 * @property subjectDao necessary for get the color of the subject from element
 * @property viewLifecycleOwner for observe values.
 */
class ElementAdapter(
    private val clickListener: HolderListener<Element>,
    private val subjectDao: SubjectDao,
    private val viewLifecycleOwner: LifecycleOwner
) : ListAdapter<Element, ElementAdapter.ViewHolder>(ElementDiffCallback()),
    ListenersOfActionMode<Element>, BackgroundTranslucentListener, TypedElementsListener {
    override val selectAll = MutableLiveData<Boolean>()
    override val actionModeEnabled = MutableLiveData<Boolean>()
    override val translucent = MutableLiveData(false)
    override var selectedItems = ArrayList<Element>()
    override val typedElements = MutableLiveData(false)

    /**
     * Called by RecyclerView to display the data at the specified position.
     *
     * @param holder The ViewHolder which should be updated to represent the contents of the
     *        item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, clickListener, viewLifecycleOwner)
    }

    /**
     * Called when RecyclerView needs a new [ViewHolder] of the given type to represent
     * an item.
     *
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     *               an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent, this, this, this, subjectDao)
    }

    /**
     * Change the selected state for all elements of the list.
     *
     * @param value if all elements should be selected.
     */
    override fun changeSelectedAllTo(value: Boolean) {
        if (value) {
            selectedItems.clear()
            selectedItems.addAll(currentList)
        }
        selectAll.value = value
    }

    /**
     * Activate other behaviours that should be doing in this mode
     * and clear the [selectedItems] list for prevent older item selections
     * will be selected again.
     *
     */
    override fun activateActionMode() {
        selectedItems.clear()
        actionModeEnabled.value = true
    }

    /**
     * Activate other behaviours that should be doing in this mode,
     * clear the [selectedItems] list and restore the list.
     *
     * This method is used for restoring states when a configuration device change.
     *
     * @param items selected.
     */
    override fun activateActionMode(items: ArrayList<Element>) {
        selectedItems.clear()
        selectedItems.addAll(items)
        actionModeEnabled.value = true
    }

    /**
     * Called for restore default behaviours when this mode end.
     *
     */
    override fun actionModeDone() {
        actionModeEnabled.value = false
        selectAll.value = false
    }

    /**
     * This set the background of the elements to a translucent colour.
     * This method is called when the page of the details elements is from fragment that have
     * the colour of the subject applied.
     *
     */
    override fun setTranslucentBackground() {
        translucent.value = true
    }

    /**
     * This hide the icons of the elements.
     * This method is called when the page of the details elements is from fragment that have
     * a type defined.
     *
     */
    override fun setTypedElements() {
        typedElements.value = true
    }

    /**
     * Describe an item view, metadata and behaviour about its place within the RecyclerView.
     *
     * @property binding the view which the item will be loaded.
     * @property context used for obtain the multiple colour.
     * @property listenersOfActionMode for changes modes and selected items state.
     * @property backgroundTranslucentListener for apply translucent backgrounds.
     * @property typedElementsListener for hide the icons of the elements if necessary
     * @property subjectDao necessary for load the background colour from element
     */
    class ViewHolder private constructor(
        private val binding: ListItemElementBinding,
        private val context: Context,
        private val listenersOfActionMode: ListenersOfActionMode<Element>,
        private val backgroundTranslucentListener: BackgroundTranslucentListener,
        private val typedElementsListener: TypedElementsListener,
        private val subjectDao: SubjectDao
    ) : RecyclerView.ViewHolder(binding.root) {
        private var actionModeEnabled = false

        /**
         * Attach data to the view, listen for events and define their behaviour.
         *
         * @param item the item will be rendered.
         * @param clickListener for single click and long click actions.
         * @param lifecycleOwner for observe events.
         */
        fun bind(
            item: Element,
            clickListener: HolderListener<Element>,
            lifecycleOwner: LifecycleOwner
        ) {
            binding.element = item

            binding.listItemElementItemContainer.setOnClickListener {
                if (actionModeEnabled) {
                    longClick(clickListener, item)
                } else {
                    clickListener.onClick(item.cod)
                }
            }
            if (item.deliveryDate != null
                && item.deliveryDate!! < Calendar.getInstance() && item.type != TYPE_EXAM
                || item.deliveryDate != null && item.deliveredDate != null
                && item.deliveredDate!! < item.deliveryDate!! && item.type != TYPE_EXAM
            )
                binding.listItemElementDelayedText.visibility = View.VISIBLE
            else
                binding.listItemElementDelayedText.visibility = View.GONE
            if (item.grade != null) {
                binding.listItemElementGrade.text = item.grade!!.toString()
                if (item.grade!! < 5)
                    binding.listItemElementGrade.setTextColor(
                        ContextCompat.getColor(
                            context,
                            R.color.delayedTextColour
                        )
                    )
                else
                    binding.listItemElementGrade.setTextColor(
                        ContextCompat.getColor(
                            context,
                            R.color.primaryTextColor
                        )
                    )
                binding.listItemElementGrade.visibility = View.VISIBLE
            } else {
                binding.listItemElementGrade.visibility = View.GONE
            }
            backgroundTranslucentListener.translucent.observe(lifecycleOwner, Observer {
                if (it) {
                    binding.listItemElementItemContainer.setBackgroundResource(R.drawable.selectable_transparent_item_background)
                } else {
                    changeBackgroundColor(item.cod)
                }
            })
            binding.listItemElementItemContainer.setOnLongClickListener {
                longClick(clickListener, item)
                true
            }
            typedElementsListener.typedElements.observe(lifecycleOwner, Observer {
                if (it) {
                    binding.listItemElementImageType.visibility = View.GONE
                } else {
                    val imageType = obtainImageType(item.type, context.resources, context)
                    binding.listItemElementImageType.setImageDrawable(imageType.icon)
                    binding.listItemElementImageType.visibility = View.VISIBLE
                }
            })
            binding.executePendingBindings()
            listenersOfActionMode.actionModeEnabled.observe(lifecycleOwner, Observer {
                actionModeEnabled = if (it) {
                    if (listenersOfActionMode.selectedItems.contains(item)) {
                        select()
                    }
                    true
                } else {
                    deselect()
                    false
                }
            })
            listenersOfActionMode.selectAll.observe(lifecycleOwner, Observer {
                if (it) {
                    select()
                }
            })
            if (listenersOfActionMode.actionModeEnabled.value == true && listenersOfActionMode.selectedItems.contains(
                    item
                )
            ) {
                select()
            } else {
                deselect()
            }
        }

        /**
         * The action when the user perform a long click to the item.
         *
         * @param clickListener for notify to parent to handle the click action.
         * @param item of the view clicked.
         */
        private fun longClick(
            clickListener: HolderListener<Element>,
            item: Element
        ) {
            clickListener.onLongClick(item)
            if (binding.listItemElementItemContainer.isSelected) {
                deselect()
                listenersOfActionMode.selectedItems.remove(item)
            } else if (listenersOfActionMode.actionModeEnabled.value == true) {
                select()
                listenersOfActionMode.selectedItems.add(item)
            }
        }

        /**
         * Change state of the elements and its view into a selected state.
         *
         */
        private fun select() {
            binding.listItemElementItemContainer.isSelected = true
            binding.listItemElementTitle.isSelected = true
            if (binding.listItemElementImageType.visibility == View.VISIBLE) {
                binding.listItemElementImageType.setColorFilter(
                    ContextCompat.getColor(
                        context,
                        R.color.secondaryColor
                    )
                )
            } else
                binding.listItemElementImageSelected.visibility = View.VISIBLE
        }

        /**
         * Change state of the elements and its view into a unselected state.
         *
         */
        private fun deselect() {
            binding.listItemElementItemContainer.isSelected = false
            binding.listItemElementTitle.isSelected = false
            if (binding.listItemElementImageType.visibility == View.VISIBLE) {
                binding.listItemElementImageType.clearColorFilter()
            } else
                binding.listItemElementImageSelected.visibility = View.GONE
        }

        /**
         * Load the background colour for item
         *
         * @param codElement which the colour will be loaded.
         */
        private fun changeBackgroundColor(codElement: String) {
            val uiScope = CoroutineScope(Dispatchers.Main + SupervisorJob())
            uiScope.launch {
                val type = withContext(Dispatchers.IO) {
                    subjectDao.getTypeColourFromElement(codElement)
                }
                binding.listItemElementItemContainer.setBackgroundColor(
                    obtainColour(
                        type,
                        context.resources,
                        context
                    ).color
                )
            }
        }

        companion object {
            /**
             * Static function for load and create appropriately the [ViewHolder].
             *
             * @param parent The ViewGroup into which the new View will be added after it is bound to
             *               an adapter position.
             * @param listenersOfActionMode for changes modes and selected items state.
             * @param backgroundTranslucentListener for apply translucent backgrounds.
             * @param typedElementsListener for hide the icons of the elements if necessary
             * @param subjectDao necessary for load the background colour from element
             * @return a new [ViewHolder]
             */
            fun from(
                parent: ViewGroup,
                listenersOfActionMode: ListenersOfActionMode<Element>,
                backgroundTranslucentListener: BackgroundTranslucentListener,
                typedElementsListener: TypedElementsListener,
                subjectDao: SubjectDao
            ): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemElementBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(
                    binding,
                    parent.context,
                    listenersOfActionMode,
                    backgroundTranslucentListener,
                    typedElementsListener,
                    subjectDao
                )
            }
        }
    }
}

/**
 * Diff class to compare the difference between two items
 *
 */
class ElementDiffCallback : DiffUtil.ItemCallback<Element>() {
    /**
     * @param oldItem
     * @param newItem
     * @return if the items are the same
     */
    override fun areItemsTheSame(oldItem: Element, newItem: Element): Boolean =
        oldItem.id == newItem.id

    /**
     * @param oldItem
     * @param newItem
     * @return if the content of the items are the same
     */
    override fun areContentsTheSame(oldItem: Element, newItem: Element): Boolean =
        oldItem == newItem

}