/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.adapters.pages

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.educosystem.eduapp.ui.pages.PageSchoolYearFragment
import com.educosystem.eduapp.util.PageListSchoolYearConstants
import com.educosystem.eduapp.util.PageListener

/**
 * A viewpager adapter for [com.educosystem.eduapp.ui.DetailSchoolYearFragment].
 *
 * @property listener listener for the [PageSchoolYearFragment].
 * @constructor
 *
 * @param fragment which have the ViewPager element.
 */
class PageSchoolYearAdapter(
    fragment: Fragment,
    private val listener: PageListener
) : FragmentStateAdapter(fragment), PageListSchoolYearConstants {

    /**
     * @return the total fragments that will be loaded.
     */
    override fun getItemCount(): Int = 2

    /**
     * Create the fragments depending of the number of page.
     *
     * @param position of the page.
     * @return a new fragment.
     */
    override fun createFragment(position: Int): Fragment {
        val fragment =
            PageSchoolYearFragment(listener)
        fragment.arguments = Bundle().apply {
            putBoolean(ARG_OBJECT, 0 == position)
        }
        return fragment
    }
}