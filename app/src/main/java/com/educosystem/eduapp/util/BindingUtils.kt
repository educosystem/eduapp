/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.util

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.educosystem.eduapp.R
import java.util.*

/**
 * Adapt the start date
 */
@BindingAdapter("startDateString")
fun TextView.setStartDateString(item: Calendar?) {
    if (item != null) {
        text = context.getString(R.string.startDate, formatDate(item, context.resources))
        visibility = VISIBLE
    } else {
        visibility = GONE
    }
}

/**
 * Adapt the finish date
 */
@BindingAdapter("finishDateString")
fun TextView.setFinishDateString(item: Calendar?) {
    if (item != null) {
        text =
            context.getString(
                R.string.finishDate,
                formatDate(item, context.resources)
            )
        visibility = VISIBLE
    } else {
        visibility = GONE
    }
}

/**
 * Adapt the delivery date
 */
@BindingAdapter("deliveryDateString")
fun TextView.setDeliveryDateString(item: Calendar?) {
    if (item != null) {
        text = context.getString(
            R.string.deliveryDateWithDate,
            "${formatDate(item, context.resources)} " +
                    String.format(
                        "%02d:%02d",
                        item.get(Calendar.HOUR_OF_DAY),
                        item.get(Calendar.MINUTE)
                    )
        )
        visibility = VISIBLE
    } else {
        visibility = GONE
    }
}

/**
 * Adapt the delivered date
 */
@BindingAdapter("deliveredDateString")
fun TextView.setDeliveredDateString(item: Calendar?) {
    if (item != null) {
        text = context.getString(
            R.string.deliveredDateWithDate,
            "${formatDate(item, context.resources)} " +
                    String.format(
                        "%02d:%02d",
                        item.get(Calendar.HOUR_OF_DAY),
                        item.get(Calendar.MINUTE)
                    )
        )
        visibility = VISIBLE
    } else {
        visibility = GONE
    }
}

/**
 * Adapt the integer type color of subject to a backgroundColor for views
 */
@BindingAdapter("typeToBackgroundColor")
fun View.setBackgroundColorByType(type: Int) {
    val color = obtainColour(type, context.resources, context)
    setBackgroundColor(color.color)
}

/**
 * Adapt the type of the elements, block and subBlock
 */
@BindingAdapter("elementType")
fun ImageView.setElementTypeImage(type: Int?) {
    if (type != null) {
        val imageType = obtainImageType(type, context.resources, context)
        setImageDrawable(imageType.icon)
        contentDescription = imageType.description
        visibility = VISIBLE
    } else
        visibility = GONE
}

/**
 * Adapt the type of the elements, block and subBlock
 */
@BindingAdapter("percent")
fun TextView.setPercentString(percent: Int?) {
    if (percent != null) {
        text = context.getString(R.string.percent, percent)
        visibility = VISIBLE
    } else {
        visibility = GONE
    }
}

/**
 * Adapt the text with visibility changes
 */
@BindingAdapter("textAndVisibility")
fun TextView.setTextString(textString: String?) {
    if (textString != null && textString.isNotEmpty()) {
        text = textString
        visibility = VISIBLE
    } else {
        visibility = GONE
    }
}