/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.util

import android.graphics.drawable.Drawable
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

/**
 * To hide an show the fab icon when the nestedScrollView is scrolled
 */
class NestedScrollListener(private val fab: FloatingActionButton) :
    NestedScrollView.OnScrollChangeListener {

    override fun onScrollChange(
        v: NestedScrollView?,
        scrollX: Int,
        scrollY: Int,
        oldScrollX: Int,
        oldScrollY: Int
    ) {
        if (scrollY > oldScrollY) {
            fab.hide()
        } else {
            fab.show()
        }
    }
}

/**
 * To notify hide an show the fab icon to the parent when the recyclerView is scrolled
 */
class RecyclerViewListener(private val scrollListener: ScrollListener) :
    RecyclerView.OnScrollListener() {
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        scrollListener.hideButton(dy > 0)
    }
}

/**
 * Interface to implements the communication between the fragment and the adapter when the action mode
 * is activated
 */
interface ListenersOfActionMode<T> {
    val selectAll: MutableLiveData<Boolean>
    val actionModeEnabled: MutableLiveData<Boolean>
    var selectedItems: ArrayList<T>

    fun activateActionMode()
    fun actionModeDone()
    fun changeSelectedAllTo(value: Boolean)
    fun activateActionMode(items: ArrayList<T>)
}

/**
 * Interface to implement if the adapter have to change the background colour
 */
interface BackgroundTranslucentListener {
    val translucent: MutableLiveData<Boolean>

    fun setTranslucentBackground()
}

/**
 * Interface to implement if the element adapter have to show the type icons of the elements
 */
interface TypedElementsListener {
    val typedElements: MutableLiveData<Boolean>

    fun setTypedElements()
}

/**
 * Interface to implement what element is active
 */
interface ActiveFlagListener<T> {
    val activeItems: MutableLiveData<List<T>>

    fun updateActiveItem(items: List<T>)
}

/**
 * Interface to implements the communication between the fragment and the adapter when only need to
 * communicate the selected items
 */
interface ListenerOfSelectMode<T> {
    val selectedItems: ArrayList<T>
    val selectChanged: MutableLiveData<Boolean>

    fun changeSelected(items: ArrayList<T>)
}

/**
 * Necessary to use the recyclerViewListener
 * The class that want to communicate to parent need to implement a listener class that implement
 * this interface
 */
interface ScrollListener {
    fun hideButton(scrolledDown: Boolean)
}

/**
 * Util class for communicate from fragment page listener to parent fragment events
 */
class PageListener(
    private val startNavigate: (cod: String) -> Unit,
    private val showSnackBarMessage: (message: String, function: (() -> Unit)?) -> Unit,
    private val hideFabButton: (hide: Boolean) -> Unit
) : ScrollListener {
    fun navigate(cod: String) = startNavigate(cod)
    fun showMessage(message: String, function: (() -> Unit)? = null) =
        showSnackBarMessage(message, function)
    override fun hideButton(scrolledDown: Boolean) = hideFabButton(scrolledDown)
}

/**
 * Util class for communicate a click or a long click from a viewHolder to the fragment
 */
class HolderListener<T>(
    private val clickListener: (cod: String) -> Unit,
    private val onLongClickListener: (item: T) -> Unit
) {
    fun onClick(cod: String) = clickListener(cod)
    fun onLongClick(item: T) = onLongClickListener(item)
}

data class Colour(val color: Int, val description: String)

data class ImageType(val icon: Drawable?, val description: String)