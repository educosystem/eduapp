/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */



package com.educosystem.eduapp.util

import android.content.Context
import android.content.res.Resources
import androidx.core.content.ContextCompat
import com.educosystem.eduapp.R
import java.util.*
import kotlin.collections.ArrayList

/**
 * Transform a calendar date to a string according to the translate of regional date
 */
fun formatDate(date: Calendar, resources: Resources): String {
    return resources.getString(
        R.string.dateFormatted,
        date.get(Calendar.MONTH) + 1,
        date.get(Calendar.DAY_OF_MONTH),
        date.get(Calendar.YEAR)
    )
}

/**
 * @return all list of colours available
 */
fun obtainListOfColours(resources: Resources, context: Context): MutableList<Colour>? {
    val colours = ArrayList<Colour>()
    for (i in 0..10) {
        colours.add(obtainColour(i, resources, context))
    }
    return colours.toMutableList()
}

/**
 * @return the color class depending of the type
 */
fun obtainColour(type: Int, resources: Resources, context: Context): Colour =
    when (type) {
        1 -> Colour(
            ContextCompat.getColor(context, R.color.palette1),
            resources.getString(R.string.salmonColour)
        )
        2 -> Colour(
            ContextCompat.getColor(context, R.color.palette2),
            resources.getString(R.string.pinkColour)
        )
        3 -> Colour(
            ContextCompat.getColor(context, R.color.palette3),
            resources.getString(R.string.violetColour)
        )
        4 -> Colour(
            ContextCompat.getColor(context, R.color.palette4),
            resources.getString(R.string.blueColour)
        )
        5 -> Colour(
            ContextCompat.getColor(context, R.color.palette5),
            resources.getString(R.string.greenColour)
        )
        6 -> Colour(
            ContextCompat.getColor(context, R.color.palette6),
            resources.getString(R.string.yellowColour)
        )
        7 -> Colour(
            ContextCompat.getColor(context, R.color.palette7),
            resources.getString(R.string.amberColour)
        )
        8 -> Colour(
            ContextCompat.getColor(context, R.color.palette8),
            resources.getString(R.string.orangeColour)
        )
        9 -> Colour(
            ContextCompat.getColor(context, R.color.palette9),
            resources.getString(R.string.deepOrangeColour)
        )
        10 -> Colour(
            ContextCompat.getColor(context, R.color.palette10),
            resources.getString(R.string.grayColour)
        )
        else -> Colour(
            ContextCompat.getColor(context, R.color.palette0),
            resources.getString(R.string.defaultColour)
        )
    }

/**
 * @return the type of color depending of the color of the object [colour]
 */
fun obtainTypeColour(colour: Colour, context: Context): Int =
    when (colour.color) {
        ContextCompat.getColor(context, R.color.palette1) -> 1
        ContextCompat.getColor(context, R.color.palette2) -> 2
        ContextCompat.getColor(context, R.color.palette3) -> 3
        ContextCompat.getColor(context, R.color.palette4) -> 4
        ContextCompat.getColor(context, R.color.palette5) -> 5
        ContextCompat.getColor(context, R.color.palette6) -> 6
        ContextCompat.getColor(context, R.color.palette7) -> 7
        ContextCompat.getColor(context, R.color.palette8) -> 8
        ContextCompat.getColor(context, R.color.palette9) -> 9
        ContextCompat.getColor(context, R.color.palette10) -> 10
        else -> 0
    }

/**
 * @return the ImageType class depending of the type of the content
 */
fun obtainImageType(type: Int, resources: Resources, context: Context): ImageType =
    when (type) {
        TYPE_EXAM -> ImageType(
            ContextCompat.getDrawable(context, R.drawable.ic_exam_black_24dp),
            resources.getString(R.string.examsName)
        )
        TYPE_WORK -> ImageType(
            ContextCompat.getDrawable(context, R.drawable.ic_work_black_24dp),
            resources.getString(R.string.worksName)
        )
        TYPE_HOMEWORK -> ImageType(
            ContextCompat.getDrawable(context, R.drawable.ic_home_work_black_24dp),
            resources.getString(R.string.homeworksName)
        )
        else -> ImageType(
            ContextCompat.getDrawable(context, R.drawable.ic_unknown_black_24dp),
            resources.getString(R.string.otherName)
        )
    }

/**
 * @return the type of content depending of the icon [imageType]
 */
fun obtainTypeImage(imageType: ImageType, context: Context): Int =
    when (imageType.icon) {
        ContextCompat.getDrawable(context, R.drawable.ic_exam_black_24dp) -> TYPE_EXAM
        ContextCompat.getDrawable(context, R.drawable.ic_work_black_24dp) -> TYPE_WORK
        ContextCompat.getDrawable(context, R.drawable.ic_home_work_black_24dp) -> TYPE_HOMEWORK
        else -> TYPE_OTHER
    }