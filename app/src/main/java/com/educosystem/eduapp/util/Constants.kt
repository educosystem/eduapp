/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.util

const val TYPE_EXAM = 1
const val TYPE_WORK = 2
const val TYPE_HOMEWORK = 3
const val TYPE_OTHER = 10
const val LINEAR_LAYOUT_ELEMENTS = 1
const val GRID_LAYOUT_ELEMENTS = 2

interface DetailSchoolYearFragmentConstants {
    val ARG_OBJECT: String
        get() = "codSchoolYear"
}

interface DetailEvaluationFragmentConstants {
    val ARG_OBJECT: String
        get() = "codEvaluation"
}

interface DetailSubjectFragmentConstants {
    val ARG_OBJECT: String
        get() = "codSubject"
}

interface DetailSubSubjectFragmentConstants {
    val ARG_OBJECT: String
        get() = "codSubSubject"
}

interface EditBlockDialogConstants {
    val ARG_SUB_SUBJECT: String
        get() = "codSubSubject"
    val ARG_BLOCK: String
        get() = "codBlock"
    val ARG_SUB_BLOCK: String
        get() = "codSubBlock"
    val ARG_COLOR: String
        get() = "color"
}

interface PageListSchoolYearConstants {
    val ARG_OBJECT: String
        get() = "active"
}

interface PageDetailSchoolYearConstants {
    val ARG_OBJECT: String
        get() = "codSchoolYear"
}

interface PageDetailEvaluationConstants {
    val ARG_OBJECT: String
        get() = "codEvaluation"
}

interface PageDetailSubjectConstants {
    val ARG_OBJECT: String
        get() = "codSubject"
}

interface PageDetailSubSubjectConstants {
    val ARG_OBJECT: String
        get() = "codSubSubject"
}

interface PageDetailBlockConstants {
    val ARG_OBJECT: String
        get() = "codBlock"
}

interface PageElementConstants {
    val ARG_TYPE_LIST: String
        get() = "typeList"
    val ARG_SUBJECT: String
        get() = "codSubject"
    val ARG_SUB_SUBJECT: String
        get() = "codSubSubject"
    val ARG_BLOCK: String
        get() = "codBlock"
    val ARG_SUB_BLOCK: String
        get() = "codSubBlock"
    val ARG_TYPED_BLOCK: String
        get() = "typedBlock"
}

enum class ListElementsTypes {
    ACTIVE, PENDING, COMPLETED, PENDING_GRADES
}