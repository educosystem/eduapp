/*
 *
 *  * This file is part of EduApp.
 *  *
 *  *     EduApp is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     EduApp is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  *     Copyright Cosme José Nieto Pérez, 2020
 *
 */

package com.educosystem.eduapp.util

import android.content.res.Resources
import android.os.Build
import com.educosystem.eduapp.R
import java.util.*

/**
 * Adapt the calendar with the spinners items.
 *
 * @property calendar value.
 * @property resources for get the string representation.
 * @property representation for put a personalized text in the spinner.
 */
data class CalendarAdapter(
    var calendar: Calendar,
    val resources: Resources,
    var representation: String? = null
) {
    override fun toString(): String =
        representation
            ?: resources.getString(
                R.string.dateOf,
                calendar.getDisplayName(
                    Calendar.DAY_OF_WEEK,
                    Calendar.SHORT,
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                        resources.configuration.locales[0]
                    else
                        resources.configuration.locale
                ),
                calendar.get(Calendar.DAY_OF_MONTH),
                calendar.getDisplayName(
                    Calendar.MONTH,
                    Calendar.SHORT,
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                        resources.configuration.locales[0]
                    else
                        resources.configuration.locale
                )
            )

}